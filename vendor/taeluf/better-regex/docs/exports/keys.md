
# Syntax.Comments
Each comment must start with ` #` (space hash). Everything following in that line is comment and will be removed from output
If your pattern uses ` #`, escape it like ` \#`


# Syntax.Functions
```
::functionName(arg1 ;; arg2 ;; arg3) ## Comments if you want
```
- The function must be the only thing on the line. Comments allowed.  
- Args must **not** be quoted. Every arg will be trimmed (surrounding whitespace is removed).  
- Args are separated by ' ;; ' (space semicolon semicolon space)


# Syntax.Refs
Functions can accept a refs list as an argument. Each ref is closed in double moustaches `{{refname}}`
```
## Combine selected refs with the bar (|) separator
::combine( {{ref1}}{{ref2}}{{ref3}} ;; |)
```
Each ref name can be composed of `a-z`, `A-Z`, `0-9`, and/or special chars: `. - _`


# Example.Cleaning.Src
<<<REGEX
One
#a comment at the start
## Another start of line comment
#
##
\#\ 
\
\ 
    eol_esc_slash\\\\\\\\\\\\
    eol_space_test\\\\\\\\\         
    abc\ #Should this be a comment? It IS, only because making it NOT a comment is much more complicated & harder to communicate.
    def\  #this IS a comment
    ghi\ \#this is not a comment
    jkl\\\\ #this IS a comment
        #
    \ Two # Am a comment
    ( ( # Am another comment
        \#Three # This seems like a lot of comments
		#[0-9] # You need escape your # lie \# to use a space + hash as not-a-comment
    ))?
REGEX,


# Example.Cleaning.Target
'One\\#\\ \\ \\ eol_esc_slash\\\\\\\\\\\\eol_space_test\\\\\\\\\\ abc\\ def\\ ghi\\ \\#this is not a commentjkl\\\\\ Two( (\#Three))?',


# Example.Full
$reg = <<<REGEX
    /abc\ # abc then a space
        ( # join referenced regexes with a |
        ::combine({{one}}{{two}}{{three}} ;; | )
        )\\ # literal backslash

        \# # literal hashtag (then comment)
    xyz/
REGEX;
$reg_refs = [
    'one'=>'(1|one|uno)',
    'two'=>'(2|two|dos)',
    'three'=>'(3|three|tres)',
    'four'=>'(4|four|quatro)'
];
$br = new \Breg();
$br->handle('combine',
    function(array $keys, string $joiner) use ($reg_refs){
        // make an array of only the selected regs
        $regs = [];
        foreach ($keys as $k){
            $regs[] = $reg_refs[$k];
        }
        return implode($joiner, $regs);
    }
);
$final = $br->parse($reg);

$this->compare(
    '/abc\ ((1|one|uno)|(2|two|dos)|(3|three|tres))\\\\#xyz/',
    $final,
);
$this->is_true(
    preg_match($final, 'abc dos\\#xyz/') === 1
);

