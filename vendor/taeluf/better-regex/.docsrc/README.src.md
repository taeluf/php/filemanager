# Better Regex
Multi-line regex with comments and functions. 

## Features
- `# comments` 
- `::functions(arg1 ;; {{array_item_1}}{{array_item_2}} ;; arg3)`
    - `$br->handle('functions', function(string $arg1, array $arg2, string $arg3){return 'a string';})`
- multi-line (`abc<NEWLINE>def` yields `abcdef`)

## Additional Functionality
- `trim`med lines
- End of line escaped space `pattern \ NEWLINE` 
- escaped hash `pattern \# pattern` (literal hashtag)
- escaped backslash `pattern \\NEWLINE ` (preserves the `\\`)

## Install
@template(composer_install, taeluf/better-regex)


## Full Example
```php
<?php
@import(Example.Full)
```

## Versions
- `v0.3` is an old, terribly designed version
- `v0.4` is good to go 
- Future plans: I don't expect to change this library much. It works and it doesn't need more features. It MIGHT get some built-in `::functions()` or a nicer syntax at some point? But it doesn't matter.

## Troubleshooting / Extra Stuff
- I think comments require a space before the `#`
- comments & multi-line are available with the `PCRE_EXTENDED` flag in php normally
