<?php

namespace Taeluf\BetterReg\Test\Cleaning;

function regex(){ 
    return
    [
    'src'=>
//@export_start(Example.Cleaning.Src)
<<<REGEX
One
#a comment at the start
## Another start of line comment
#
##
\#\ 
\
\ 
    eol_esc_slash\\\\\\\\\\\\
    eol_space_test\\\\\\\\\         
    abc\ #Should this be a comment? It IS, only because making it NOT a comment is much more complicated & harder to communicate.
    def\  #this IS a comment
    ghi\ \#this is not a comment
    jkl\\\\ #this IS a comment
        #
    \ Two # Am a comment
    ( ( # Am another comment
        \#Three # This seems like a lot of comments
		#[0-9] # You need escape your # lie \# to use a space + hash as not-a-comment
    ))?
REGEX,
//@export_end(Example.Cleaning.Src)
    'target'=>
//@export_start(Example.Cleaning.Target)
    'One\\#\\ \\ \\ eol_esc_slash\\\\\\\\\\\\eol_space_test\\\\\\\\\\ abc\\ def\\ ghi\\ \\#this is not a commentjkl\\\\\ Two( (\#Three))?',
//@export_end(Example.Cleaning.Target)
    // 'target'=>'Oneeol_space_test\\\\\ abc\ def\ ghi\ \#this is not a commentjkl\\\ Two( (\#Three))?',
    ];
}
class Cleaning extends \Tlf\Tester {

    public function testCleaning(){
        // This tests:
        // a. Removing comments
        // b. End of line escaped space `\ `
        // c. Beginning of line escaped space
        // d. Trimming lines
        // e. Removing newlines
        // f. escaped hash `\#`
        // g. escaped slash `\\` at end of line (but there should NOT be a space)
        $regex = regex();
        $regexSrc = $regex['src'];
        $target = $regex['target'];

        $parser = new \Breg\Parser();
        $parsed = $parser->parse($regexSrc);
        $this->compare($target,$parsed['clean_reg']);

    }
}

