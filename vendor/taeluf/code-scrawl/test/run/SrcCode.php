<?php

namespace Tlf\Scrawl\Test;

/**
 * For testing all things related to source code
 */
class SrcCode extends \Tlf\Tester {

    public $php_code = 
        <<<PHP
            <?php 

            /**
             * Does nothing
             * @feature(no feature)
             * @deprecated because it does nothing
             * @todo delete this useless class
             */
            class Abc extends Def {
                function ghi(){}
            } 
        PHP;



    /**
     * @test export_start() code export_end()
     */
    public function testExportStartEnd(){
        $str = <<<BASH
            // @export_start(change_git_origin)
            old_remote="$(git config --get remote.origin.url)"
            git remote remove origin
            git remote add origin "\${new_url}"
            # push all
            git push --mirror origin
            git remote add "$(date +%d_%m_%y_old)" "\${old_remote}"
            // @export_end(change_git_origin)
            // @export_start(nothing)
            var="some value"
            // @export_end(nothing)
        BASH;

        $start_end_ext = new \Tlf\Scrawl\FileExt\ExportStartEnd();
        $exports = $start_end_ext->get_exports($str);

        $this->compare(
            [
            'change_git_origin'=>
                'old_remote="$(git config --get remote.origin.url)"'."\n"
                ."git remote remove origin\n"
                .'git remote add origin "${new_url}"'."\n"
                ."# push all\n"
                ."git push --mirror origin\n"
                .'git remote add "$(date +%d_%m_%y_old)" "${old_remote}"',

            'nothing'=>'var="some value"',
            ],
            $exports
        );
    }


    /**
     * @test export a docblock (everything above the \@export() line)
     */
    public function testDocblockExport(){
        $str = <<<PHP
            /**
             * use the ast to create docs using the classList template
             *
             * @export(no_return) and some text?
             * @return array of files like `['rel/path'=>'file content']`
             */
            function who_cares(){}
            /**
             * Parsed `\$str` into an ast (using the Lexer)
             * @param \$str a string to parse
             * @return array ast
             * @export(with_return)
             */
            public \$it_doesnt_matter = null;
        PHP;
        $docblock_ext = new \Tlf\Scrawl\FileExt\ExportDocBlock();
        
        $blocks = $docblock_ext->get_docblocks($str);
        $exports = $docblock_ext->get_exports($blocks);

        $this->compare(
            [
                'no_return'=>'use the ast to create docs using the classList template',
                'with_return'=>"Parsed `\$str` into an ast (using the Lexer)\n@param \$str a string to parse\n@return array ast"
            ],
            $exports,
        );
        // print_r($exports);
    }

    /** 
     * @test php ast 
     * @test php make_docs()
     * @test scrawl write_doc()
     */
    public function testPhpExt(){
        $this->empty_dir($this->file('test/input/docs/'));
        $str = $this->php_code;



        $scrawl = new \Tlf\Scrawl(
            [
                'dir.docs'=>$this->file('test/input/docs/'),
                'markdown.preserveNewLines' => false,
                'markdown.prependGenNotice' => false,
            ],
        );
        $php_ext = new \Tlf\Scrawl\FileExt\Php($scrawl);

        $ast = $php_ext->parse_str($str);
        $files = $php_ext->make_docs($ast);

        foreach ($files as $rel_path=>$src){
            $scrawl->write_doc($rel_path, $src);
        }

        $this->str_contains($files['class/Abc.md'],
            '# class Abc',
            '- `function ghi()` ',
        );

        $this->compare(
            $files['class/Abc.md'],
            'file://'.$this->file('test/input/docs/class/Abc.md')
        );

    }

}
