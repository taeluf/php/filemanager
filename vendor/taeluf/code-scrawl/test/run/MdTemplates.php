<?php

namespace Tlf\Scrawl\Test;

/**
 * For testing all things `.md` file
 *
 */
class MdTemplates extends \Tlf\Tester {


    /** @test template bash/install */
    public function testBashInstall(){
        $scrawl = new \Tlf\Scrawl();
        $template = $scrawl->get_template('bash/install', ['scrawl-test', 'bin/scrawl-test']);

        $this->str_contains(
            $template,
            'command="scrawl-test"',
            'git clone https://gitlab.com/taeluf/php/CodeScrawl.git ${command}',
            'chmod ug+x "${downloadDir}/${command}/bin/scrawl-test"',

        );
        // echo $template;
    }

    /** @test template php/composer_install */
    public function testComposerInstall(){
        $scrawl = new \Tlf\Scrawl();
        $template = $scrawl->get_template('php/composer_install', ['taeluf/code-scrawl', 'v0.7.x-dev']);

        $this->str_contains(
            $template,
            'composer require taeluf/code-scrawl v0.7.x-dev',
            '"taeluf/code-scrawl": "v0.7.x-dev"',
        );
    }

    /** 
     *
     * These tests are pretty straightforward & do NOT use the ast verb or verb class at all (except passing it to the the templates) ... those more involved tests are integration tests & not every template needs to be tested that way ... just one or two
     *
     * @test ast/method template
     * @test ast/default template
     * @test ast/function_list template
     */
    public function testAstTemplates(){
        $this->test('ast/method & ast/default templates');
        // because testAstVerb() properly tests those templates ...
        $this->method_exists('Tlf\\Scrawl\\Test\\MdDocs', 'testAstVerb');


        $this->test('ast/function_list template');
        $scrawl = new \Tlf\Scrawl();
        $php_ext = new \Tlf\Scrawl\FileExt\Php($scrawl);
        $ast_ext = new \Tlf\Scrawl\Ext\MdVerb\Ast($scrawl);
        $ast = $php_ext->parse_str(<<<PHP
            <?php
            /** abc test description */
            function abc(){}
            /** def test description */
            function def(){}
        PHP);
        $ast['relPath'] = ':memory:';
        $md = $scrawl->get_template('ast/function_list', ['file.memory', $ast, $ast_ext]);

        $this->compare_lines(
            '# File :memory:
            ## Functions
            - `abc`: abc test description
            - `def`: def test description',
            $md
        );


        $this->test('ast/class template');
        $scrawl = new \Tlf\Scrawl();
        $php_ext = new \Tlf\Scrawl\FileExt\Php($scrawl);
        $ast_ext = new \Tlf\Scrawl\Ext\MdVerb\Ast($scrawl);
        $ast = $php_ext->parse_str(<<<PHP
            <?php
            class One {
                /** sure const */
                const sure = "sure";
                /** abc var */
                public \$abc = "okay";
                /** def test description */
                function def(){}
            }
        PHP);
        $md = $scrawl->get_template('ast/class', ['class.One', $ast['class'][0], $ast_ext]);

        $this->compare_lines(
            '# class One

            ## Constants
            - `const sure = "sure";` 

            ## Properties
            - `public $abc = "okay";` abc var

            ## Methods 
            - `function def()` def test description',
            $md
        );

        $this->test('ast/class_methods');
        $scrawl = new \Tlf\Scrawl();
        $php_ext = new \Tlf\Scrawl\FileExt\Php($scrawl);
        $ast_ext = new \Tlf\Scrawl\Ext\MdVerb\Ast($scrawl);
        $ast = $php_ext->parse_str(<<<PHP
            <?php
            class One {
                /** def test description */
                function def(){}
                function xyz(){}
            }
        PHP);
        $md = $scrawl->get_template('ast/class_methods', ['class.One', $ast['class'][0], $ast_ext]);


        $this->compare_lines(
            '- `$one->def()`: def test description'
            ."\n".'- `$one->xyz()`:',
            $md
        );
    }

}
