<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File ./Utility/DocBlock.php  
  
# class Tlf\Scrawl\Utility\DocBlock  
@featured  
  
## Constants  
  
## Properties  
- `static protected $regex = [  
                'DocBlock./**' => ['/((\/\*\*.*\n)( *\*.*\n)* *\*\/)/'],  
        'DocBlock./**.removeBlockOpen' => '/(^\/\*\*)/',  
        'DocBlock./**.removeLineOpenAndBlockClose' => '/(\n\s*\* ?\/?)/',  
    ];`   
  
## Methods   
- `public function __construct($rawBlock, $cleanBlock)`   
- `static public function DocBlock($name, $match, $nullFile, $info)`   
- `static public function extractBlocks($fileContent)`   
- `static public function cleanBlock($rawBlock)`   
  
