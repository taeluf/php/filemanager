<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File /test/run/Integrate.php  
  
# class Tlf\Scrawl\Test\Integrate  
  
See source code at [/test/run/Integrate.php](/test/run/Integrate.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function check_full_test(string $which)`   
- `public function testRunCli()`   
- `public function testRunFull()`   
- `public function testAllClasses()`   
- `public function testGenerateApiDir()` @test  
- `public function testGenerateApiDirPrototype()`   
- `public function testGetAllClasses()`   
- `public function testPhpExtWithScrawl()`   
  
