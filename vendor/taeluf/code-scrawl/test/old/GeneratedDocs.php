<?php

namespace Tlf\Scrawl\Test;


/**
 * Generates documentation & tests the output
 *
 */
class GeneratedDocs extends \Tlf\Tester {

    public $dir;

    public function prepare(){
        $this->dir = $root = dirname(__DIR__).'/input/Project';
        $readme_file = dirname($root).'/README.md';
        $this->empty_dir($root.'/docs/', true);
        if (is_file($readme_file))unlink($readme_file);

        $args = json_decode(file_get_contents($root.'/.config/scrawl.json'),true);
        $args['noprompt'] = true;
        $scrawl = new \Tlf\Scrawl($root, $args);
        $scrawl->generate_docs(null, $args);

    }

    public function testTemplate(){
        $file = $this->dir.'/docs/Template.md';
        $content = file_get_contents($file);

        $this->compare(
            // 2 spaces added for 'markdown.preserveNewLines' feature
            "## Template Test  "
            ."\none",
            $content
        );
    }

    public function testRegexMatching(){
        $file = $this->dir.'/docs/RegexTest.md';
        $content = file_get_contents($file);
        $this->compare(
            // needs 2 spaces after first line because of "markdown.preserveNewLines" setting
            "000defghijkl  "
            ."\njklghidef000",
            trim($content),
        );
    }

    public function testFilesPresent(){
        $files = [
            'README.md',
            'docs/exports/keys.md',
            'docs/exports/keys.php',
            'docs/README.md',
            'docs/Test.md',
            'docs/RegexTest.md',
        ];

        foreach ($files as $f){
            $this->is_file($this->dir.'/'.$f);
        }
    }

}
