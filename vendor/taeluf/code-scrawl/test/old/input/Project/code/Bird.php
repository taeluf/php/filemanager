<?php

/**
 * A class for doing bird stuff, like flying ...
 */
class Bird {

    public function fly(){
        echo "good";
    }

    public function die(){
        echo "not so good";
    }
}
