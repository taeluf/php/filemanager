<?php
/**
 * @param $args[0] the key pointing to the ast, must begin with `class.ClassName`
 * @param $args[1] a method ast array
 * @param $args[2] is the AstVerb class instance
 */
$key = $args[0];
$method = $args[1];
$ext = $args[2];

$method_name = $method['name'];
$class = $ext->get_ast($key, 2);
$class_name=$class['name'];
?>
# <?=$class_name?>::<?=$method_name?>  
<?=$method['docblock']['description']??'no description';?>

