<?php
/**
 * @param $args[0] the key pointing to the ast, must begin with `class.ClassName`
 * @param $args[1] array file ast
 * @param $args[2] the AstVerb class instance
 */
$File = $args[1];

?>
# File <?=$File['relPath']?>

## Functions
<?php
foreach ($File['functions'] ??[] as $function){
    $descript = $function['docblock']['tip'] ?? $function['docblock']['description'] ?? '';
    $name = $function['name'];
    echo "- `$name`: $descript\n";
}
