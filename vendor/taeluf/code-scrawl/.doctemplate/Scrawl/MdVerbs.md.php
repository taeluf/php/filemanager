<?php
/**
 * No params... uses $this to access scrawl
 */

ob_start();
$ext = $this->mdverb_ext;
$ast_verbs = new Tlf\Scrawl\Ext\MdVerb\Ast($this);

$verbs = [];
foreach ($ext->handlers as $verb=>$callable){
    $object = $callable[0];
    $method_name = $callable[1];
    $class = get_class($object);
    $ast = $this->get_class_ast($class);

    $method_ast = $ast_verbs->get_ast('class.'.$class.'.methods.'.$method_name);

    if (!isset($method_ast['docblock'])){
        $verbs[$verb] = "No description Found--".$method_name;
        continue;
    }

    $attributes = $method_ast['docblock']['attribute'];
    $usage = array_filter($attributes,
        function($v){if ($v['name']=='usage')return true; return false;}
    )[0]['description']??null;
    if ($usage!=null)$usage = ". Usage: `".$usage.'`';

    $output = array_filter($attributes,
        function($v){if ($v['name']=='output')return true; return false;}
    )[0]['description']??null;
    if ($output != null)$output = ". Output: `".$output."`";

    $description = trim($method_ast['docblock']['description'])
        .$usage
        .$output
        ;
    $verbs[$verb] = $description;
}
ob_get_clean();
// print_r($ext);
// exit;
// echo "\n\n\n-----------\n\n";
// print_r($verbs);
// echo "\n\n\n-----------\n\n";
// exit;
//
foreach ($verbs as $verb=>$description){
    echo "- `@$verb()`: $description  \n";
}
