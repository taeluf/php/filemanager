<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run//MdTemplates.php  
  
# class Tlf\Scrawl\Test\MdTemplates  
For testing all things `.md` file  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testBashInstall()`   
- `public function testComposerInstall()`   
- `public function testAstTemplates()` These tests are pretty straightforward & do NOT use the ast verb or verb class at all (except passing it to the the templates) ... those more involved tests are integration tests & not every template needs to be tested that way ... just one or two  
  
  
