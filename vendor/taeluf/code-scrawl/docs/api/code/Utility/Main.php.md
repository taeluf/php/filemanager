<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/Utility/Main.php  
  
# class Tlf\Scrawl\Utility\Main  
  
See source code at [/code/Utility/Main.php](/code/Utility/Main.php)  
  
## Constants  
  
## Properties  
- `static protected $classMap=[];`   
- `static protected $isRegistered=false;`   
  
## Methods   
- `static public function removeLeftHandPad($textBlock)`   
- `static public function allFilesFromDir(string $rootDir, string $relDir, array $forExt=[])`   
- `static public function DANGEROUS_removeNonEmptyDirectory($directory)`   
- `static public function getComposerPackageName()` Check your composer.json in the current working directory for a `"name"`  
- `static public function getCurrentBranchForComposer()`   
- `static public function getGitCloneUrl()`   
  
