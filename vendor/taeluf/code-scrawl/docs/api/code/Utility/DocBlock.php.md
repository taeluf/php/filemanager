<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/Utility/DocBlock.php  
  
# class Tlf\Scrawl\Utility\DocBlock  
@featured  
See source code at [/code/Utility/DocBlock.php](/code/Utility/DocBlock.php)  
  
## Constants  
  
## Properties  
- `public $raw;`   
- `public $clean;`   
- `static protected $regex = [  
                'DocBlock./**' => ['/((\/\*\*.*\n)( *\*.*\n)* *\*\/)/'],  
        'DocBlock./**.removeBlockOpen' => '/(^\/\*\*)/',  
        'DocBlock./**.removeLineOpenAndBlockClose' => '/(\n\s*\* ?\/?)/',  
    ];`   
  
## Methods   
- `public function __construct($rawBlock, $cleanBlock)`   
- `static public function DocBlock($name, $match, $nullFile, $info)`   
- `static public function extractBlocks($fileContent)`   
- `static public function cleanBlock($rawBlock)`   
  
