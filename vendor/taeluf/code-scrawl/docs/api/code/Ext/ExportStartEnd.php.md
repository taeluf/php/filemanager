<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/Ext/ExportStartEnd.php  
  
# class Tlf\Scrawl\FileExt\ExportStartEnd  
Export code between `// @export_start(key)` and `// @export_end(key)`  
@featured  
See source code at [/code/Ext/ExportStartEnd.php](/code/Ext/ExportStartEnd.php)  
  
## Constants  
  
## Properties  
- `protected $regs = [  
        'exports'=>   
                        '/\ *(?:\/\/|\#)\ *@export_start\(([^\)]*)\)((?:.|\r|\n)+)\ *(?:\/\/|\#)\ *(@export_end\(\1\))/',  
    ];`   
  
## Methods   
- `public function __construct()`   
- `public function get_exports($str)`   
  
