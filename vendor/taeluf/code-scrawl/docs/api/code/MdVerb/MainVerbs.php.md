<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/MdVerb/MainVerbs.php  
  
# class Tlf\Scrawl\Ext\MdVerb\MainVerbs  
  
See source code at [/code/MdVerb/MainVerbs.php](/code/MdVerb/MainVerbs.php)  
  
## Constants  
  
## Properties  
- `public \Tlf\Scrawl $scrawl;` a scrawl instance  
  
## Methods   
- `public function __construct(\Tlf\Scrawl $scrawl)`   
- `public function setup_handlers(\Tlf\Scrawl\Ext\MdVerbs $md_ext)` add callbacks to `$md_ext->handlers`  
- `public function at_system(string $system_command, ...$options)` Run a command on the computer's operating system.  
  
Supported options: `trim`, `trim_empty_lines`, `last_x_lines, int`  
  
- `public function at_template(string $templateName, ...$templateArgs)` Load a template  
- `public function at_import(string $key)` Import something previously exported with `@export` or `@export_start/@export_end`  
  
- `public function at_file(string $relFilePath)` Copy a file's content into your markdown.  
  
- `public function at_see_file(string $relFilePath, string $link_name = null)` Get a link to a file in your repo  
  
- `public function at_hard_link(string $url, string $name=null)` just returns a regular markdown link. In future, may check validity of link or do some kind of logging  
- `public function at_easy_link(string $service, string $target)` Get a link to common services (twitter, gitlab, github, facebook)  
  
  
