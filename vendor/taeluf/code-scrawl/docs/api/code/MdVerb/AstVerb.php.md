<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/MdVerb/AstVerb.php  
  
# class Tlf\Scrawl\Ext\MdVerb\Ast  
  
See source code at [/code/MdVerb/AstVerb.php](/code/MdVerb/AstVerb.php)  
  
## Constants  
  
## Properties  
- `public \Tlf\Scrawl $scrawl;`   
  
## Methods   
- `public function __construct($scrawl)`   
- `public function get_markdown($key, $template='ast/default')` Get an ast & optionally load a custom template for it  
  
- `public function get_ast(string $key, int $length=-1)`   
- `public function getVerbs(): array`   
- `public function getAstClassInfo(array $info, string $fqn, string $dotProperty)`   
- `public function verbAst($info, $className, $dotProperty)`   
- `public function getClassMethodsTemplate($verb, $argListStr, $line)`   
  
