# Code Scrawl Status

## Dec 20, 2022
- documented architecture
- made `$scrawl->mdverb_ext` accessible to bootstrap files
- `file.bootstrap` is now `require`d at start of `run()` so `$scrawl` can ALSO be accessed via `$this`.
- TODO: in scrawl->run(), add a simple hooks / extensions mechanism. Call hooked methods (or objects) on each source code file, and on each documentation source file, and at the end of run(). Possible other intermediate calls.
- TODO: Use `@‌template('all_classes')` or something similar to show the architecture. (need to write their docblocks and also change the template)
- TODO: In `.doctemplate/Scrawl/Templates.md.php`, add docblock descriptions, at least, to output

## Dec 18, 2022
- It doesn't look like I have a way to dynamically add php extension classes (such as the bash classes). See the Dec 10 notes for more information.
- Updated README to have a better flow & better detailed documentation
- TODO: Improve documentation of how to write extensions
- TODO: Setup dynamically adding php extension classes.
- TODO: Re-add `scrawl init` bin function

## Dec 10, 2022
- Extensions! I don't seem to have a way to set extensions in the config.json, since the only place extensions are dynamically checked for is in `parse_str()` & it expects an already instantiated object. in `Integrate.php::testPhpExtWithScrawl()`, extensions are dynamically added, but this isn't through configs or through the CLI. I'd like to fix this, so I can write extensions in other packages, then add them to the config, then have them run like they're supposed to. One solution is to add a simple callback function that can do things dynamically. So, when `Scrall->run()` is called, I could check the config for a `setup_file` or `setup_function`, then that file or function could setup scrawl however it needs to be setup. That allows more configurability, since it gives PHP access to scrawl when scrawl is run through CLI, and it is simpler because I don't have to come up with a new scheme for extension setup.

## TODO (apr 27, 2022)
- review old notes?? I think i already did the new structure? idk

## Goals
- rewrite scrawl to the new structure
- add an annotations feature that extracts all `@tags` from docblocks & comments throughout the code base & links them to asts (and/or files, maybe? idk)

## Versions
Lexing (ast generation) is not really ready
- v0.3 depends on lexer v0.6, which is abandoned but stable??
- v0.4 depends on lexer v0.2, which supports SIMPLE bash lexing & SIMPLE php lexing
- v0.5 depends on lexer v0.2, which is under development & has several failing tests
- v0.6 depends on lexer v0.7, the up & coming lexer
- v0.7 less extensible, much simpler codebase

## TODO
- IMPORTANT re-run `scrawl` on this repo & review it for errors & clean up the documentation
- IMPORTANT update default branch for code scrawl
- PLEASE write test for `scrawl init` (likely requires some internal changes)
- PLEASE make it so global `scrawl` command can successfully run `vendor/bin/scrawl`
- MAYBE add `@verb` escaping like `\@verb` or `@\verb` (probably `\@verb`)
- EH reorg some of the classes
- EH rework some of the naming
- EH rework some of the defaults
- MEH clean up the `bin/scrawl` script
- MEH review the `code/Old.php` file for ... stuff i might actually use
- MEH review the `old/GeneratedDocs.php` test class

## Latest
- Feb 15, 2022:
    - integration test for `scrawl->run()` (and write `run()`)
    - add integration test for running scrawl from cli (and modify `bin/scrawl`)
    - clean up the repo
- feb 14, 2022: 
    - api dir generation
    - all_classes template

## Feb 11, 2022 end of day
I was working on generating the `Api` folder ... prototyping it in a test `phptest -test generateApiDir`

i want to finish that prototype
then i want to make a more integrated test that basically puts that prototype into a more generic function on scrawl or the php ext ... idk

then i also want to do the `all_classes` template ... which would still require me to set the file ast on every class ... which would ... require additional processing, maybe setting file ast's to Scrawl or by pre-processing classes and adding file path to their asts before separating them FROM their file asts or by adding references back to their parent ast ... idk about that one

And once those "two" things are done, i'd like to write an integration test that ... runs code scrawl on a project directory (likely from the `test/input/` dir somewhere) ... 

THEN i can update to use the cli runner with that integrated code

THEN i can review old code & delete almost all of it

Woo heck hoo!

have a good time reed! Don't spend too much time on unimportant software. There are other priorities. This is important (to me), but far from crucial & probably not that important for the world ... but maybe the future will bear out something different & I'll find out that my awesomse software is awesome lol idk.

Love,
Reed <3


## Feb 11, 2022 (near end of day)
goals:
1. test all_classes template
2. write api docs to disk for every php file in the code dirs, matching the directory structure on disk

## Feb 10, 2022 end of day
TODO:
- NO write export list ext
- NO all docblocks ext? idk ... i might just skip it ...
- test all my templates

DONE:
- ast verb
- moved SimpleVerbs to new structure. Wrote tests for them
- cleaned up the repo (stuff i've covered already or that i don't need).
- 

Notes:
- code-old.bak is code-old before i started deleting things ... i really shouldn't need any of it
- test/old/ is ... old tests?? i think they might be useful. should review them


## Feb 10, 2022
I made the new branch ... and made notes on v0.6 ...
i want to just ... clean this up, i think ... remove all the old trash code so i can start centering around the new, good code ... then start rewriting and stuff

## Feb 8, 2022
- I've started a rewrite ... i'll need to make a new branch ... i'm still needing to finish `New/Ext/ExportStartEnd.php` and its tests ... Then I need to modify the existing rewrites & use the new version of BetterReg (Breg) ... just look at `code/New` & `test/run/Idk.php`

## Jan 28, 2022
I would really like to index all the `@whatevers` throughout comments & docblocks. Then be able to like, `$scrawl->get('feature', 'PhpFileHandler')` and have a reference to the docblock & property/method/class/etc on which `@feature(PhpFileHandler)` is declared

## Jan 25, 2022
- i updated the lexer integration code & the `classList` template so api docs are now being generated ... though still missing many important features

## Dec 6, 2021 (end of day):
TODO:
- after review, set this as default branch
- code/Scrawl.php needs major refactor (because it's just confusing)
- Extension stuff needs major refactor (because it's juts confusing)
- if config file does not exist, then prompt/ask if init is desired

NOTES:
- tests are passing
    - autoloading MIGHT not be working correctly ... I'm not sure
- cli implementation is totally based upon cli lib now
- composer.json is correct
- nothing lexer/ast related has been reviewed
- `scrawl` and `scrawl init` both work
- `.doctemplate` is implemented
- Docs are largely written

Extra TODO:
- test each template (mainly just to have example usage)


## v0.6: Up & coming scrawl
Just made new branch. TODO:
- general cleanup
- setup new cli lib
- fix all tests


---

## Copied from ... v0.4? Pretty sure
Basically, its in a very messy state and it needs some real time & attention to clean everything up.

- My lexer isn't properly parsing php. I need to fix this. Especially look at Grammar.php's ast. It IS getting `onLexerStart()` method, but its being recorded as a function in the file, not a method of the class.
- I just started updating the `verbAst` function (old implementation for `@ast()`), but didn't finish. Right now I just have it calling the method used for `@ast_class()`, but I haven't confirmed it due to the aforementioned lexer issues
- My templates verb function also needs updated to the latest version.

### Ideas
- shorthand extension declaration for built-in extensions.
    - `Lang\\*` to load all language extensions
    - `MdVerb\\Ast` for Ast extension (instead of `\Tlf\Scrawl\Ext\MdVerb\Ast`)
- Remove the `key=>value` pairing & switch to pure array of class names (and shorthands)
- a `disable` key to disable certain extensions that are automatically setup. 
- `@ast(type, key, astprop.whatever)`. Example: `@ast(class, \Tlf\Scrawl, methods.__construct.docblock)` will print a `trim`med docblock into the md file
- `@todo()` to print all TODOs found in the code base
    - `@TODO(*)` to output all TODOs (or something like it)
    - `@TODO(rel/file.ext)` to output all TODOs from `rel/file.ext`
- Configs for what generated output files should be written
- `@see_class(ClassName)` to output `[FqnClassName](LinkToClassFile)`
- `@see_function(FunctionName)` to output `[function_name()](LinkToFunctionFile)`
- `@see_file(Rel/FileName)` to output a link to the file (allows scrawl to report errors, so you know the link stays valid)
- `filename.src.md.php` to execute as php file, THEN run code scrawl
    - `filename.src.php.md` to run code scrawl THEN execute as php file
- `@see_dependencies()` to show links to all dependencies (as declared in composer.json)
- add optional link checking (`curl` every link to make sure they are valid)


### Latest (newest to oldest)
- changed `lex` setting to `lex.php` and `lex.bash` and now it defaults to `false`
- `@ast_class` works! `@ast` does too, but its currently just an alias for @ast_class
- add error reporting via `$scrawl->error('header', $msg)`
- Modify mdverb regex to allow `@verb()s` anywhere, not just at the start of a line. & remove requirement for rest of line to be whitespace.
- add `@easy_link(twitter, TaelufDev)` mdverb.
- Added `composer_install` template
- Added `@template(template_name, ...args)` as part of `Md/SimpleVerbs` extension
- Add `lex` config to conditionally disable the lexer.
- Md verb methods now receive a list of args instead of an argListString. The about the match is passed in an array as the first arg
- Vastly improved `@ast()`
- Refactored namespaces & other stuff. Much cleaned up! 
- Refactored to `interface`-driven extension system (rather than key-based)
- Clean up documentation files
- `@file(relative/path/to/file.whatever)` to include the trimmed contents of a file
- Cached lexer asts (in Lexer)
- Minor fixes
- new ApiLexer extension integrates with the PHPGrammar of my lexer library
