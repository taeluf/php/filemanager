<?php

namespace Tlf\Scrawl\FileExt;

/**
 * Export code between `// @export_start(key)` and `// @export_end(key)`
 * @featured
 */
class ExportStartEnd {


    protected $regs = [
        'exports'=> 
            //For this regex: $1 is the key, $2 is the code, $3 is the @export_end line
            '/\ *(?:\/\/|\#)\ *@export_start\(([^\)]*)\)((?:.|\r|\n)+)\ *(?:\/\/|\#)\ *(@export_end\(\1\))/',
    ];

    public function __construct(){}

    public function get_exports($str){
        
        preg_match_all($this->regs['exports'], $str, $matches, PREG_SET_ORDER);
        // print_r($matches);

        $exports = [];
        foreach ($matches as $index=>$m){
            $exports[$m[1]] = \Tlf\Scrawl\Utility\Main::trimTextBlock($m[2]);
        }

        return $exports;
    }

}
