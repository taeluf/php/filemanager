<?php

namespace Tlf\Tester\Test;

class Runner extends \Tlf\Tester {

    /**
     * @test SOME of the return value from running a directory of tests
     */
    public function testRunnerResults(){

        $cli = $runner = new \Tlf\Tester\Runner(dirname(__DIR__).'/input/Runner/',['phptest']);

        $cli->load_inputs(json_decode(file_get_contents(dirname(__DIR__,2).'/src/defaults.json'),true));

        $cli->load_inputs(['dir.test'=>['run/']]);

        $results = $runner->run_dir($cli, $cli->args);

        unset($results['class']);
        $this->compare(
            ['pass'=>1,
            'fail'=>0,
            'tests_run'=>1,
            'disabled'=>0,
            'assertions_pass'=>1,
            'assertions_fail'=>0
            ],
            $results
        );

    }
}

