<?php

namespace Tlf\Tester;

/**
 * Assert exceptions
 */
trait Exceptions {

    public function catch($exceptionClass,$strict=false){
        $catcher = new \Tlf\Tester\ExceptionCatcher($exceptionClass);
        $this->catchers[] = $catcher;
        return $catcher;
    }

    public function throw($e){
        $list = $this->catchers;
        foreach ($list as $index => $cat){
            if ($cat->matches($e)){
                //@TODO allow one catcher to be re-used & just require that each exception be caught by at least one.
                unset($this->catchers[$index]);
                ob_start();
                $this->handleDidPass(true);
                ob_get_clean();
                // "Exception was caught";
                return true;
            }
        }
        $this->handleDidPass(false);
        echo "Taeluf\\Tester: An exception was not handled...";
        throw $e;
    }


}
