<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/Runner.php  
  
# class Tlf\Tester\Runner  
  
  
## Constants  
  
## Properties  
- `public $server_port = [];`   
  
## Methods   
- `public function backward_compatability()`   
- `public function get_server_host($name='main')` get the server host   
- `public function get_host_port(string $server_name): int`   
- `public function start_server($cli, $args)` Start a localhost server for testing  
- `public function init($cli, $args)` Copies sample test files into `getcwd().'/test'`  
- `public function require_directory($path)`   
- `public function run_dir($cli, $args)` executes all tests inside test directories (config `dir.test`)  
  
