<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/Utility.php  
  
# class Tlf\Tester\Utility  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `static public function xdotoolRefreshFirefox($switchBackToWindow = false)`   
- `static public function startOb()`   
- `static public function endOb($ob_level)`   
- `static public function getClassFromFile($file)` dependency on Util added on Dec 8, 2022.  
- `static public function getAllFiles($dir,$relativeTo='', $endingWith'')` Get all files in a directory. Does not return directories  
  
  
