<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/ExceptionCatcher.php  
  
# class Tlf\Tester\ExceptionCatcher  
  
  
## Constants  
  
## Properties  
- `protected $strict;`   
- `protected $targetClass;`   
- `protected $contains = [];`   
  
## Methods   
- `public function __construct($expectingClass, $strict=false)`   
- `public function containing($msg)`   
- `public function shortenMessage($msg)`   
- `public function matches($exception)`   
  
