# PHP tester status / notes

## Nov 6, 2023 
create v1.0 branch. It will be strongly typed, refactored, and just cleaned up in general.

- move cli script to bin/phptest
- move command setup & config loading to the Runner. 
- added config/phptest.json as an option for config file location
- add types and docblocks to Runner
- docblock some configs in the runner
- move code dir to src/

## Oct 10, 2023
Rewrote the system for running and calling test servers. I wanted to add optional bootstrap loading, and it was a mess, so I cleaned up the implementation. This probably will cause some breaking changes, but all the built-in tests (including multi-server tests) passed.

Significantly updated README

## Apr 12, 2022
- Server: add curl_post() to get body, header_text, headers array & cookies array (cookies parsing to be implemented)

## Dec 8, 2021
- Doing major refactor of phptest ...
    - it is going well. I'm in code/Tester, code/Runner, and code/trait/Assertions. Mainly in Tester & Runner. I'm working on the run() loop. I just got test pass/fail printing how I want for individual test methods. But I'm getting an exception on line 127 of Runner. Run `phptest` to see the error. I just disabled the `startOb` & `endOb` that wraps `$tester->run()` (called in Runner) so the tester itself can output its results.
    - The rest of what I'm doing is in this vein ... having the printing closer to the actual happening. Simpler data structures. Easier to follow code.

- I need to:
    - print the class name from which tests are being run
    - make sure `-test` flag works
    - enable printing when only a specific test is being run (so it shows the full test's output)
    - Idunno... I think just finish the refactor

## Versions
- v0.3: Uses `taeluf/cli` library. adds server tests with help of `phptest server`. major refactor. new init setup
- v0.2: uses internal cli code. no server tests
- v0.1: Idunno

## Ideas
- Write default config file if not found
- Cache map of files to test & add `phptest scan` to re-scan (or `phptest --scan` to enable scanning)
- Assertions extensibility so its easy for any package to add its own assertions
