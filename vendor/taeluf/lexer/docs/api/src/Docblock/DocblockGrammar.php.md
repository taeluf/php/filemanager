<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Docblock/DocblockGrammar.php  
  
# class Tlf\Lexer\DocblockGrammar  
  
See source code at [/src/Docblock/DocblockGrammar.php](/src/Docblock/DocblockGrammar.php)  
  
## Constants  
  
## Properties  
- `public $directives = [  
  
        '/*'=>[  
            'start'=>[  
                'match'=>'/\\/\*/',  
                'buffer.clear',  
            ],  
            'stop'=>[  
                'match'=>'*/',  
                                'rewind 2',  
                'this:processDocblock',  
                'forward 2',  
                'buffer.clear',  
            ]  
        ],  
    ];`   
  
## Methods   
- `public function getNamespace()`   
- `public function onGrammarAdded($lexer)`   
- `public function onLexerStart($lexer, $ast, $token)`   
- `public function onLexerEnd($lexer, $ast, $token)`   
- `public function processDocblock($lexer, $ast, $token, $directive)`   
- `public function buildAstWithAttributes($lines)`   
- `public function cleanIndentation($body)` Remove indentation and * from docblock body   
  
