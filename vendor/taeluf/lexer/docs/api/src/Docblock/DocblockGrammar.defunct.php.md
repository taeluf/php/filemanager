<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Docblock/DocblockGrammar.defunct.php  
  
# class Tlf\Lexer\DocblockGrammar_Defunct  
I ran into a lot of issues while writing this grammar, so I took a completely different approach.  
  
I think there's some useful stuff here, but if a new Docblock Grammar is to be written, I think this should all be scrapped & a new setup designe.  
  
Be sure to read the notes in ` I ran into a lot of issues while writing this grammar, so I took a completely different approach.  
  
I think there's some useful stuff here, but if a new Docblock Grammar is to be written, I think this should all be scrapped & a new setup designe.  
  
Be sure to read the notes in `.dev` folder before developing this further!  
See source code at [/src/Docblock/DocblockGrammar.defunct.php](/src/Docblock/DocblockGrammar.defunct.php)  
  
## Constants  
  
## Properties  
- `public $directives = [  
        '/*'=>[  
            'start'=>[  
                'match'=>'/\\/\*/',  
                'buffer.clear',  
                'ast.new'=>[  
                    '_type'=>'docblock',  
                    '_addto'=>false,  
                                        '_setPrevious'=>'docblock',  
                ],  
                'then :@',  
                'then :\n',  
                'then :*',  
                'then :!*',  
                'then :*/'=>[  
                    'start'=>[  
                        'rewind 2',  
                        'ast.append description',  
                        'buffer.clear',  
                    ],  
                ],  
            ],  
            'stop'=>[  
                'match'=> '*/',  
                                                'lexer:popHead'  
            ]  
        ],  
        '*/'=>[  
            'start'=>[  
                'match'=>'*/',  
                'directive.pop 1',  
                'rewind 1',  
            ]  
        ],  
        '\n'=>[  
            'start'=>[  
                'match'=>'/(\r|\n)/',  
                'then :*',  
                'then :!*',  
            ],  
        ],  
        '*'=>[  
            'start'=>[  
                'match'=>"/\*[^\/]/",  
                                'rewind 2',  
                'buffer.clearNext 1',  
                'buffer.appendChar'=>' ',  
                'forward 1',  
                'stop',  
            ]  
        ],  
        '!*'=>[  
            'start'=>[  
                'match'=>'/[^\s\*]$/',  
                'rewind 1',  
                'buffer.clear',  
                'then :\n' =>[  
                    'start'=>[  
                                                'ast.append description',  
                        'buffer.clear',  
                                            ]  
                ],  
                'then :@',  
                'then :*/'=>[  
                    'start'=>[  
                        'rewind 2',  
                        'ast.append description',  
                        'buffer.clear'  
                    ],  
                ],  
            ],  
            'stop'=>[  
                'match'=>'*/',  
                'rewind 1',  
            ],  
        ],  
        '@'=>[  
            'start'=>[  
                'match'=>'@',  
            ]  
        ]  
  
    ];`   
  
## Methods   
- `public function onGrammarAdded($lexer)`   
- `public function onLexerStart($lexer, $ast, $token)`   
- `public function onLexerEnd($lexer, $ast, $token)`   
  
