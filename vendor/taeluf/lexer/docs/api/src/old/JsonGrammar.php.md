<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/old/JsonGrammar.php  
  
# class Tlf\Lexer\JsonGrammar  
This is an incomplete grammar, mainly used for testing & building v0.5 of the lexer. It gets "quoted values" and nested arrays.  
See source code at [/src/old/JsonGrammar.php](/src/old/JsonGrammar.php)  
  
## Constants  
  
## Properties  
- `public $directives = [  
  
        'file'=>[  
                                                'onStart'=>[  
                'rewind'=>1,  
                'then'=>[  
                    ':object',  
                    ':array',  
                    ':whitespace',  
                ],  
            ],  
        ],  
              
        'object' => [  
            'start'=>'{',  
            'onStart'=>[  
                'buffer.clear'=>true,  
                'ast.new'=>[  
                    '_setto'=>'root',  
                    'type'=>'object',  
                ],  
            ],  
            'stop'=>'}',  
            'onStop'=>  
            [  
                'pop_head'=>true,  
                'pop_directive'=>true,  
                'buffer.clear'=>true,  
            ],  
  
            'then'=>[  
                ':key'=>[],  
                ':whitespace'=>[],  
            ]  
        ],  
  
        'array' => [   
            'start'=>'[',  
            'onStart'=>[  
                'buffer.clear'=>true,  
                                'ast.new'=>[  
                    '_setto'=> 'root',  
                                        'type'=>"array",  
                ],  
                'then'=>[  
                                        ':array'=>[  
                        'onStart'=>[  
                            'buffer.clear'=>true,  
                                                        'ast.new'=>[  
                                '_addto'=>'value',  
                                                                'type'=>"array",  
                            ],  
                        ],  
                        'onStop'=>[  
                            'pop_head'=>true,  
                            'buffer.clear'=>true,  
                            'pop_directive'=>false,  
                            'then'=>[  
                                ':whitespace',  
                                ':comma',  
                                ':array.stop'=>[  
                                    'onStart'=>[  
                                        'pop_directive'=>true,  
                                        'pop_head'=>false,  
                                                                                                                    ]  
                                ],  
                            ]  
                        ],  
                    ],  
                    ':whitespace',  
                    ':value'=>[  
                        'onStop'=>[  
                            'rewind'=>1,  
                            'this:appendValueToAst',  
                            'forward'=>1,  
                            'buffer.clear'=>true,  
                              
                            'then'=>[  
                                ':whitespace',  
                                ':comma'=>[  
                                                                                                                                                                                                                                    ],  
                                ':array.stop'=>[  
                                    'onStart'=>[  
                                        'pop_head'=>false,  
                                        'pop_directive'=>true,  
                                        'rewind'=>1,  
                                                                            ]  
                                ],  
                            ],  
  
                        ],  
                    ],  
                    ':array.stop'=>[  
                        'onStart'=>[  
                            'pop_head'=>false,  
                            'pop_directive'=>true,  
                            'rewind'=>1,  
                                                    ]  
                    ],  
                ],  
            ],  
            'stop'=>']',  
            'onStop'=>[  
                'pop_head'=>true,  
                'pop_directive'=>true,  
                'buffer.clear'=>true,  
            ],  
  
        ],  
  
  
        'key'=>[  
                                    'start' => ["'", '"'],  
                'onStart'=>[  
                    'buffer.clear',  
                ],  
            'match'=>'/^$1([a-zA-Z0-9_\-]+)/',  
            'stop'=>'/$1$/',  
            'onStop'=>[  
                                                                'previous'=>'key',  
                'pop_directive'=>true,  
            ],  
  
            'then'=>[  
                ':whitespace',  
                ':colon'=>[  
                ],  
            ]  
        ],  
  
        'value'=>[  
            'is'=>[  
                                                ':str_value',  
            ],  
        ],  
  
          
                  
                'str_value'=>[  
            'start' => [  
                    "'",  
                    '"',  
                ],  
            'onStart'=>[  
                'buffer.clear',  
            ],  
                                    'match'=>[['/((?<!\\\\)[^' ,1, '])+/']],  
                        'stop'=>[['/[^\\\\]',1,'$/']],  
            'onStop'=>[  
                                                                                'buffer.clear'=>true,  
                  
                                'then'=>[  
                    ':comma'=>[  
                    ],  
                    ':whitespace',  
                ]  
            ],  
        ],  
  
        'comma'=>[  
            'start'=>',',  
            'onStart'=>[  
                'buffer.clear'=>true,  
                                                'pop_directive'=>true,  
            ],  
        ],  
  
        'colon'=>[  
            'match'=> ':',  
            'then'=> [  
                ':whitespace'=> [  
  
                ],  
                ':value'=>[  
                    'then'=>[  
                        ':comma'=>[  
                            'then'=>[  
                                ':key',  
                                ':object.stop'=>[  
                                    'bubble'=>true,  
                                ],  
                            ]  
                        ],  
  
                    ],  
                ],  
            ],  
        ],  
        'whitespace'=>[  
                                                'match'=>'/^\s+$/i',  
            'onStop'=>[  
                'buffer.clear'=>true,  
            ],  
        ],  
    ];`   
  
## Methods   
- `public function getNamespace()`   
- `public function getAstClass():string`   
- `public function onLexerStart($lexer,$file,$token)`   
- `public function appendValueToAst($lexer, $ast, $token)`   
  
