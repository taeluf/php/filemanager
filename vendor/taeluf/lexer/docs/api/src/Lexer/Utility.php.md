<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Lexer/Utility.php  
  
# class Tlf\Lexer\Utility  
  
See source code at [/src/Lexer/Utility.php](/src/Lexer/Utility.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `static public function trim_trailing_whitespace(string $str)` Trim trailing whitespace from each line  
- `static public function trim_indents(string $str)` Remove leading indents from every line, but keep relative indents  
  
