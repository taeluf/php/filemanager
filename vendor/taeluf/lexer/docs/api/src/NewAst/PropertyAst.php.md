<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/NewAst/PropertyAst.php  
  
# class Tlf\Lexer\Ast\PropertyAst  
  
See source code at [/src/NewAst/PropertyAst.php](/src/NewAst/PropertyAst.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function getTree($sourceTree = null)`   
- `public function getCode(string $language): string`   
- `public function get_php_code(): string`   
- `public function get_javascript_code(): string`   
  
