<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/NewAst/ClassAst.php  
  
# class Tlf\Lexer\Ast\ClassAst  
  
See source code at [/src/NewAst/ClassAst.php](/src/NewAst/ClassAst.php)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function getTree($sourceTree = null)`   
- `public function getCode(string $language): string`   
- `public function get_php_code(): string`   
- `public function get_javascript_code(): string`   
  
