<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Php/PhpGrammar.php  
  
# class Tlf\Lexer\PhpGrammar  
This is not for actual parsing yet. This is for design work. The $directives array & 'php_open' and 'namespace' are design aspects I'm interested in implementing at some point... maybe  
See source code at [/src/Php/PhpGrammar.php](/src/Php/PhpGrammar.php)  
  
## Constants  
  
## Properties  
- `public $directives;`   
- `public $notin = [  
        'keyword'=>[  
                        '__halt_compiler', 'abstract', 'and', 'array', 'as', 'break', 'callable', 'case', 'catch', 'class', 'clone', 'const', 'continue', 'declare', 'default', 'die', 'do', 'echo', 'else', 'elseif', 'empty', 'enddeclare', 'endfor', 'endforeach', 'endif', 'endswitch', 'endwhile', 'eval', 'exit', 'extends', 'final', 'for', 'foreach', 'function', 'global', 'goto', 'if', 'implements', 'include', 'include_once', 'instanceof', 'insteadof', 'interface', 'isset', 'list', 'namespace', 'new', 'or', 'print', 'private', 'protected', 'public', 'require', 'require_once', 'return', 'static', 'switch', 'throw', 'trait', 'try', 'unset', 'use', 'var', 'while', 'xor'  
        ],  
    ];`   
  
## Methods   
- `public function getNamespace()`   
- `public function buildDirectives()`   
- `public function onGrammarAdded($lexer)`   
- `public function onLexerStart($lexer,$file,$token)`   
  
