<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Ast.php  
  
# class Tlf\Lexer\Ast  
  
See source code at [/src/Ast.php](/src/Ast.php)  
  
## Constants  
  
## Properties  
- `protected $_path;`   
- `protected $_name;`   
- `protected $_ext;`   
- `public $_source;`   
- `protected $_tree = [];`   
- `public $_type;`   
- `protected $passthrough = [];`   
  
## Methods   
- `public function __construct($type, $startingTree=[])`   
- `public function removeAst($key, $ast)` Removes an ast if it is setto/addedto this ast. If $key points to an empty array, then $key is unset from this ast  
  
- `public function set($key,$value)` Set $value to $key, overwriting any previously set value  
- `public function append($key, $value)`   
- `public function setAll(array $keyValues)`   
- `public function add($key,$value, $subKey=false)` Add $value to an array  
- `public function push($key, $value, $subKey=false)` Alias for add   
- `public function has(string $key): bool` Check if key is set on this ast.  
- `public function get($key)`   
- `public function getAll()` Get the current tree as-is (asts are still asts)  
- `public function addPassthrough($ast)`   
- `public function getTree($sourceTree=null)` Get the tree as a pure array  
- `public function setTree($astTree)`   
- `public function __get($prop)`   
- `public function __set($prop, $value)`   
- `public function __isset($prop)`   
- `static public function __set_state($array)`   
  
