<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File src/Token.php  
  
# class Tlf\Lexer\Token  
  
See source code at [/src/Token.php](/src/Token.php)  
  
## Constants  
  
## Properties  
- `public $source;` the source string  
- `protected $remainder = false;`   
- `protected $buffer = false;`   
- `public $index = -1;` start position in source. First time next() is called, index becomes 0  
- `protected $len = 0;`   
- `protected $prevToken;`   
- `protected $matches =[];`   
- `protected $match;`   
- `public int $line_number = 0;` The line number currently being processed.   
Ticks up WHEN next() adds a \n, so line number will be increased while \n at top of buffer.  
Ticks down WHEN rewind() is called, for EACH \n in the rewound chars.  
  
## Methods   
- `public function __construct($source)`   
- `public function buffer()`   
- `public function remainder()`   
- `public function setBuffer($newBuffer)` Set a new buffer. May corrupt the token, but remainder is unchanged.  
  
- `public function append($str)`   
- `public function clearBuffer()`   
- `public function next()`   
- `public function rewind(int $amount)` Remove $amount characters from the buffer & prepend them to $remainder.  
- `public function forward(int $amount)` Move the pointer forward by `$amount` chars by repeatedly calling `$this->next()`  
- `public function match($index=false)`   
- `public function setMatch($match)`   
  
