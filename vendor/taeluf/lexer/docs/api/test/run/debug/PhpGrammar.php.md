<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/debug/PhpGrammar.php  
  
# class Tlf\Lexer\Test\Debug\PhpGrammar  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testMethodBodyIsString()`   
- `public function testScrawl2()` A block nested within a method caused the next method declaration to be incorect, so test all methods are correctly parsed in code-scrawl/Scraw2.php   
- `public function get_ast($file, $debug=true, $stop_loop-1): array` Get the ast of a file in `test/input/php/lex/`  
  
- `public function get_ast_methods(array $ast): array` Get the method names & declaration from an class ast  
  
  
