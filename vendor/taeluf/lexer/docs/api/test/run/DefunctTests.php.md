<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/run/DefunctTests.php  
  
# class Tlf\Lexer\Test\DefunctTests  
These are tests that either aren't really tests or that probably don't have a place any more  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function testMakeJson()` convert a tree into json to see if i like the output better  
This was never a test. Just a way to run some code.  
- `public function testPhpGrammarNew_SampleClass()` An old test I was using to design the new php grammar.  
It is just a mess now and i don't want to mess with it.  
  
