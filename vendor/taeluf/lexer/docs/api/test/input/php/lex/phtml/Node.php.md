<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/input/php/lex/phtml/Node.php  
  
# class Taeluf\PHTML\Node  
less sucky HTML DOM Element  
  
This class extends PHP's DOMElement to make it suck less  
  
The original version of this file was a pre-made script by the author below.  
The only meaningful pieces of code I kept are the two `if 'innerHTML'` blocks of code.  
No license information was available in the copied code and I don't remember what it said on the author's website.  
  
The original package had the following notes from the author:  
  
- Authored by: Keyvan Minoukadeh - http://www.keyvan.net - keyvan@keyvan.net  
  - See: http://fivefilters.org (the project this was written for)  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function __construct()`   
- `public function is(string $tagName): bool` is this node the given tag  
- `public function has(string $attribute): bool`   
- `public function attributes()` get an array of DOMAttrs on this node  
- `public function attributesAsArray()` return an array of attributes ['attributeName'=>'value', ...];  
- `public function __set($name, $value)` Used for setting innerHTML like it's done in JavaScript:  
@code  
$div->innerHTML = '<h2>Chapter 2</h2><p>The story begins...</p>';  
@endcode  
- `public function __unset($name)` if the node has the named attribute, it will be removed. Otherwise, nothing happens  
- `public function __isset($name)`   
- `public function __get($name)` Used for getting innerHTML like it's done in JavaScript:  
@code  
$string = $div->innerHTML;  
@endcode  
- `public function __toString()`   
- `public function xpath($xpath)`   
- `public function addHiddenInput($inputName, $value)` Adds a hidden input to a form node  
If a hidden input already exists with that name, do nothing  
If a hidden input does not exist with that name, create and append it  
  
- `public function boolAttribute($attributeName)` Find out if this node has a true value for the given attribute name.  
Literally just returns $this->hasAttribute($attributeName)  
  
I wanted to implement an attribute="false" option... but that goes against the standards of HTML5, so that idea is on hold.  
  
See https://stackoverflow.com/questions/4139786/what-does-it-mean-in-html-5-when-an-attribute-is-a-boolean-attribute   
  
- `public function getInnerText()`   
- `public function __call($method, $args)`   
  
