<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/input/php/lex/code-scrawl/Scrawl2.php  
  
# class Tlf\Scrawl2  
  
  
## Constants  
  
## Properties  
- `public array $stuff = [];` array for get/set  
- `public array $extensions = [  
        'code'=>[],  
    ];`   
- `public string $dir_docs = null;` absolute path to your documentation dir  
- `public string $dir_root = null;` absolute path to the root of your project  
- `public array $template_dirs = [  
  
    ];`   
- `public bool $markdown_preserveNewLines = true;` if true, append two spaces to every line so all new lines are parsed as new lines  
- `public bool $markdown_prependGenNotice = true;` if true, add an html comment to md docs saying not to edit directly  
  
## Methods   
- `public function __construct(array $options=[])`   
- `public function get_template(string $name, array $args)`   
- `public function process_str(string $string, string $file_ext)`   
- `public function addExtension(object $ext)`   
- `public function get(string $group, string $key)`   
- `public function get_group(string $group)`   
- `public function set(string $group, string $key, $value)`   
- `public function parse_str($str, $ext)`   
- `public function write_doc(string $rel_path, string $content)` save a file to disk in the documents directory  
- `public function read_file(string $rel_path)` Read a file from disk, from the project root  
- `public function report(string $msg)` Output a message to cli (may do logging later, idk)  
- `public function warn($header, $message)` Output a message to cli, header highlighted in red  
- `public function good($header, $message)` Output a message to cli, header highlighted in red  
- `public function prepare_md_content(string $markdown)` apply small fixes to markdown  
  
