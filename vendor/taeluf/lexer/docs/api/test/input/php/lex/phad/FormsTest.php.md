<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/input/php/lex/phad/FormsTest.php  
  
# class Phad\Test\Integration\Forms  
This class appears to test both form compilation and form submission  
  
  
## Constants  
  
## Properties  
- `protected array $blogTableColumns = ['id'=>'INTEGER PRIMARY KEY','title'=>'VARCHAR(200)', 'body'=>'VARCHAR(2000)'];`   
  
## Methods   
- `public function testDeleteItem()`   
- `public function testErrorMessage()`   
- `public function testSubmitDocumentation()`   
- `public function testControllerOnSubmitDocumentation()`   
- `public function testWithInlineOnSubmit()`   
- `public function testInsertWithValidOption()`   
- `public function testUpdateRedirectsToTarget()`   
- `public function testUpdateValid()`   
- `public function testInsertValid()`   
- `public function testSubmitInvalid()`   
- `public function testDisplayWithNoObject()`   
- `public function testHasSelectOptions()`   
- `public function testHasPropertiesData()`   
  
