<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/input/php/lex/MethodParseErrors.php  
  
# class Sample  
  
  
## Constants  
  
## Properties  
  
## Methods   
- `public function cats()`   
- `public function dogs($arg)`   
- `public function dogs2($arg=1)`   
- `public function dogs_3($arg='yes')`   
- `public function bears()` bears are so cute  
- `public function bears_are_best()` bears are so cute  
- `public function bears_do_stuff()`   
- `public function bears_cuddle_stuff()`   
- `public function bears_nest()`   
- `public function cleanSource($srcCode): string` Replaces inline PHP code with placeholder, indexes the placeholder, and returns the modified code  
  
- `public function makes_it_11()`   
- `public function output(): string`   
- `public function ok_13()`   
- `public function writeTo(string $file, $chmodTo=null): bool`   
- `public function yep_15()`   
- `public function __construct($html)`   
- `public function okay_17()`   
- `public function output2($withPHP=true)`   
- `public function now_19()`   
- `public function fill_php($html, $withPHP=true)`   
- `public function ugh_21()`   
- `public function create(string $tableName, array $colDefinitions, bool $recreateIfExists=false)`   
- `public function its_23()`   
  
