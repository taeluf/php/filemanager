<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File test/Tester.php  
  
# class Tlf\Lexer\Test\Tester  
  
  
## Constants  
  
## Properties  
- `protected $sample_thingies = [  
  
                'Values.CloseArgList'=>[  
                        'ast.type'=>'var_assign',  
                        'start'=>['php_code'],  
                        'input'=>'"This")',  
                        'expect'=>[  
                'value'=>'"This"',  
                'declaration'=>'"This"',  
            ],  
        ],  
  
        'Docblock.OneLine'=>[  
                        'start'=>['/*'],   
                        'input'=>"/* abc */",  
                        'expect.previous'=>[  
                                "docblock"=> [  
                    'type'=>'docblock',  
                    'description'=>'abc',  
                ],  
            ],  
        ],  
    ];` you pass an array like this to runDirectiveTest($grammars, $thingies)  
  
## Methods   
- `protected function runDirectiveTests($grammars, $thingies)` See above sample  
  
- `protected function parse(\Tlf\Lexer $lexer, string $toLex, array $startingDirectives, array $startingAst)` parse input and return an ast tree (without the ast type & the source)  
  
