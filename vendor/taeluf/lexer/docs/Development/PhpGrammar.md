<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# PHP Grammar  
This document is for the further development of the PHP Grammar.  
  
## Status  
- Language Support: Php 8.2  
- Parses:  
    - Classes, traits, interfaces, namespaces  
    - methods, functions, anonymous functions  
    - class propreties, class consts, method/function properties  
    - docblocks, comments  
    - return types, property types  
- Does NOT parse:  
    - expressions, for loops, method calls  
    - enums  
- Work being conducted:  
    - wrote `wd_enum` and the elseif in `unhandled_wd` to set the enum name. (*commented them out*)  
    - adding enum support requires parsing the `case`es & may have other syntax. Need a quick break down of the syntax & some tests.  
  
## Documentation   
- [Features Available](/test/output/PhpFeatures.md) - Overview of which features are implemented, tested, and passing.  
- [Architecture Description](/code/Php/README.md)  
  
## Overview  
You most likely will work on `Operations.php` or `Words.php` to add any new features. If you define or modify any directives, then you may want to add handlers in `Handlers.php`.  
  
To *define new operations*, add a symbol to the operations array in get_operations. It will look like `'&&'=>'and'`. Then define a method `op_and()`  
1. Define an operation handler like: `public function op_my_new_symbol($lexer, $ast, $xpn)`  
2. Add the symbol to `get_operations()`: `'&%'=>'my_new_symbol'`  
3. Fill out your operation handler  
  
To *define new words*, either:  
- define a word handler: `public function wd_enum($lexer, $xpn, $ast)`  
- or add a case to `unhandled_wd()`, like `else if ($ast->type == 'trait' && !isset($ast->name))`  
  
To *create a new handler*, definable in the directives:  
- `public function handleMyNewDirective($lexer, $ast, $token, $directive)`  
- Then, In a directive write `'this:handleMyNewDirective'`  
  
For *writing directives*, look at the CoreDirectives.php file and the [README](/README.md).  
  
## Code  
- [PhpGrammar.php](/code/Php/PhpGrammar.php) - Base class, implementing the lexer's callback methods, and `use`ing the directive & handler traits.  
- Directives  
    - [CoreDirectives.php](/code/Php/CoreDirectives.php)  
    - [Words.php](/code/Php/StringDirectives.php)  
- Handlers  
    - [Handlers.php](/code/Php/Handlers.php) - Defines methods callable by directives. Enables Operations & Words as simplified handlers.  
    - [Operations.php](/code/Php/Operations.php) - Handles all the symbols  
    - [Words.php](/code/Php/Words.php) - handles keywords & other non-string alphanumeric chars, like property names.  
  
## Testing  
- PHP directive test:   
    - `phptest -test ShowMePhpFeatures` for a summary of the directive tests  
        - see `test/output/PhpFeatures.md` or view in the terminal  
    - `phptest -test Directives -run Directive.TestName`   
        - add `-version 0.1` to skip new directive handling. Default in tests is `-version 1.0` which has new signal-based functionality. Default in production is `0.1` and in code use `$lexer->version \Tlf\Lexer\Versions::_1` for signal-based.  
        - add `-stop_loop 50` to stop after the 50th loop  
        - see [`test/src/Php`](/test/src/Php/) to create new directive test  
        - see [`test/Tester.php`](/test/Tester.php) - see the `$sample_thingies` at the top for an example. And see `runDirectiveTests()`, though idr how it works.  
  
## TODO  
- parse expressions  
- Document it better  
