<?php

namespace Tlf\Lexer\Ast;

class PropertyAst extends \Tlf\Lexer\Ast {

    public function getTree($sourceTree = null){
        $val = $this->get('value') ?? [];
        foreach ($val as $i=>$v){
            if (is_object($v)){
                $val[$i] = $v->getTree();
            }
        }
        return $val;
    }


    public function getCode(string $language): string{
        //var_dump($this);
        //exit;

        $code = '';
        switch($language){
            case 'php':
                $code = $this->get_php_code();
                break;
            case 'javascript': 
                $code = $this->get_javascript_code();
                break;
            default:
                throw new \Exception("Language '$language' is not valid");
        }
        return $code;
    }

    public function get_php_code(): string {
        $t = (object)$this->_tree;
        //var_dump($t);
        //exit;

        //$definition = <<<PHP
            //$modifiers $type \$$name $set $value;
        //PHP;

        $parts = [];
        foreach ($t->modifiers as $m){
            $parts[] = $m;
        }

        if (!empty($m->datatype))$parts[] = $m->datatype;
        
        $parts[] = "\${$t->name}";

        if (isset($t->value))$parts[] = "= ".var_export($t->value,true);

        $statement = implode(" ",$parts);

        $docblock = "";
        if (!empty($t->docblock)&&$t->docblock!=[]){
            $docblock = new DocblockAst($t->docblock['type'], $t->docblock);
            $statement = $docblock->get_php_code() . "\n" . $statement;
        }

        $statement .= ";";

        return $statement;
    }

    public function get_javascript_code(): string {
        $t = (object)$this->_tree;
        //var_dump($t);
        //exit;

        //$definition = <<<PHP
            //$modifiers $type \$$name $set $value;
        //PHP;

        $statement = "";
        $errors = [];

        $parts = [];
        foreach ($t->modifiers as $m){
            $errors[] = "\n//ERROR, TODO: property modifier '{$m}' cannot be output as javascript code on property {$t->name}.";
        }

        if (!empty($m->datatype))$errors[] = "\n//ERROR, TODO: datatype '{$t->datatype}' cannot be output in javascript on property {$t->name}";
        
        $parts[] = "{$t->name}";

        if (isset($t->value))$parts[] = "= ".var_export($t->value, true);


        $docbloc_code = "";
        if (!empty($t->docblock)&&$t->docblock!=[]){
            $docblock = new DocblockAst($t->docblock['type'], $t->docblock);
            $docbloc_code = $docblock->get_javascript_code();
        }
        $statement = 
            implode("\n", $errors)."\n"
            .$docbloc_code."\n"
            .implode(" ",$parts)
            .';';

        return $statement;
    }

}
