<?php

namespace Tlf\Lexer\Ast;

class ClassAst extends \Tlf\Lexer\Ast {

    public function getTree($sourceTree = null){
        $val = $this->get('value') ?? [];
        foreach ($val as $i=>$v){
            if (is_object($v)){
                $val[$i] = $v->getTree();
            }
        }
        return $val;
    }


    public function getCode(string $language): string{
        //var_dump($this);
        //exit;

        $code = '';
        switch($language){
            case 'php':
                $code = $this->get_php_code();
                break;
            case 'javascript': 
                $code = $this->get_javascript_code();
                break;
            default:
                throw new \Exception("Language '$language' is not valid");
        }
        return $code;
    }

    public function get_php_code(): string {
        $t = (object)$this->_tree;
        $class_definition = "";
        if (!empty($t->namespace))$class_definition .= "\nnamespace {$t->namespace}";
        //var_dump($t);
        //var_dump($this->_tree);

        $class_definition .= "\nclass {$t->name} extends {$t->extends} {";

        foreach ($t->properties as $property){
            $p = new PropertyAst($property['type'],$property);
            $class_definition .= "\n".$p->get_php_code();
        }

        $class_definition .= "\n\n}";

        return $class_definition;
    }

    public function get_javascript_code(): string {
        $t = (object)$this->_tree;
        $class_definition = "";
        if (!empty($t->namespace))$class_definition .= "\n// ERROR, TODO: Cannot convert namespace '{$t->namespace}' to javascript.";

        $class_definition .= "\nclass {$t->name}";

        if (isset($t->extends))$class_definition .= " extends {$t->extends}";

        $class_definition .= " {\n";

        foreach ($t->properties as $property){
            $p = new PropertyAst($property['type'],$property);
            $class_definition .= "\n".$p->get_javascript_code();
        }

        $class_definition .= "\n\n}";

        return $class_definition;
    }

}
