<?php

namespace Tlf\Lexer\Bash;

trait OtherDirectives {

    protected $_other_directives = [
        'bash'=>[
            'is'=>[
                ':comment',
                ':docblock',
                ':function',
            ],
        ],

        'docblock'=>[
            'start'=>[
                'match'=>'##',
                'rewind 2',
                'buffer.clear',
                'forward 2',
            ],
            'stop'=>[
                'match'=>'/(^\s*[^\#])/m',
                'rewind 2',
                'this:handleDocblockEnd',
                'buffer.clear',
                // 'forward 2'
            ]
        ],

        'function'=>[
            'start'=>[
                'match'=>'/(?:function\s+)?([a-zA-Z\_0-9]*)(?:(?:\s*\(\))|\s+)\{/',
                'this:handleFunction',
                'stop',
                'buffer.clear',
                'lexer:unsetPrevious docblock'
            ]
        ],

        'comment'=>[
            'start'=>[
                'match'=>'/#[^\#]/',
                'rewind 2',
                'buffer.clear',
                'forward 1',
                'ast.new'=>[
                    '_addto'=>'comments',
                    '_type'=>'comment',
                    'src'=>'_token:buffer',
                ],
                'buffer.clear //again',
            ],

            // I definitely need a comment parsing directive
            
            'match'=>[
                //then I have to decide whether I want to allow them with no parentheses.
                //And I do. It should work with or without.
                //So, heck. That complicates things a lot
                'match'=>'/@[a-zA-Z0-9]/',
                'rewind 1',
                'ast.append src',
                'rewind 1 // again',
                'ast.append description',
                'forward 2',
                'buffer.clear',
                'then :+'=>[
                    'start'=>[
                        //just immediately start
                        'match'=>'',
                        'rewind 1',
                    ],
                    'stop'=>[

                        'match'=>'/(\\r|\\n)/',
                        'rewind 1',
                        'ast.append src',
                        'buffer.clear',
                    ]
                ],

            ],
            'stop'=>[
                'match'=>'/(\\r|\\n)/',
                'rewind'=>1,
                'ast.append src',
                'ast.append description',
                'forward'=>1,
                'buffer.clear',
                'ast.pop',
                // 'directive.',
            ],
        ],
    ];
}
