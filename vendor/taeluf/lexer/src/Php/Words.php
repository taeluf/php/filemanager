<?php

namespace Tlf\Lexer\PhpNew;

use \Tlf\Lexer\Versions;

trait Words {

    public function unhandled_wd($lexer, $xpn, $ast, $word){

        if ($lexer->version >= Versions::_1){
            if ($lexer->signal == 'expect_var_name'){
                $ast->set('name', $word);
                $lexer->signal = 'expect_var_assign';
                return;
            }  else if ($lexer->signal == 'expect_var_name_in_assignment'){
                $ast->set('name', $word);
                $lexer->signal = 'expect_op_terminate';
                $lexer->popHead();
                // $xpn->push('declaration', $word);
                return;
            }
        }

        
        if ($ast->type=='property_name'||$ast->type=='arg_name'
            ||$ast->type=='var_name'
        ){
            // set variable/property name
            $ast->set('value', $word);
            $lexer->popHead();
        } 
        else if ($ast->type == 'method' && !isset($ast->name) 
            || $ast->type == 'function' && !isset($ast->name)){
            // set method/function name
            $ast->set('name', $word);
            $xpn->words = [];
            $xpn->last_word = null;
        } else if ($ast->type=='return_types'){
            // add a return type
            $ast->push('value', $word);
        }  else if ($ast->type == 'class' && !isset($ast->name)){
            // set class name
            $ast->set('name',$word);
            if (isset($ast->fqn))$ast->fqn .= $word;
            $xpn->words = [];
        } 
        // else if ($ast->type == 'class' && !isset($ast->name)){
        //     // set enum name
        //     $ast->set('name',$word);
        //     if (isset($ast->fqn))$ast->fqn .= $word;
        //     $xpn->words = [];
        // }
        else if ($ast->type=='class_extends'){
            // set the name of a class being extended??
            $ast->value = $word;
            $lexer->popHead();
        } else if ($ast->type == 'trait' && !isset($ast->name)){
            // set trait name
            $ast->set('name',$word);
            if (isset($ast->fqn))$ast->fqn .= $word;
            $xpn->words = [];
        } else if ($ast->type=='const_name'){
            // set const name
            $ast->value = $word;
            $lexer->popHead();
        }
    }

    public function wd_function($lexer, $xpn, $ast){
        if ($ast->type == 'class_body'){
            // its a method
            $method = new \Tlf\Lexer\Ast('method');
            $method->args = [];
            $this->docblock($method);
            $words = $xpn->words;
            array_pop($words); // remove 'function' 
            $method->modifiers = $words;
            $xpn->words = [];
            $lexer->setHead($method);
            // $ast->push('methods', $ast->get('type'));
            $ast->push('methods', $method);
        } else if (($ast->type=='file'||$ast->type=='namespace')
            &&( is_null($xpn->last_op)
                || $xpn->last_op == 'terminate'
                ||$xpn->last_op == 'block_end'
                ||$xpn->last_op == 'block_start'
            )

        ){

            $method = new \Tlf\Lexer\Ast('function');
            $method->args = [];
            $this->docblock($method);
            // $words = $xpn->words;
            // array_pop($words); // remove 'function'
            // $method->modifiers = $words;
            $xpn->words = [];
            $lexer->setHead($method);
            // $ast->push('methods', $ast->get('type'));
            $ast->push('functions', $method);



            // // var_dump($xpn->last_op);
            // // var_dump($xpn);
            // // exit;
            // // its a method
            // $function = new \Tlf\Lexer\Ast('function');
            // $function->args = [];
            // $this->docblock($function);
            // // $words = $xpn->words;
            // // array_pop($words); // remove 'function'
            // // $function->modifiers = $words;
            // $xpn->words = [];
            // $lexer->setHead($function);
            // // $ast->push('methods', $ast->get('type'));
            // $ast->push('functions', $function);
            // // $ast->set('body', new \Tlf\Lexer\StringAst('function_body'));
        }

    }
    
    public function wd_const($lexer,$xpn, $ast){
        if ($ast->type!='class_body')return;

        $const = new \Tlf\Lexer\Ast('const');
        $const->name = new \Tlf\Lexer\StringAst('const_name');
        $ast->push('const',$const);
        $lexer->setHead($const);
        $lexer->setHead($const->name);
        if (count($xpn->words)>1){
            $const->modifiers = array_slice($xpn->words,0,-1);
        }
        $xpn->words = [];

    }

    public function wd_namespace($lexer, $xpn, $ast){
        if ($ast->type!='file' && $ast->type != 'str'){
            $this->unhandled_wd($lexer, $xpn, $ast,$lexer->token->buffer());
            return;
        }

        $ns = new \Tlf\Lexer\Ast('namespace');
        $this->docblock($ns);
        $ast->set('namespace', $ns);
        $lexer->setHead($ns);
        $xpn->words = [];
    }

    public function wd_use($lexer, $xpn, $ast){



        if ($ast->type == 'function'){
            // echo "\n\n\n-----------\n\n";
            // var_dump($ast);
            // exit;
            $use_vars = new \Tlf\Lexer\Ast('use_vars');
            // $t
            $ast->push('use_vars', $use_vars);
            $lexer->setHead($use_vars);
            // $xpn->words = [];

        }

        // use_trait
        if ($ast->type=='file' || $ast->type == 'str' || $ast->type == 'namespace'){
            $trait = new \Tlf\Lexer\Ast('use_trait');
            $this->docblock($trait);
            $ast->push('traits', $trait);
            $lexer->setHead($trait);
            $xpn->words = [];
            return;
        }

    }

    // public function wd_enum($lexer,$xpn, $ast){
        // if ($ast->type!='file'&&$ast->type!='namespace')return;
        // $good_op =( is_null($xpn->last_op)
                // || $xpn->last_op == 'terminate'
                // ||$xpn->last_op == 'block_end'
                // ||$xpn->last_op == 'block_start');
        // if (!$good_op)return;
//
//
        // $enum = new \Tlf\Lexer\Ast('enum');
        // $this->docblock($enum);
//
        // if (count($xpn->words)>1){
            // $enum->modifiers = array_slice($xpn->words,0,-1);
        // }
        // if ($ast->type=='namespace'&&$ast->name!=''){
            // $enum->namespace = $ast->name;
            // $enum->fqn = $ast->name.'\\';
        // } else {
            // $enum->fqn = '';
            // $enum->namespace = '';
        // }
        // $xpn->words = [];
        // $lexer->setHead($enum);
        // $ast->add('enum', $enum);
    // }

    public function wd_class($lexer, $xpn, $ast){
        if ($ast->type!='file'&&$ast->type!='namespace')return;
        // var_dump($xpn);
        // exit;

        $good_op =( is_null($xpn->last_op)
                || $xpn->last_op == 'terminate'
                ||$xpn->last_op == 'block_end'
                ||$xpn->last_op == 'block_start');
        if (!$good_op)return;

        $class = new \Tlf\Lexer\Ast('class');
        $this->docblock($class);
        // echo 'zeep';
        // exit;

        if (count($xpn->words)>1){
            $class->modifiers = array_slice($xpn->words,0,-1);
        }
        if ($ast->type=='namespace'&&$ast->name!=''){
            $class->namespace = $ast->name;
            $class->fqn = $ast->name.'\\';
        } else {
            $class->fqn = '';
            $class->namespace = '';
        }
        $xpn->words = [];
        $lexer->setHead($class);
        $ast->add('class', $class);
    }

    public function wd_trait($lexer, $xpn, $ast){
        if ($ast->type!='file'&&$ast->type!='namespace')return;

        $trait = new \Tlf\Lexer\Ast('trait');
        if ($ast->type=='namespace'&&$ast->name!=''){
            $trait->fqn = $ast->name.'\\';
            $trait->namespace = $ast->name;
        } else {
            $trait->fqn = '';
            $trait->namespace = '';
        }
        $xpn->words = [];
        $lexer->setHead($trait);
        $ast->add('trait', $trait);

    }

    public function wd_extends($lexer, $xpn, $ast){
        if ($ast->type!='class')return;

        $extends = new \Tlf\Lexer\StringAst('class_extends');
        $ast->set('extends', $extends);
        $lexer->setHead($extends);
    }

}

