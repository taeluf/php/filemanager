<?php

namespace Tlf\Lexer;

/**
 *
 * This is not for actual parsing yet. This is for design work. The $directives array & 'php_open' and 'namespace' are design aspects I'm interested in implementing at some point... maybe
 *
 */
class PhpGrammar extends Grammar {

    use PhpNew\Handlers;
    use PhpNew\Operations;
    use PhpNew\Words;

    // use PhpNew\LanguageDirectives;
    // use PhpNew\ClassDirectives;
    // use PhpNew\ClassMemberDirectives;
    // use PhpNew\BodyDirectives;
    use PhpNew\StringDirectives;
    use PhpNew\CoreDirectives;

    public $directives;

    public $notin = [
        'keyword'=>[
            // 'match'=>'/this-regex-available on php.net keywords page/',
            '__halt_compiler', 'abstract', 'and', 'array', 'as', 'break', 'callable', 'case', 'catch', 'class', 'clone', 'const', 'continue', 'declare', 'default', 'die', 'do', 'echo', 'else', 'elseif', 'empty', 'enddeclare', 'endfor', 'endforeach', 'endif', 'endswitch', 'endwhile', 'eval', 'exit', 'extends', 'final', 'for', 'foreach', 'function', 'global', 'goto', 'if', 'implements', 'include', 'include_once', 'instanceof', 'insteadof', 'interface', 'isset', 'list', 'namespace', 'new', 'or', 'print', 'private', 'protected', 'public', 'require', 'require_once', 'return', 'static', 'switch', 'throw', 'trait', 'try', 'unset', 'use', 'var', 'while', 'xor'
        ],
    ];

    public function getNamespace(){return 'phpnew';}

    public function buildDirectives(){
        $this->directives = array_merge(
            // $this->_language_directives,
            // $this->_body_directives,
            // $this->_class_directives,
            // $this->_class_member_directives,
            $this->_string_directives,
            $this->_core_directives,
        );
    }

    public function onGrammarAdded($lexer){
        $this->buildDirectives();
        $lexer->addDirective($this->getDirectives(':php_open')['php_open']);
    }

    public function onLexerStart($lexer,$file,$token){
        $lexer->addGrammar(new DocblockGrammar());

        $token->append(' ');

        $xpn = new Ast('expression');
        $lexer->setPrevious('xpn', $xpn);

        // $this->buildDirectives();
        // $lexer->addDirective($this->getDirectives(':html')[':html']);
        // $lexer->stackDirectiveList('phpgrammar:html', 'phpgrammar:php_open');
        // $lexer->setDirective([$this->getDirective('html')]);

        if ($file->type=='file'){
            $file->set('namespace', '');
        }
    }



}
