# Php Grammar Overview
This is a cursory overview of the structure & general logic flow of the PhpGrammar 

The PhpGrammar uses a bunch of traits. 

All calls into php start with something defined in the directives.

Directives are defined in `CoreDirectives.php` and `StringDirectives.php`

Basically the `php_open` directive lists all the other directives that can be matched inside the root of php code (after `<?php`). These are things like `word`, `operation`, `string`, and `comment`.

The `word` directive is activated (`start`ed) when the token matches `'/^[a-zA-Z0-9\_\\\\]$/`. It is subsequently stopped upon matching the same, then we `rewind 1`, call `PhpGrammar->handleWord(...)` (which is in the `Handlers.php` trait), then clear the buffer.

`operation` matches for various symbols, and calls `handleOperation()`.

There are various other handlers in `Handlers.php` Some of them just do the one thing they need to do, like `handleWhitespace` just pushes the whitespace onto the declaration of the previous expression. `handleComment` for (`//` & `#` type comments) just pushes the token's buffer onto 'comments' on the head ast.

The directives themselves do additional operations before and/or after the php handler. `comment` directive will clear the buffer when the match starts, and will clear the buffer at the end, after `handleComment` is called. This means `handleComment` does not have to clear the buffer. The directive handles it.

It gets more complex with `handleWord` and `handleOperation` which are defined in `Handlers.php` trait. These call methods defined in the traits in `Words.php` and `Operations.php`. `trait Handlers` basically just calls `wd_'matched_string'` for words and `op_'matched_string'` for operations. Ex `wd_namespace` is called if the buffer is exactly `namespace`. `unhandled_wd` is called if there is no function named `wd_matched_string`.

`trait Operation` is the same way, except it is `op_word`. the `word` here comes from `Operations::get_operations()` which turns an array map where the key is the symbols and the value is the `word`. So the `word` can be things like `does_not_equal` for `!=` or `+ - * ** /` are all just `math`, so `op_math` gets called, if it is defined.

Some operations are set like `++` maps to `increment` or `function op_increment` but `function op_increment` does not exist. In this case, we just return. There is no php handler for unhandled operations. However, these unhandled operations will still be matched by the `operation` directive, and the buffer will be cleared after `handleOperation` returns.

