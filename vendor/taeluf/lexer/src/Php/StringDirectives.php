<?php

namespace Tlf\Lexer\PhpNew;

trait StringDirectives {

    public $_string_directives = [
        'string'=>[
            'is'=>[
                ':string_single',
                ':string_double',
                ':heredoc',
            ],
        ],


        'string_backslash'=>[
            'start'=>[
                'match'=>'/\\\\.$/',
                'stop',
                'halt.all',
                'this:handleStringBackslash',
            ],
        ],

        'string_instructions'=>[
            'start'=>[
                'rewind 1',
                'buffer.clear',
                'forward 1',
                'then :string_backslash',
            ],
            'stop'=>[
                // 'ast.push word',
                // 'previous.set string',
                'this:handleString',
                'rewind 1',
                // 'buffer.clear',
                'directive.pop',
            ]
        ],

        'string_single'=>[
            'start'=>[
                'match'=>"/^'$/",
                'inherit :string_instructions.start',
                'then :string_single.stop'=>[
                    'start'=>[
                        'inherit :string_instructions.stop',
                    ]
                ],
            ],
            'stop'=>[
                'match'=>"'",
                'buffer.clear',
            ],
        ],

        'string_double'=>[
            'start'=>[
                'match'=>'/^"$/',
                'inherit :string_instructions.start',
                'then :string_double.stop'=>[
                    'start'=>[
                        'inherit :string_instructions.stop',
                    ]
                ],
            ],
            'stop'=>[
                'match'=>'"',
                'buffer.clear',
            ],
        ],


        'heredoc'=>[
            // also capturing nowdoc
            'start'=>[
                'match'=>'<<<',
                'then :heredoc_key'
            ],
            'stop'=>[
                'rewind 1',
                'stop',
            ],
        ],

        'heredoc_key'=>[
            'start'=>[
                'match'=>'/\'?([a-zA-Z\_0-9]+)\'?[^a-zA-Z0-9_]/',
                'rewind 1',
                'previous.set heredoc_key !'=>'_token:match 1',
            ],
            'stop'=>[
                'match !'=>'_lexer:previous heredoc_key',
                'previous.set string',
                'buffer.clear',
                'directive.pop',
                // 'print' => "\n\n\n---\n",
                // 'die !' => '_lexer:previous string',
            ]
        ],

    ];

}
