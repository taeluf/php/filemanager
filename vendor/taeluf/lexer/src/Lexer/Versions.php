<?php

namespace Tlf\Lexer;

class Versions {

    /**
     * All code from before adding versioning
     */
    const _old = 0.1;
    /**
     * First versioned code. Adds parsing of statements, like assigning variables, calling methods, etc.
     */
    const _1   = 1.0;
}
