<?php

namespace Tlf\Lexer;

/**
 * @warning This likely works flawlessly, due to its not using many features, but I haven't tested it since doing some minor redesigns & improvements to the lexer
 */
class OldBashGrammar extends Grammar {


    protected $regexes = [

        'docblockStart'=>[
            'regex'=>'/(##.*)/',
            'state'=>[null, 'comment'],
        ],
        'docblockEnd'=>[
            'regex'=>'/(^\s*[^\#])/m',
            'state'=>['docblock'],
        ],
        'function'=>[
            'regex'=>'/(?:function\s+)?([a-zA-Z\_0-9]*)(?:(?:\s*\(\))|\s+)\{/',
            'state'=>[null],
        ],
        'comment'=>[
            'regex'=> '/#[^#]/',
            'state'=>[null],
        ],
        'commentEnd'=>[
            'regex'=> '/#[^\n]*\n/m',
            'state'=>['comment'],
        ]
    ];

    public function onLexerStart($lexer,$file,$token){
        // $file->set('namespace','');
    }

    public function onComment($lexer, $fileAst, $token){
        // echo "LEX COMMENT\n";
        // echo "PreState: ".$lexer->getState()."\n";
        $lexer->setState('comment');
        // echo "State: ".$lexer->getState()."\n";
        // echo "Comment Start: ".$token->buffer();
        // echo "\n";
    }
    public function onCommentEnd($lexer, $fileAst, $token){
        // echo "LEX COMMENT END\n";
        if ($lexer->getState()!='comment')return;
        // echo "PreState: ".$lexer->getState()."\n";
        $token->clearBuffer();
        $lexer->popState();

        // echo "State: ".$lexer->getState()."\n";
        // echo "Comment End: ".$token->match(0);
        // echo "\n";
    }


    public function onDocblockStart($lexer, $ast, $token){
        if ($lexer->getState()=='comment')$lexer->popState();
        $token->clearBuffer();
        $token->setBuffer($token->match(1));
        $lexer->setState('docblock');
    }
    public function onDocblockEnd($lexer, $ast, $token){
        $block = $token->buffer();
        $remLen = strlen($token->match(0));
        $block = substr($block,0,-$remLen);
        $block = preg_replace('/^\s*#+/m','',$block);
        $block = \Tlf\Scrawl\Utility::trimTextBlock($block);

            $docLex = new \Tlf\Lexer();
            $docLex->addGrammar(new \Tlf\Lexer\DocBlockGrammar());

            $docAst = new \Tlf\Lexer\Ast('docblock');
            $docAst->set('src', $block);
            $docLex->setHead($docAst);

            $docAst = $docLex->lexAst($docAst, $block);

        $lexer->setPrevious('docblock', $docAst);
        $ast->add('childDocblock', $docAst);
        $token->setBuffer($token->match(0));
        $lexer->setState(null);
    }
    public function onFunction($lexer, $file, $token){

        // echo "State: ".$lexer->getState()."\n";
        // echo "Function: ".$token->match(0);
        // echo "\n";

        $class = new Ast('function');
        $class->set('name',$token->match(1));
        $class->set('docblock', $lexer->unsetPrevious('docblock'));
        $file->add('function',$class);


        $token->clearBuffer();
    }
}
