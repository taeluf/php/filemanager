# Grammar Testing
Unit testing is the best way I've found to really develop a grammar. It allows you to focus on very specific pieces of the grammar at a time, then later put it all together by parsing an entire document.

This library depends upon @easy_link(tlf, php-tests) for testing.

Links:
- Writing a Grammar: @see_file(docs/GrammarWriting.md)
- Grammar Example: @see_file(docs/GrammarExample.md)
- Grammar Commands: @see_file(docs/GrammarCommands.md)

## Howto
1. Extend `\Tlf\Lexer\Test\Tester` which extends from `\Tlf\Tester` or copy the methods from it & modify as needed for a different testing library.
2. Implement a `testGrammarName()` method that uses the test runner
3. Implement the data structure for defining the unit tests.

Example of `2.`:
```php
@ast(Tlf\Lexer\Test\PhpGrammar, methods.testPhpDirectives.definition) {
@ast(Tlf\Lexer\Test\PhpGrammar, methods.testPhpDirectives.body)
}
```

Example of  `3.`:
-- whatever goes here

