## Cli command parsing
Options:
- call `$this->execCommand()`
    - If there is no namespace
- call `$grammars[namespace]->$command()`
    - if there is a namespace
- call `$targets[$namespace]->$command()`
    - if there is a namespace

Args options:
- `command` is an argument (only for `execCommand` / no namespace)
- `$phpArg` is `false` (do not run the command)
- `$phpArg` is `true` (run the command)
- `$phpArg` is a single argument appended to cli arguments (anything but `false`)
- `$phpArg` is an array of arguments. Give `...` as the last cli argument
- TODO `$phpArg` is an array of commands to run. Give `[]` as the last cli argument. `command` is prepended to each entry in the `$phpArg` array
- TODO `$phpArg` is an executable object+method+args. Pass `!` as the last cli argument, then use `_object:method arg1 arg2` like `_lexer:previous docblock`


So basically, we parse the `namespace:command` which gets us an object + method & MAY start an arg list. Then we parse the cli command args & add them to the list.
1: Create an args list from the cli args + php args
2: Get the object + method & modify args list as needed
3: Call object + method with the final args list


## Command Expansion
Add `command ...`, `command []`, and `command // abc dogs and cats` features
- default: `command abc=>[1,2,3]` is same as `command abc [1,2,3]`. (though arrays aren't allowed in the command shorthands. The array is a single argument)
- `...`: `command abc ...=>['d','e','f']` is same as `command abc d e f` 
- `[]`: `command abc []=>['a'=>true, 'b'=>'dog']` is same as `command abc a true` and `command abc b dog`
    - `command abc []=>['a','b']` is same as `command abc a` and `command abc b`
- `//`: `command abc // cats` is same as `command abc`. The `//` lets you write multiple matches, for example.
- `!`: `command def ! => '_lexer:previous docblock'` will call `command def $lexer->previous('docblock')`, essentially


## Directive Overrides 
I don't know if this is accurate, but I think it is & once verified, these notes can be used for documentation.

- Inheritance rules:
    - if raw `match` directive is first in src directive, then add it first
    - then add instructions from the child directive, in declared order
    - then add all other instructions from the source directive, in their declared order.
        - If child directive contains any keys found in source directive, then do not copy the value from the source directive. The child simply overwrites (but in the child's declared order)
