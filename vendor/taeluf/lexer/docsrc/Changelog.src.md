# ChangeLog

## Other Notes
- 2023-11-21: Cleaned up the repo, moved files around
- 2023-11-21: Added prototype outputting code from an ast

## Git Log
`git log` on @system(date, trim)`
@system(git log --pretty=format:'- %h %d %s [%cr]' --abbrev-commit)
