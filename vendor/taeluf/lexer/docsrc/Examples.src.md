# Lexer Examples

## Lex a file
```php
<?php
@import(Test.Doc.LexFile)
```
See @see_file(test/input/php/lex/SampleClass.php) for the input file and @see_file(test/output/php/tree/SampleClass.js) for the output `$tree`.

## Lex a string
This example is a bit more involved. `Docblock` is generally added by a programming language's grammar, so `Docblock` does not automatically start being listened for, the way `<?php` would be with the PhpGrammar.
```php
@import(Test.Doc.LexString)
```
The root ast contains the string, which we're not really interested in.

## Lex with your own root ast
This is basically what happens when you lex a file, EXCEPT `lexFile()` automatically handles caching, so subsequent runs on the same unchanged file will be loaded from cache. This approach ignores the cache completely.
```php
<?php
@import(Test.Doc.LexAst)
```
