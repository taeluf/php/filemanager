<?php

namespace Tlf\Lexer\Php;

trait ClassDirectives {

    protected $_class_directives = [

        'php_class_code'=>[
            'is'=>[
                ':separator',
                ':use_trait',
                ':class_modifier',
                // ':php_close',
                // ':php8_attribute',

            ],
        ],

        'use_trait'=>[
            'start'=>[
                'match'=>'use ',
                'buffer.clear'=>true,
            ],
            'stop'=>[
                'match' => ';',
                'rewind'=>1,
                'this:captureUseTrait',
                'forward'=>1,
                'buffer.clear'=>true,
            ],
        ],

        'class'=>[
            'stop'=>[
                'inherit :block.stop match',
                // 'directive.pop 1',
                'stop',
                'buffer.clear',
            ],
            'start'=>[
                'match'=>'/^(class|trait)[^a-zA-Z0-9_]$/',
                'rewind'=>1,
                'ast.new'=>[
                    '_type'=>'_token:buffer',
                    '_addto'=>'_token:buffer',
                    'docblock'=>'_lexer:unsetPrevious docblock',
                    'namespace'=>'_lexer:previous namespace.name',
                    'declaration'=>'_token:buffer',
                ],
                'buffer.clear'=>true,
                'then :separator'=>
                [
                    'start'=>[
                        'rewind 1 //okayokay',
                        'ast.append declaration',
                    ],
                    'stop'=>[
                        // 'inherit :separator.stop match',
                        // 'previous.append class.declaration !'=>'_lexer:unsetPrevious whitespace',
                        'rewind 1',
                        'ast.append declaration',
                        'forward 1',
                        'directive.pop 1',
                        'then :class_name',
                        'then :separator'=>[
                            'stop'=>[
                                'rewind 1',
                                'ast.append declaration',
                                'forward 1',
                            ],
                        ],
                    ]
                ]
            ]
        ],

        'class_name'=>[
            'start'=>[
                'inherit :varchars.start match',
                'start',
                'ast.append declaration !'=>'_lexer:previous php.varchars',
                'ast.set name !'=>'_lexer:unsetPrevious php.varchars',
                'buffer.clear',
                //@todo catch extends & implements
                'then :class_block',
                'then :separator'=>[
                    'start'=>[
                        'rewind 1',
                        'ast.append declaration',
                        'buffer.clear',
                        'forward 1',
                    ],
                    'stop'=>[
                        'rewind 1',
                        'ast.append declaration',
                        'forward 1',
                    ],
                ],
            ],
        ],

        'class_block'=>[
            'start'=>[
                'inherit :block.start match',
                'start',
                'rewind'=>1,
                'ast.append declaration',
                'forward 1',
                'buffer.clear'=>true,
                'then :php_class_code',
                'then.pop :block.stop',
            ],
            'stop'=>[
                'rewind 1',
                'buffer.clear'=>true,
                'directive.pop 2',
            ],
        ],

    // close the directive
    ];

}

