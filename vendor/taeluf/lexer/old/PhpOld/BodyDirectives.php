<?php

namespace Tlf\Lexer\Php;

trait BodyDirectives {

    protected $_body_directives = [

        'namespace'=> [
            'start'=>[
                'match'=>'/^namespace\s$/',
                // 'directive.stop_others'=>true,
                'rewind'=>1,
                'buffer.clear'=>true,
                //@todo change :whitespace back to :separator
                'then :whitespace'=>[
                    'stop'=>[
                        'directive.pop 1',
                        'then :separator',
                        'then :varchars' => [
                            'start'=>[
                                'rewind'=>1,
                                'this:holdNamespaceName',
                                'buffer.clear',
                                'stop',
                                // 'then :separator', // this was just for debug
                                'then :separator'=>[
                                    'stop'=>[
                                        'buffer.clear',
                                        // 'directive.pop 1',
                                    ]
                                ],
                                'then :+backslash'=>[
                                    'start'=>[
                                        'match'=> '\\',
                                        'stop',
                                        'directive.pop 1',
                                        'buffer.clear',
                                    ],
                                ],
                                'then :+semicolon'=>[
                                    'start'=>[
                                        'match'=>';',
                                        'rewind'=>1,
                                        'stop'=>true,
                                        'directive.pop'=>2,
                                    ],
                                ],
                                'halt',
                            ],
                            // 'stop'=>[
                                // 'stop',
                                // 'rewind 1',
                            // ],
                        ],
                    ],
                ],
            ],
            'stop' => [
                'match'=> ';',
                'this:saveNamespace',
                'buffer.clear'=>true,
            ],
        ],

        'function'=>[
            'start'=>'/^function$/',
            'onStart'=>[
                'buffer.clear'=>true,
                'ast.new'=>[
                    '_type'=>'function',
                    '_into'=>'functions',
                ]
            ],
        ],
        'interface'=>[
            'start'=>'/^interface$/',
            'onStart'=>[
                'buffer.clear'=>true,
                'ast.new'=>[
                    '_type'=>'interface',
                    '_into'=>'interfaces',
                ]
            ],
        ],
        // 'trait'=>[
            // 'start'=>'/^trait$/',
            // 'onStart'=>[
                // 'buffer.clear'=>true,
                // 'ast.new'=>[
                    // '_type'=>'trait',
                    // '_into'=>'traits',
                // ]
            // ],
        // ],

    //close directives
    ];

}
