<?php

namespace Tlf\Lexer\Php;

trait ClassMemberDirectives {

    protected $_class_member_directives = [

        'class_modifier'=> [
            'start'=>[
                'match'=>'/(static|public|protected|private) /',
                'rewind'=>1,
                'previous.append'=>['modifier'],
                'buffer.clear'=>true,
                'directive.stop_others'=>true,
                'then :class_modifier'=>[
                    'start'=>[
                        'rewind'=>1,
                        'previous.append'=>['modifier'],
                        'buffer.clear'=>true,
                        // 'directive.pop 1',
                        'stop',
                        'halt',
                    ]
                ],
                'then :separator'=>[
                    'stop'=>[
                        'rewind 1',
                        'previous.append'=>['modifier'],
                        'forward 1',
                    ],
                ],
                'then :class_const',
                'then :class_property',
                'then :varchars'=>[
                    'start'=>[
                        'rewind'=>1,
                        'previous.append'=>['modifier'],
                        'buffer.notin keyword',
                        'previous.set'=>'method.name',
                        // 'previous.append'=>['class.name', 'method.definition'],
                        'stop',
                        'directive.pop 1',
                        'then :class_method',
                        'then :separator'=>[
                            'stop'=>[
                                'previous.append'=>['method.definition'],
                            ]
                        ],
                    ]
                ],
                // 'then []'=>[
                    // ':separator',
                    // ':class_const',
                    // ':class_property',
                    // ':class_method',
                // ],
            ],
            'stop'=>[
                'stop',
                'rewind'=>1,
            ],
        ],

        
        'class_method'=>[
            'start'=>[
                'match'=>'(',
                'previous.append'=>'method.definition',
                'rewind'=>1,
                'ast.new'=>[
                    '_addto'=>'methods',
                    '_type'=>'method',
                    'modifiers'=>'_lexer:previous modifier',
                    'docblock'=>'_lexer:unsetPrevious docblock',
                    'name'=>'_lexer:unsetPrevious class.name',
                ],
                'forward'=>1,
                'buffer.clear'=>true,
                'then :argslist_close',
                'then :strings'=>[
                    'start'=>[
                        'previous.append'=>['method.definition','method.arglist'],
                        'buffer.clear'
                    ],
                    'stop'=>[
                        'previous.append'=>['method.definition','method.arglist'],
                        'buffer.clear'=>true,
                    ],
                ],
            ]
        ],

        'argslist_close'=>[
            'start'=>[
                'match' => ')',
                'rewind 1',
                'previous.append method.arglist',
                'forward 1',
                'previous.append method.definition',
                'buffer.clear',
                'stop',
                'directive.pop 1',
                'then :block' => [
                    'start'=>[
                        'rewind 1',
                        'buffer.clear',
                        'this:storeMethodDefinition',
                        'forward'=>1,
                        'buffer.clear true',
                        'then :block'=>[
                            'stop'=>[
                                'buffer.clear',
                            ]
                        ],
                        'then :block.stop'=>[
                            'start'=>[
                                'rewind'=>1,
                                'directive.pop 1',
                            ]
                        ],
                        'then :separator',
                        'then :strings'=>[
                            'stop'=>[
                                'buffer.clear'=>true,
                            ]
                        ],
                    ],
                    'stop'=>[
                        'directive.pop 2',
                        'ast.pop',
                        'buffer.clear'=>true,
                    ],
                ],
            ],
        ],

        'class_property'=>[
            'start'=>[
                'match'=>'$',
                'ast.new'=>[
                    '_addto'=>'properties',
                    '_type'=>'property',
                    'modifiers'=>'_lexer:previous modifier',
                    'docblock'=>'_lexer:unsetPrevious docblock'
                ],
                'rewind'=>1,
                'buffer.clear true'=>true,
                'forward'=>1,
                'previous.append'=>['property.declaration'],
                'buffer.clear'=>true,
                'then :varchars'=>[
                    'start'=>[ // capture property name
                        'rewind'=>1,
                        'previous.append'=>['property.declaration'],
                        'ast.set'=>'name',
                        'directive.pop 1',
                        'buffer.clear'=>true,
                        'then :separator'=>[
                            'start'=>[
                                'previous.append'=>['property.declaration'],
                            ],
                        ],
                        'then :+equals'=>[
                            'start'=>[
                                'match'=>'=',
                                'previous.append'=>['property.declaration'],
                                'buffer.clear'=>true,
                                'then :separator'=>[
                                    'stop'=>[
                                        'rewind 1',
                                        'previous.append'=>['property.declaration'],
                                        'forward 1',
                                    ],
                                ],
                                'then :strings'=>[
                                    'stop'=>[
                                        'previous.append'=>['property.declaration'],
                                        'buffer.clear'=>true,
                                    ],
                                ],
                                'then :+semicolon'=>[
                                    'start'=>[
                                        'match'=>';',
                                        'previous.append'=>['property.declaration'],
                                        'rewind'=>1,
                                        // 'ast.set'=>'declaration',
                                        'this:setPropertyDeclaration',
                                        'forward'=>1,
                                        'buffer.clear'=>true,
                                        'directive.pop'=>3,
                                        'ast.pop'=>true,
                                    ]
                                ]
                            ]
                        ],
                        'then :+semicolon'=>[
                            'start'=>[
                                'match'=>';',
                                'previous.append'=>['property.declaration'],
                                'rewind'=>1,
                                // 'ast.set'=>'declaration',
                                'this:setPropertyDeclaration',
                                'forward'=>1,
                                'buffer.clear'=>true,
                                'directive.pop'=>2,
                                'ast.pop'=>true,
                            ],
                        ],
                    ],
                ],
            ],
        ],

        'class_const'=>[
            'stop'=>[
                'stop',
                'rewind 1',
            ],
            'start'=>[
                'match'=>'const ',
                'ast.new'=>[
                    '_type'=>'const',
                    '_addto'=>'consts',
                    'docblock'=>'_lexer:unsetPrevious docblock',
                    'definition'=>'_token:buffer',
                    'modifiers'=>'_lexer:unsetPrevious modifier',
                ],
                'buffer.clear',
                'then :varchars'=>[ //catch the const's name
                    'start'=>[
                        'rewind 1',
                        'buffer.notin keyword',
                        'ast.append'=>'definition',
                        'ast.set'=>'name',
                        'buffer.clear'=>true,
                        // 'directive.pop 1',
                        'stop',
                        // 'halt',
                        'then :+equals-forconst'=>[
                            'start'=>[
                                'match'=>'=',
                                'ast.append'=>'definition',
                                'buffer.clear',
                                'then :separator'=>[
                                    'stop'=>[
                                        'ast.append'=>'definition',
                                    ],
                                ],
                                'then :strings'=>[
                                    'start'=>[
                                        'buffer.clear',
                                    ],
                                    'stop'=>[
                                        'ast.append'=>'definition',
                                        'buffer.clear',
                                    ],
                                ],
                                'then :+semicolon'=>[
                                    'start'=>[
                                        'match'=>';',
                                        'ast.append'=>'definition',
                                        'directive.pop 4',
                                        'buffer.clear',
                                    ],
                                ]
                            ],
                            'stop'=>[
                                'stop',
                                'rewind 1',
                            ]
                        ],
                    ],
                ]
            ]
        ],

    // close the directive
    ];

}

