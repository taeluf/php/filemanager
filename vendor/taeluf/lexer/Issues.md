## Issue 1, Scrawl2
```
// ////////
// // debugging
// ////////
// //
//
// // issue: addExtension's declaration is showing
//     // foreach ($extensions as $ext)
//     //     ---multiple blank lines---
//     // public function addExtension(object $ext)
// // issue recurs as long as the block `{}` or unterminated statement `foreach()` remains
// // if i terminate the block `{};` the issue goes away
// // foreach block open is loop 501
//
// // solution: the expression needed reset on block end, so i `lexer->setPrevious('xpn', new Ast())`
// // side effect: crashed Phad test due to `implode($xpn->declaration)` failing due to declaration NOT being an array
//
// $stop_loop = -1;
// $ast = $this->get_ast('code-scrawl/Scrawl2-partial.php',false, $stop_loop);
// $methods = $this->get_ast_methods($ast);
// print_r($methods);
//
// // print_r($ast);
//
// return;
// $this->run_parse_file('MethodParseErrors',true);
```
