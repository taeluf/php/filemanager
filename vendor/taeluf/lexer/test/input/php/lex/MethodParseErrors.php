<?php

class Sample extends cats {

    public function cats(){}
    public function dogs($arg){}
    public function dogs2($arg=1){}
    public function dogs_3($arg='yes'){}
    /** bears are so cute */
    public function bears(){}
    /** bears are so cute */
    public function bears_are_best(){
    
    }
    public function bears_do_stuff(){
        echo "love bears";
    }

    public function bears_cuddle_stuff(){
        $str = "resist the temptation to cuddle a bear. not safe. big sad";
        echo $str;
    }

    public function bears_nest(){
        $cats = 'run away from bears';
        if ($cats == 'idk'){
            echo "this is a block";
        }
    }

    /**
     * Replaces inline PHP code with placeholder, indexes the placeholder, and returns the modified code
     *
     * @param  mixed $srcCode - The source code
     * 
     * @return string - source code with all PHP replaced by codeIds
     */
    public function cleanSource($srcCode): string {
        $parser = new PHPParser($srcCode);
        $parsed = $parser->pieces();
    }

    public function makes_it_11(){}

    public function output(): string{
        // print_r($this->code);
        // // echo $this->code[0]->;
        // exit;
        // print_r($this->placeholder);
        $code = implode("\n",$this->code);
        // return $code;
        $ph = [];
        foreach ($this->placeholder as $id=>$codeArray){
            $ph[$id] = implode('',$codeArray);
        }
        $last = $code;
        while($last != $code = str_replace(array_keys($ph),$ph,$code))$last=$code;
        return $code;
    }

    public function ok_13(){}

    public function writeTo(string $file, $chmodTo=null): bool {
        $output = $this->output();
        if (is_dir(dirname($file))){
            mkdir(dirname($file),0771,true);
            // chmod(dirname($file),0770);
        }
        $didPut = file_put_contents($file,$output);
        if ($chmodTo!==null){
            // chmod($file,$chmodTo);
        }
        if ($didPut===false)return false;
        else return true;
    }

    public function yep_15(){}

    public function __construct($html)
    {
        parent::__construct();

        $this->srcHTML = $html;

        $parser = new PHTML\PHPParser($html);
        $enc = $parser->pieces();
        $this->php = $enc->php;
        $this->cleanSrc = $enc->html;
        $this->cleanSrc = $this->cleanHTML($this->cleanSrc);
        $hideXmlErrors=true;
        libxml_use_internal_errors($hideXmlErrors);
        $this->registerNodeClass('DOMElement', '\\Taeluf\\PHTML\\Node');
        $this->registerNodeClass('DOMText', '\\Taeluf\\PHTML\\TextNode');
        // $this->registerNodeClass('DOMText', 'RBText');

        $html = '<root>'.$this->cleanSrc.'</root>';
        $this->loadHTML($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
        $this->formatOutput = true;
        libxml_use_internal_errors(false);

    }    

    public function okay_17(){}

    public function output2($withPHP=true){
        // echo "\n".'-start output call-'."\n";
        $list = $this->childNodes[0]->childNodes;

        $hiddenTagsNodes = $this->xpath('//*[@hideOwnTag]');
        foreach ($hiddenTagsNodes as $htn){
            if ($htn->hideOwnTag==false||$htn->hideOwnTag=='false'){
                unset($htn->hideOwnTag);
                continue;
            }
            $parent = $htn->parentNode;
            $childNodeList = $htn->children;
            foreach ($childNodeList as $child){
                $htn->removeChild($child);
                $parent->insertBefore($child, $htn);
            }
            $parent->removeChild($htn);
        }
        $html = '';
        foreach ($list as $item){
            $html .= $this->saveHTML($item);
        }

        /** Run the php-code-replacer as long as there is a placeholder (while preventing infinite looping) */
        $html = $this->fill_php($html, $withPHP);
        
        $html = $this->restoreHtml($html);

        return $html;
    }

    public function now_19(){}

    public function fill_php($html, $withPHP=true){
        $maxIters = 25;
        $iters = 0;
        while ($iters++<$maxIters&&preg_match('/php([a-zA-Z]{26})php/', $html, $match)){
            foreach ($this->php as $id=>$code){
                if ($withPHP)$html = str_replace($id,$code,$html);
                else $html = str_replace($id,'',$html);
            }
        }

        if (($phpAttrVal=$this->phpAttrValue)!=null){
            $html = str_replace("=\"$phpAttrVal\"", '', $html);
        }
        return $html;
    }


    public function ugh_21(){}


    public function create(string $tableName, array $colDefinitions, bool $recreateIfExists=false){
        $colStatements = [];
        foreach ($colDefinitions as $col => $definition){
            $statement = '`'.$col.'` '. $definition;
            $colStatements[] = $statement;
        }
        $colsSql = implode(", ", $colStatements);
        $drop = $recreateIfExists ? "DROP TABLE IF EXISTS `{$tableName}`;\n" : '';
        $sql =
        <<<SQL
            {$drop}
            CREATE TABLE IF NOT EXISTS `{$tableName}`
            (
            {$colsSql}
            )
            ;
            
        SQL;

        $this->exec($sql);
    }

    public function its_23(){}
}
