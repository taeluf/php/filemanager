array (
  'type' => 'file',
  'ext' => 'php',
  'name' => 'SampleClass',
  'path' => '/home/reed/data/owner/Reed/projects/php/Lexer/test/php/SampleClass.php',
  'namespace' => 'Cats\\Whatever',
  'namespace.docblock' => NULL,
  'class' => 
  array (
    0 => 
    array (
      'type' => 'class',
      'docblock' => 
      array (
        'type' => 'docblock',
        'description' => 'This is the best class anyone has ever written.',
      ),
      'namespace' => 'Cats\\Whatever',
      'declaration' => 'class Sample extends cats ',
      'name' => 'Sample',
      'traits' => 
      array (
        0 => 'Some\\Demons',
      ),
      'comments' => 
      array (
        0 => 'First comment',
        1 => 'Second Comment',
      ),
      'properties' => 
      array (
        0 => 
        array (
          'type' => 'property',
          'modifiers' => 'protected ',
          'docblock' => 
          array (
            'type' => 'docblock',
            'description' => 'Why would you name a giraffe Bob?',
          ),
          'name' => 'giraffe',
          'declaration' => 'protected $giraffe = "Bob";',
        ),
        1 => 
        array (
          'type' => 'property',
          'modifiers' => 'private ',
          'docblock' => NULL,
          'name' => 'cat',
          'declaration' => 'private $cat = "Jeff";',
        ),
        2 => 
        array (
          'type' => 'property',
          'modifiers' => 'static public ',
          'docblock' => NULL,
          'name' => 'dog',
          'declaration' => 'static public $dog = "PandaBearDog";',
        ),
      ),
      'methods' => 
      array (
        0 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => 
          array (
            'type' => 'docblock',
            'description' => 'dogs
',
            'attribute' => 
            array (
              0 => 
              array (
                'type' => 'attribute',
                'name' => 'return',
                'description' => 'dogs',
              ),
            ),
          ),
          'name' => 'dogs',
          'definition' => 'public function dogs($a= "abc")',
          'arglist' => '$a= "abc"',
        ),
      ),
      'consts' => 
      array (
        0 => 
        array (
          'type' => 'const',
          'docblock' => NULL,
          'definition' => 'const Doygle = "Hoygle Floygl";',
          'modifiers' => 'public ',
          'name' => 'Doygle',
        ),
      ),
    ),
  ),
)