#!/usr/bin/env bash

cur_branch(){
    local branch;
    branch="$(git branch --show-current)";
    echo $branch;
}

is_cur_branch_behind(){
    curBranch="$(cur_branch)"
    git for-each-ref --format="%(refname:short) %(upstream:short)" refs/heads | \
    while read local remote
    do
        [ -z "$remote" ] && continue
        if [[ "$local" != "$curBranch" ]];then
            continue
        fi
        git rev-list --left-right ${local}...${remote} -- 2>/dev/null >/tmp/git_upstream_status_delta || continue
        # AHEAD=$(grep -c '^<' /tmp/git_upstream_status_delta)
        BEHIND=$(grep -c '^>' /tmp/git_upstream_status_delta)
        if [[ $BEHIND = 0 ]];then
            return 1;
        fi;
        return 0;
    done
}

cur_branch_behind_count(){
    local count;
    curBranch="$(cur_branch)"
    git for-each-ref --format="%(refname:short) %(upstream:short)" refs/heads | \
    while read local remote
    do
        [ -z "$remote" ] && continue
        if [[ "$local" != "$curBranch" ]];then
            continue
        fi
        git rev-list --left-right ${local}...${remote} -- 2>/dev/null >/tmp/git_upstream_status_delta || continue
        # AHEAD=$(grep -c '^<' /tmp/git_upstream_status_delta)
        count=$(grep -c '^>' /tmp/git_upstream_status_delta)
        echo "$count";
        return;
    done
}


branches_status(){

    # This was found on stackoverflow, but I lost the url. It will be modified
    # 
    # by http://github.com/jehiah
    # this prints out some branch status (similar to the '... ahead' info you get from git status)

    # example:
    # $ git branch-status
    # test (ahead 1) | (behind 112) origin/master
    # main (ahead 2) | (behind 0) origin/master
    # Method found on 

    # Another noteable resource is: https://stackoverflow.com/questions/17719829/check-if-local-git-repo-is-ahead-behind-remote

    curBranch="$(cur_branch)"
    git for-each-ref --format="%(refname:short) %(upstream:short)" refs/heads | \
    while read local remote
    do
        [ -z "$remote" ] && continue
        marker=""
        if [[ "$local" = "$curBranch" ]];then
            marker="*"
        fi
        git rev-list --left-right ${local}...${remote} -- 2>/dev/null >/tmp/git_upstream_status_delta || continue
        AHEAD=$(grep -c '^<' /tmp/git_upstream_status_delta)
        BEHIND=$(grep -c '^>' /tmp/git_upstream_status_delta)
        msg "${marker}${local}\n  - [ahead $AHEAD] [behind $BEHIND]"
        # on $remote"
    done
}

function branch_list_unique(){
    local -n output_branchList=${1};
    declare -A output_build_branchList
    output_branchList=()

    while read branch
    do
        isRemote=false
        branch="${branch#\'}"
        branch="${branch%\'}"
        name="${branch#refs/heads/}"
        if [[ "${name}" == "${branch}" ]];then
            name="${name#refs/remotes/origin/}"
            isRemote=true
        fi
        if [[ -z "${output_build_branchList["$name"]}" \
            && -z "${output_build_branchList["remote:$name"]}"  ]];then
            if $isRemote; then
                name="remote: ${name}"
            fi
            output_build_branchList["$name"]="$name"
            output_branchList+=("${name}")
        fi
    done <<< $(git for-each-ref --shell --format="%(refname)"  --sort='-authordate:iso8601' --sort='refname:rstrip=-2' )
    # ${@:1})

}
