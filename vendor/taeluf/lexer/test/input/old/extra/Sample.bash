#!/usr/bin/env bash

import core/core-update
import core/core-unsorted
import core/core-revert
# import core/core-url

## 
# Get aliases for core_ functions
#
function core_aliases(){
    declare -n al="$1"
    al=()
    
    ## Aliases to other groups
    al+=(s:"switch")
    al+=(del:"delete")
    al+=(dep:"depend")
    # al+=(check:"check")

    ## Aliases to "extra"
    al+=(url:"extra url")
    al+=(check:"extra check")
    al+=(status:"extra check")
    al+=(ignore:"extra ignore")
}

function core_tip_check(){
    echo "Check the status of your project"
}

function core_tip(){
    echo "Core commands for git bent. Stuff you need all the time??"
}

function core(){
    # run core help
    do_fancy_help "core"
}

function core_tip_help(){
    echo "View all functions and extended help"
}
# function core_help(){
    # run help
    # return
#    # echo "CORE HELP"
    # do_fancy_help "core"
#
#
    # return
##    @export_start(Internal.prompt_choose_function)
    # prompt_choose_function \
        # "# bent [command]" \
            # "run help" "help" "View all functions and extended help" \
            # "run core save" "save" "Save & upload your project" \
            # "run core upload" "upload" "upload your project (doesn't save first)" \
            # "run core update" "update" "Download all changes" \
            # "'" \
            # "run core tag" "tag" "Create a numbered release of your project" \
            # "run core revert" "revert" "Rollback to a previous save of your project" \
            # "run core merge" "merge" "Merge current branch into another branch" \
            # "run core check" "check" "Check the status of your project" \
            # "run core url" "url" "Get special urls to git hosts" \
            # "run core ignore" "ignore" "Download a .gitignore file from Github's gitignore repo" \
        # \
    # ;
##    @export_end(Internal.prompt_choose_function)
# }

function core_tip_save(){
    echo "Save & upload your project"
}
##
# Save your project
# @shorthand s, commit
#
function core_save(){
    is_project_dir || return;

    commitMsg="${@}"

    files_with_conflicts conflictingFiles
    if [[ "${#conflictingFiles[@]}" -gt 0 ]]; then
        msg_notice "Merge Conflicts in:"
        # msg
        for cf in "${conflictingFiles[@]}";do
            msg "  ${cf#*/}"
        done
        msg
        prompt_yes_or_no "Continue?" || return;
    fi

    # @TODO Ask which changed files should be saved

    changes="$(str_trim "$(changed_files)")"

    if [[ "$changes" == "" ]]; then
        msg "No changes, nothing to save."
        prompt_yes_or_no "Nothing to save. Upload anyway?" na \
            || return;
        run core upload
        return;
    fi

    msg
    header "Current Status"
    msg_status "$(project_dir)"
    run extra check

    msg

    changes=""
    if [[ "$commitMsg" == "" ]];then
        prompt_or_quit "Commit Msg ${cOff}or blank" changes -e \
            || return;
    else
        changes="$commitMsg"
    fi

    if [[ "$changes" == "" ]]; then
        changes="[no commit msg given]"
    fi
    git add -A
    git commit -m "${changes}"

    # @TODO integrate configs into save, upload? prompt
    # if I had a 'config' feature, I'd do 'config save_upload_answer prompt_answer', which would load the config named 'save_upload_answer' and store it in 'prompt_answer'
    # which would stop this prompt from happening
    # Possibly offer "yes, no, always, never" as options & save always/never to config
    # & `config "core save" "save_upload_answer" prompt_answer`
        # enables `bent config core save` to directly edit those config values
    prompt_yes_or_no "Upload?" \
        || return;
    run core upload
}

function core_tip_upload(){
    echo "Upload your project (without saving first)"
}
function core_upload(){
    is_project_dir || return;
    # @TODO only provide -u origin cur_branch when remote is not set, to avoid the message:
        # "Branch 'test-git-operations' set up to track remote branch 'test-git-operations' from 'origin'."

    run extra check 

    git push -u origin "$(cur_branch)" 

    run extra check 
}

function core_tip_tag(){
    echo "Tag your project to make a packaged release"
}
function core_tag(){
    is_project_dir || return;

    curBranch=$(project_dir)

    changed_files_array changedFiles
    if [[ "${#changedFiles[@]}" -gt 0 ]];then
        msg_notice "There are ${#changedFiles[@]} files with uncommitted changes"
    fi

    msg_header "Branch to tag"
    prompt_choose_branch branch || return

    prompt_or_quit "Tag name/version number (ex: 1.0.0)" release \
        || return;

    prompt_or_quit "Describe this tag/version: " description \
        || return

    git tag -a "$release" -m "$description" "$branch"
    git push --tags
    # git push origin :$branch
}
