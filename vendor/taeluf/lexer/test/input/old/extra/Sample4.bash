#!/usr/bin/env bash


ssh_keygen(){
    # is_project_dir || return;

    header "UNPOLISHED FEATURE"
    msg "  This feature is imperect, but it should work"

	# @TODO Add option for BitBucket, GitLab, Gitea
	# @TODO Improve language to use less jargon
	# @TODO Allow exiting with q/n/c 
	# @TODO report location of the key
	# @TODO allow naming the SSH key
	# @TODO add colors to the instructions
	# @TODO add instruction on how to load ssh keys

    # Setup SSH Key
    msg
    msg
    msg "An SSH key will securely sign you in without using your online password. \n    It saves you typing & is more secure.\n"
    prompt_yes_or_no "Configure an SSH key now? (y-yes/n-no)" \
        || return;

    #@TODO figure out why gitlab ssh key isn't working
    ssh_url=$(url ssh_key)

    cd ~/.ssh

    msg "\n1. Go to ${ssh_url}\n2. Click 'New SSH Key'\n3. Enter 'GitBent' for the title.\n4. Follow Prompts Below for the key.\n";
    
    msg_instruct "[ctrl-c] to exit this setup"
    prompt_or_quit "[enter] to continue" na -e \
        || return;

    sshFile="git_key_$RANDOM";

    msg "The SSH Key will be encrypted if you enter a password here. You SHOULD use a password. If you use a simple password, such as a PIN number, then it is very important that your computer is secure.\n"

    # read -sp "Enter encryption password (or leave blank): " sshPassword
    # echo ""
    # read -sp "Confirm encryption password: " sshPasswordConfirm
    # if [[ $sshPasswordConfirm != $sshPassword ]]; then
    #     echo "The passwords did not match. Run \`bent setup\` again."
    #     return;
    # fi
    msg ""
    msg "  --this might take a second or two--   "
    # if [[ $sshPassword == "" ]]; then
    #     echo "doing without password"
    #     keygen=$(echo "${sshFile}" | ssh-keygen -t rsa -b 4096 -C "git ${gitEmail}")
    # else 
    #     echo "Using password {$sshPassword}"
    #     command="ssh-keygen -t rsa -b 4096 -P \"${sshPassword}\" -f ${sshFile} -C \"git ${gitEmail}\"";
    #     echo $command;
    #     keygen=$command;
    # fi
    # ssh-keygen -t rsa -b 4096 -f ${sshFile} -C "git ${gitEmail}"
    ssh-keygen -t ed25519 -f "${sshFile}" -C "<comment>" -C "git ${gitEmail}"
    unset sshPassword;
    unset sshPassswordConfirm;
    
    msg ""
    msg "\n--Copy the public key file & put it online--"
    msg "  1. [enter] to print it here.\n  2. Highlight the long block of text\n  3. [ctrl+shift+c] to copy it (or right-click & copy)"
    msg "  4. Go back to the online SSH Key screen\n  5. Paste it into the 'key' field"
    msg ""
    prompt_or_quit "[enter] to continue" na -e \
        || return;
    msg ""
    msg ""
    pub="${sshFile}.pub"
    cat $pub;
    msg ""

    prompt_or_quit "[enter] to continue" na -e \
        || return;
    msg ""
    msg "  0. The long text above (including 'ssh-ed25519' and the comment at the end) should now be pasted into the 'Key' field online."
    msg "  1. Click 'Add SSH Key' online (skip for GitLab)."
    msg "  2. If you used an encryption password earlier, you will be prompted for it next."
    msg ""
    prompt_or_quit "[enter] to finish" na -e \
        || return;
    msg ""
    ssh-add ${sshFile}
    msg ""
    msg ""
    msg "Your git environment is setup now! Run \`bent help\` to get started managing your projects, their versions, and backups. "
    msg ""
    # echo $gitUser;
}


