#!/usr/bin/env bash


# function log_help(){
#
    # prompt_choose_function \
        # "# bent log [command] " \
        # "run log" "log" "Show previous saves" \
        # "run log diff" "diff" "Show previous saves along with changed files" \
    # ;
# }


##
# List commits
#
function log(){
    is_project_dir || return;
    # @TODO prompt and ask if they'd like to see file-level changes
    # @TODO Show instructions on navigating & exiting the output, then prompt (for [enter]) before continuing
    git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit

    msg_instruct "[bent log diff] to see more"
}

##
# List changes between commits
#
function log_diff(){
    is_project_dir || return;
    # @TODO merge into history(), via prompt...
    git log --graph --pretty=format:'%Cred%h%Creset -%C(yellow)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit -p
}
