<?php

return 
array (
  'type' => 'file',
  'ext' => 'php',
  'name' => 'StarterGrammar.16jul21',
  'path' => '/home/reed/.disk/Dev/php/Lexer/test/php/StarterGrammar.16jul21.php',
  'namespace' => 'Tlf\\Lexer\\Test',
  'namespace.docblock' => NULL,
  'class' => 
  array (
    0 => 
    array (
      'type' => 'class',
      'docblock' => 
      array (
        'type' => 'docblock',
        'description' => 'An extremely simple grammar that builds sets of arglist from `(arg1,arg2,c) (list2_arg1,arg2)`',
      ),
      'namespace' => 'Tlf\\Lexer\\Test',
      'declaration' => 'class StarterGrammar_16Jul21 extends Grammar ',
      'name' => 'StarterGrammar_16Jul21',
      'comments' => 
      array (
        0 => 'use Starter\\LanguageDirectives;',
      ),
      'traits' => 
      array (
        0 => 'Starter\\OtherDirectives',
      ),
      'properties' => 
      array (
        0 => 
        array (
          'type' => 'property',
          'modifiers' => 'protected ',
          'docblock' => 
          array (
            'type' => 'docblock',
            'description' => 'The actual array of directives, built during onGrammarAdded()',
          ),
          'name' => 'directives',
          'declaration' => 'protected $directives;',
        ),
      ),
      'methods' => 
      array (
        0 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => 
          array (
            'type' => 'docblock',
            'description' => 'Defaults to \'startergrammar\'',
          ),
          'name' => 'getNamespace',
          'definition' => 'public function getNamespace()',
          'arglist' => '',
        ),
        1 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => 
          array (
            'type' => 'docblock',
            'description' => 'Combine the directives from traits',
          ),
          'name' => 'buildDirectives',
          'definition' => 'public function buildDirectives()',
          'arglist' => '',
          'comments' => 
          array (
            0 => '$this->_language_directives,',
          ),
        ),
        2 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => NULL,
          'name' => 'onGrammarAdded',
          'definition' => 'public function onGrammarAdded(\\Tlf\\Lexer $lexer)',
          'arglist' => '\\Tlf\\Lexer $lexer',
          'comments' => 
          array (
            0 => 'you can add more directives',
          ),
        ),
        3 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => NULL,
          'name' => 'onLexerStart',
          'definition' => 'public function onLexerStart(\\Tlf\\Lexer $lexer,\\Tlf\\Lexer\\Ast $ast,\\Tlf\\Lexer\\Token $token)',
          'arglist' => '\\Tlf\\Lexer $lexer,\\Tlf\\Lexer\\Ast $ast,\\Tlf\\Lexer\\Token $token',
          'comments' => 
          array (
            0 => '$lexer->addGrammar(new DocblockGrammar()); //if your language uses docblocks',
          ),
        ),
        4 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => 
          array (
            'type' => 'docblock',
            'description' => 'A method this grammar uses as an instruction to trim() the buffer',
          ),
          'name' => 'trimBuffer',
          'definition' => 'public function trimBuffer(\\Tlf\\Lexer $lexer, \\Tlf\\Lexer\\Ast $ast, \\Tlf\\Lexer\\Token $token, \\stdClass $directive, array $args)',
          'arglist' => '\\Tlf\\Lexer $lexer, \\Tlf\\Lexer\\Ast $ast, \\Tlf\\Lexer\\Token $token, \\stdClass $directive, array $args',
        ),
      ),
    ),
  ),
)
;
