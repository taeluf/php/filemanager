array (
  'type' => 'file',
  'ext' => 'php',
  'name' => 'SampleClass',
  'path' => '/home/reed/data/owner/Reed/projects/php/Lexer/test/php-new/SampleClass.php',
  'namespace' => 
  array (
    'type' => 'namespace',
    'name' => 'Cats\\Whatever',
    'declaration' => 'namespace Cats\\Whatever;',
    'class' => 
    array (
      0 => 
      array (
        'type' => 'class',
        'namespace' => 'Cats\\Whatever',
        'fqn' => 'Cats\\Whatever\\Sample',
        'name' => 'Sample',
        'extends' => 'cats',
        'declaration' => 'class Sample extends cats',
        'traits' => 
        array (
          0 => 
          array (
            'type' => 'use_trait',
            'name' => 'Some\\Demons',
            'declaration' => 'use Some\\Demons;',
          ),
        ),
        'comments' => 
        array (
          0 => 'First comment',
          1 => 'Second Comment',
        ),
        'properties' => 
        array (
          0 => 
          array (
            'type' => 'property',
            'modifiers' => 
            array (
              0 => 'protected',
            ),
            'docblock' => 
            array (
              'type' => 'docblock',
              'description' => 'Why would you name a giraffe Bob?',
            ),
            'name' => 'giraffe',
            'value' => '"Bob"',
            'declaration' => 'protected $giraffe = "Bob";',
          ),
          1 => 
          array (
            'type' => 'property',
            'modifiers' => 
            array (
              0 => 'private',
            ),
            'name' => 'cat',
            'value' => '"Jeff"',
            'declaration' => 'private $cat = "Jeff";',
          ),
          2 => 
          array (
            'type' => 'property',
            'modifiers' => 
            array (
              0 => 'static',
              1 => 'public',
            ),
            'name' => 'dog',
            'value' => '"PandaBearDog"',
            'declaration' => 'static public $dog = "PandaBearDog";',
          ),
        ),
        'methods' => 
        array (
          0 => 
          array (
            'type' => 'method',
            'args' => 
            array (
              0 => 
              array (
                'type' => 'arg',
                'name' => 'a',
                'value' => '"abc"',
                'declaration' => '$a= "abc"',
              ),
            ),
            'docblock' => 
            array (
              'type' => 'docblock',
              'description' => 'dogs
',
              'attribute' => 
              array (
                0 => 
                array (
                  'type' => 'attribute',
                  'name' => 'return',
                  'description' => 'dogs',
                ),
              ),
            ),
            'modifiers' => 
            array (
              0 => 'public',
            ),
            'name' => 'dogs',
            'body' => 
            array (
            ),
            'declaration' => 'public function dogs($a= "abc")',
          ),
        ),
        'const' => 
        array (
          0 => 
          array (
            'type' => 'const',
            'name' => 'Doygle',
            'modifiers' => 
            array (
              0 => 'public',
            ),
            'value' => '"Hoygle Floygl"',
            'declaration' => 'public const Doygle = "Hoygle Floygl";',
          ),
        ),
      ),
    ),
  ),
)