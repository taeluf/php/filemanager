<?php

return 
array (
  'type' => 'file',
  'ext' => 'php',
  'name' => 'PhpGrammar.16jul21',
  'path' => '/home/reed/.disk/Dev/php/Lexer/test/php/PhpGrammar.16jul21.php',
  'namespace' => 'Tlf\\Lexer\\Test',
  'namespace.docblock' => NULL,
  'class' => 
  array (
    0 => 
    array (
      'type' => 'class',
      'docblock' => 
      array (
        'type' => 'docblock',
        'description' => 'This is not for actual parsing yet. This is for design work. The $directives array & \'php_open\' and \'namespace\' are design aspects I\'m interested in implementing at some point... maybe',
      ),
      'namespace' => 'Tlf\\Lexer\\Test',
      'declaration' => 'class PhpGrammar_16Jul21 extends Grammar ',
      'name' => 'PhpGrammar_16Jul21',
      'traits' => 
      array (
        0 => 'Php\\LanguageDirectives',
        1 => 'Php\\ClassDirectives',
        2 => 'Php\\ClassMemberDirectives',
        3 => 'Php\\BodyDirectives',
        4 => 'Php\\OtherDirectives',
      ),
      'properties' => 
      array (
        0 => 
        array (
          'type' => 'property',
          'modifiers' => 'protected ',
          'docblock' => NULL,
          'name' => 'directives',
          'declaration' => 'protected $directives;',
        ),
        1 => 
        array (
          'type' => 'property',
          'modifiers' => 'public ',
          'docblock' => NULL,
          'name' => 'notin',
          'comments' => 
          array (
            0 => '\'match\'=>\'/this-regex-available on php.net keywords page/\',',
          ),
          'declaration' => 'public $notin = [
        \'keyword\'=>[
             \'match\'=>\'/this-regex-available on php.net keywords page/\',            \'__halt_compiler\', \'abstract\', \'and\', \'array\', \'as\', \'break\', \'callable\', \'case\', \'catch\', \'class\', \'clone\', \'const\', \'continue\', \'declare\', \'default\', \'die\', \'do\', \'echo\', \'else\', \'elseif\', \'empty\', \'enddeclare\', \'endfor\', \'endforeach\', \'endif\', \'endswitch\', \'endwhile\', \'eval\', \'exit\', \'extends\', \'final\', \'for\', \'foreach\', \'function\', \'global\', \'goto\', \'if\', \'implements\', \'include\', \'include_once\', \'instanceof\', \'insteadof\', \'interface\', \'isset\', \'list\', \'namespace\', \'new\', \'or\', \'print\', \'private\', \'protected\', \'public\', \'require\', \'require_once\', \'return\', \'static\', \'switch\', \'throw\', \'trait\', \'try\', \'unset\', \'use\', \'var\', \'while\', \'xor\'
        ],
    ];',
        ),
      ),
      'methods' => 
      array (
        0 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => NULL,
          'name' => 'getNamespace',
          'definition' => 'public function getNamespace()',
          'arglist' => '',
        ),
        1 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => NULL,
          'name' => 'buildDirectives',
          'definition' => 'public function buildDirectives()',
          'arglist' => '',
        ),
        2 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => NULL,
          'name' => 'onGrammarAdded',
          'definition' => 'public function onGrammarAdded($lexer)',
          'arglist' => '$lexer',
        ),
        3 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => NULL,
          'name' => 'onLexerStart',
          'definition' => 'public function onLexerStart($lexer,$file,$token)',
          'arglist' => '$lexer,$file,$token',
          'comments' => 
          array (
            0 => '$this->buildDirectives();',
            1 => '$lexer->addDirective($this->getDirectives(\':html\')[\':html\']);',
            2 => '$lexer->stackDirectiveList(\'phpgrammar:html\', \'phpgrammar:php_open\');',
            3 => '$lexer->setDirective([$this->getDirective(\'html\')]);',
          ),
        ),
        4 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => NULL,
          'name' => 'holdNamespaceName',
          'definition' => 'public function holdNamespaceName($lexer, $file, $token)',
          'arglist' => '$lexer, $file, $token',
        ),
        5 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => NULL,
          'name' => 'saveNamespace',
          'definition' => 'public function saveNamespace($lexer, $file, $token)',
          'arglist' => '$lexer, $file, $token',
        ),
        6 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => NULL,
          'name' => 'handleClassDeclaration',
          'definition' => 'public function handleClassDeclaration($lexer, $class, $token)',
          'arglist' => '$lexer, $class, $token',
        ),
        7 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => NULL,
          'name' => 'processDocBlock',
          'definition' => 'public function processDocBlock($lexer, $ast, $token)',
          'arglist' => '$lexer, $ast, $token',
        ),
        8 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => NULL,
          'name' => 'captureUseTrait',
          'definition' => 'public function captureUseTrait($lexer, $ast, $token)',
          'arglist' => '$lexer, $ast, $token',
        ),
        9 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => NULL,
          'name' => 'processComment',
          'definition' => 'public function processComment($lexer, $ast, $token)',
          'arglist' => '$lexer, $ast, $token',
        ),
        10 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => 
          array (
            'type' => 'docblock',
            'description' => 'Do nothing, apparently? I thought it was supposed to append to previous(\'whitespace\'). Idunno',
          ),
          'name' => 'appendToWhitespace',
          'definition' => 'public function appendToWhitespace($lexer, $ast, $token, $directive)',
          'arglist' => '$lexer, $ast, $token, $directive',
          'comments' => 
          array (
            0 => '$whitespace = $lexer->previous(\'whitespace\') ?? \'\';',
            1 => '$lexer->setPrevious(\'whitespace\', $whitespace.$directive->_matches[0]);',
            2 => '$lexer->setPrevious(\'whitespace\', $whitespace.$token->buffer());',
          ),
        ),
        11 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => 
          array (
            'type' => 'docblock',
            'description' => 'Combine the stored modifier with the stored property declaration',
          ),
          'name' => 'setPropertyDeclaration',
          'definition' => 'public function setPropertyDeclaration($lexer, $ast, $token, $directive)',
          'arglist' => '$lexer, $ast, $token, $directive',
        ),
        12 => 
        array (
          'type' => 'method',
          'modifiers' => 'public function ',
          'docblock' => 
          array (
            'type' => 'docblock',
            'description' => '',
          ),
          'name' => 'storeMethodDefinition',
          'definition' => 'public function storeMethodDefinition($lexer, $ast, $token, $directive)',
          'arglist' => '$lexer, $ast, $token, $directive',
          'comments' => 
          array (
            0 => 'remove the method name from the modifiers.',
          ),
        ),
      ),
      'comments' => 
      array (
        0 => 'public function end_docblock($lexer, $unknownAst, $token){',
        1 => '$block = $token->buffer();',
        2 => '$block = trim($block);',
        3 => '$block = trim(substr($block,strlen(\'/**\'),-1));',
        4 => '$block = preg_replace(\'/^\\s*\\*+/m\',\'\',$block);',
        5 => '$block = \\Tlf\\Scrawl\\Utility::trimTextBlock($block);',
        6 => '$block = trim($block);',
        7 => '// if (substr($block,0,3)==\'/**\')$block = substr($block,3);',
        8 => '',
        9 => '$docLex = new \\Tlf\\Lexer();',
        10 => '$docLex->addGrammar(new \\Tlf\\Lexer\\DocBlockGrammar());',
        11 => '',
        12 => '$docAst = new \\Tlf\\Lexer\\Ast(\'docblock\');',
        13 => '$docAst->set(\'src\', $block);',
        14 => '$docLex->setHead($docAst);',
        15 => '',
        16 => '$docAst = $docLex->lexAst($docAst, $block);',
        17 => '',
        18 => '$lexer->setPrevious(\'docblock\', $docAst);',
        19 => '$unknownAst->add(\'childDocblock\', $docAst);',
        20 => '$token->setBuffer($token->match(0));',
        21 => '}',
      ),
    ),
  ),
)
;
