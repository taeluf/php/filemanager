
- +Property.Assign.Concat(pass): ` public $global_warming = "angers"."me"."greatly";`
- +Property.Two(pass): ` public $blm = "Yes!";private $cat="yep"; `
- +Property.Assign(pass): ` public $blm = "Yes!"; `
- +Property.Typed(pass): ` public int $blm;`
- +Property.Static(pass): ` static public $blm;`
- +Property.Simple(pass): ` public $blm;`
- +Arg.Assign.Concat.BeforeComma(pass): ` $global_warming = "angers"."me"."greatly", int $okay)`
- +Arg.Assign.Concat(pass): ` $global_warming = "angers"."me"."greatly")`
- +Arg.Two(pass): ` string $blm = "abc", bool $dog = true )`
- +Arg.Assign(pass): ` string $blm = "abc" )`
- +Arg.Typed(pass): ` string $blm )`
- +Arg.Simple(pass): `$blm)`
- +Namespace.Docblock(pass): `/** docs */ namespace AbcDef_09;`
- +Namespace.MultiLayer.IntermediateDocblocks(pass): `namespace/*k*/Abc\Def\_09;`
- +Namespace.MultiLayer(pass): `namespace Abc\Def\_09;`
- +Namespace.Simple(pass): `namespace AbcDef_09;`
- -Method.SimpleBody(fail): `    public function is_the_us_imperialist():bool {
        $var = new \Value();
        return true;
    } `
- +Method.ReturnReference(pass): `public function &abc() {`
- -Method.WithNestedBlock.2(fail): `    public function is_the_us_imperialist():bool {
        if ($you_buy_into_imperialist_propaganda){
            return false;
        } 
        return true;
    } 
    public function is_the_us_nice():bool {
        if ($you_are_rich){
            return true;
        } 
        return false;
    } `
- -Method.WithNestedBlock(fail): `    public function is_the_us_imperialist():bool {
        if ($you_buy_into_imperialist_propaganda){
            return false;
        } 
        return true;
    } `
- +Method.WithReturnType(pass): `static public function abc(bool $b): string {`
- +Method.TwoArgs(pass): `/* whoo */ private function abc($arg1, $arg2) {`
- +Method.OneArg(pass): `/* whoo */ public function abc(string $def=96) {`
- +Method.Simple.OneArg(pass): `public function abc($def) {`
- +Method.Static(pass): `static public function abc() {`
- +Method.Simple(pass): `public function abc() {`
- +Class.InNamespace(pass): `namespace Abc; class Def {}`
- +Class.Final.EmptyBody(pass): `final class Abc {

}`
- +Class.Implements(pass): `class Abc extends \Def\Ghi implements iAbc, iDef {`
- +Class.Extends(pass): `class Abc extends \Def\Ghi {`
- +Class.Abstract(pass): `abstract class Abc {`
- +Class.OpenInFile(pass): `class Abc {`
- +Class.OpenInNamespace(pass): `class Abc {`
- -Integration.Class.Methods.WithNestedBlock(fail): `    class A {
        public function is_the_us_imperialist():bool {
            if ($you_buy_into_imperialist_propaganda){
                if (true){
                    return false;
                }
            } 
            return true;
        } 
        public function is_the_us_nice():bool {
            if ($you_are_rich){
                if (true){
                    return true;
                }
            } 
            return false;
        } 
    } //`
- +Integration.Class.Final.Method(pass): `final class Abc {
    public function abc() {    }}`
- +Integration.Class.Bug.ModifiersMethods(pass): `abstract class Abc {
    public function `
- +Integration.Class.Method.2(pass): `class Abc {
    public function abc() {
    }    private function def() {
    }
}final class Def {
    public function abc() {
    }    protected function def() {
    }
}`
- +Integration.Trait.Method(pass): `trait Abc {
    public function abc() {    }}`
- +Integration.Class.Method(pass): `class Abc {
    public function abc() {    }}`
- +Trait.InNamespace(pass): `namespace Abc; trait Def {}`
- +Trait.EmptyBody(pass): `trait Abc {

}`
- +Trait.OpenInFile(pass): `trait Abc {`
- +Trait.OpenInNamespace(pass): `trait Abc {`
- +Open.Class(pass): `<html><div><?php class Abc {} `
- +Open.Close.Open(pass): `<html><div><?php ?></div><?php namespace Abc; `
- +Open.Simple(pass): `<html><div><?php namespace Abc;`
- +Const.Two(pass): ` const blm = "Yes!";private const cat="yep"; `
- +Const.Private(pass): `private const blm = 86;`
- +Const.Simple(pass): `const blm = "This"."is".'a'."const";`
- +Const.NoValue(pass): `const blm `
- +UseTrait.Docblock(pass): `/** docs */ use AbcDef_09;`
- +UseTrait.WithNamespace.IntermediateDocblocks(pass): `use/*k*/Abc\Def\_09;`
- +UseTrait.WithNamespace(pass): `use Abc\Def\_09;`
- +UseTrait.Simple(pass): `use AbcDef_09;`
- +Values.Arithmetic.Nesting(pass): `(1*2+(4*6/((1+2)+(3**2)))+3 - 12) / 3 ** 2 + 987;`
- +Values.Arithmetic(pass): `(1*2)+3 - 12 / 3 ** 2 + 987;`
- +Values.Arithmetic.Add(pass): `1 + 2;`
- +Values.Array.WithConcats(pass): `["a"."b"."c",22,"c"];`
- +Values.Array.Keyed(pass): `["a"=>1,22,"c"=>'third element'];`
- +Values.Array.IntStringValues(pass): `["a",22,"c"];`
- +Values.Array.StringValues(pass): `["a","b","c"];`
- +Values.Array.DoubleValue(pass): `[1.33,2,3];`
- +Values.Array.IntValues(pass): `[1,2,3];`
- +Values.BothQuotesConcat(pass): `"This"."is".'a'."const";`
- +Values.SingleQuotesConcat(pass): `'This'.'is'.'a'.'thing';`
- +Values.DoubleQuotesConcat(pass): `"This"."is"."a"."thing";`
- +Values.Double(pass): `1.87;`
- +Values.Int(pass): `1;`
- +Values.SemicolonStop(pass): `"This";`
- +Values.CommaSeparatedArg(pass): `"This",`
- +Values.CloseArgList(pass): `"This")`
- -Bug.namespace(fail): `    public function namespace_add(string $namespace) {
        $this->namespace[] = $namespace;
    } `
- +Function.Anon(pass): `    function protect_womens_rights() use ($abc){
        return;
    } `
- -Function.SimpleBody(fail): `    function is_the_us_imperialist():bool {
        $var = new \Value();
        return true;
    } `
- -Var.Assign.Variable(fail): `$bear = $red_racoon;`
- -Var.Assign.String(fail): `$bear = "barry";`