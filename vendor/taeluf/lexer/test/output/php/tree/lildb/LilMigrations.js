{
    "type": "file",
    "namespace": {
        "type": "namespace",
        "name": "Tlf",
        "declaration": "namespace Tlf;",
        "class": [
            {
                "type": "class",
                "docblock": {
                    "type": "docblock",
                    "description": "A minimal class for handling sql migration. Create a migrations dir. Then create dirs like `v1`, `v2` & create files `up.sql`, `down.sql` in each versioned dir. Migrating from 1 to 2 will execute `v2\/up.sql`. From 3 down to 1 will execute `v2\/down.sql` and `v1\/down.sql`. You may also make files like `v1\/up-1.sql`, `v1\/up-2.sql` to execute multiple files in order.\n",
                    "attribute": [
                        {
                            "type": "attribute",
                            "name": "tagline",
                            "description": "Easy to use SQL Migrations from versioned directories"
                        }
                    ]
                },
                "namespace": "Tlf",
                "fqn": "Tlf\\LilMigrations",
                "name": "LilMigrations",
                "declaration": "class LilMigrations",
                "properties": [
                    {
                        "type": "property",
                        "modifiers": [
                            "public",
                            "\\PDO"
                        ],
                        "docblock": {
                            "type": "docblock",
                            "description": "a pdo instance"
                        },
                        "name": "pdo",
                        "declaration": "public \\PDO $pdo;"
                    },
                    {
                        "type": "property",
                        "modifiers": [
                            "public",
                            "string"
                        ],
                        "docblock": {
                            "type": "docblock",
                            "description": "the dir for migrations scripts."
                        },
                        "name": "dir",
                        "declaration": "public string $dir;"
                    }
                ],
                "methods": [
                    {
                        "type": "method",
                        "args": [
                            {
                                "type": "arg",
                                "arg_types": [
                                    "\\PDO"
                                ],
                                "name": "pdo",
                                "declaration": "\\PDO $pdo"
                            },
                            {
                                "type": "arg",
                                "arg_types": [
                                    "string"
                                ],
                                "name": "dir",
                                "declaration": "string $dir"
                            }
                        ],
                        "docblock": {
                            "type": "docblock",
                            "description": "In $dir, there should be directories named 'v1', 'v2', 'v3', and so on.\nIn the v1\/v2\/v3 dirs, there should be up.sql & down.sql files with valid SQL statements for whatever database you're using\n",
                            "attribute": [
                                {
                                    "type": "attribute",
                                    "name": "param",
                                    "description": "$pdo a pdo instance"
                                },
                                {
                                    "type": "attribute",
                                    "name": "param",
                                    "description": "$dir a directory path"
                                }
                            ]
                        },
                        "modifiers": [
                            "public"
                        ],
                        "name": "__construct",
                        "body": "$this->pdo = $pdo;\n$this->dir = $dir;",
                        "declaration": "public function __construct(\\PDO $pdo, string $dir)"
                    },
                    {
                        "type": "method",
                        "args": [
                            {
                                "type": "arg",
                                "arg_types": [
                                    "string"
                                ],
                                "name": "dbName",
                                "value": "':memory:'",
                                "declaration": "string $dbName = ':memory:'"
                            }
                        ],
                        "docblock": {
                            "type": "docblock",
                            "description": "Convenience method to initialize sqlite db in memory",
                            "attribute": [
                                {
                                    "type": "attribute",
                                    "name": "return",
                                    "description": "Tlf\\LilDb"
                                }
                            ]
                        },
                        "modifiers": [
                            "static",
                            "public"
                        ],
                        "name": "sqlite",
                        "body": "$pdo = new \\PDO('sqlite:'.$dbName);\n$pdo->setAttribute(\\PDO::ATTR_ERRMODE, \\PDO::ERRMODE_EXCEPTION);\n$ldb = new static($pdo);\nreturn $ldb;",
                        "declaration": "static public function sqlite(string $dbName = ':memory:')"
                    },
                    {
                        "type": "method",
                        "args": [
                            {
                                "type": "arg",
                                "arg_types": [
                                    "int"
                                ],
                                "name": "old",
                                "declaration": "int $old"
                            },
                            {
                                "type": "arg",
                                "arg_types": [
                                    "int"
                                ],
                                "name": "new",
                                "declaration": "int $new"
                            }
                        ],
                        "docblock": {
                            "type": "docblock",
                            "description": "Migrate from old version to new\n",
                            "attribute": [
                                {
                                    "type": "attribute",
                                    "name": "param",
                                    "description": "$old the current version of the database"
                                },
                                {
                                    "type": "attribute",
                                    "name": "param",
                                    "description": "$new the new version of the database to go to"
                                }
                            ]
                        },
                        "modifiers": [
                            "public"
                        ],
                        "name": "migrate",
                        "body": "if ($old < $new){\n    for ($i=$old+1; $i <= $new; $i++){\n        $this->run_migration_version($i, 'up');\n    }\n} else if ($old > $new) {\n    for ($i=$old-1; $i >= $new; $i--){\n        $this->run_migration_version($i, 'down');\n    }\n} else {\n    $this->run_migration_version($i, 'up');\n}",
                        "declaration": "public function migrate(int $old, int $new)"
                    },
                    {
                        "type": "method",
                        "args": [
                            {
                                "type": "arg",
                                "name": "version",
                                "declaration": "$version"
                            },
                            {
                                "type": "arg",
                                "name": "up_or_down",
                                "declaration": "$up_or_down"
                            }
                        ],
                        "modifiers": [
                            "public"
                        ],
                        "name": "run_migration_version",
                        "body": "$i = $version;\n$file = $up_or_down;\n$v_dir = \"v$i\/\";\n$migrate_dir = $this->dir.'\/'.$v_dir;\n$files = is_dir($migrate_dir) ? scandir($migrate_dir) : false;\nif ($files==false){\n    echo \"\\nMigrations dir $v_dir does not exist. Continuing.\";\n    return;\n}\n$files = array_filter($files,\n    function($v) use ($file){\n        if (substr($v,0,strlen($file))==$file)return true;\n        return false;\n    });\n\/\/@todo test the file sorting\n\/\/ \/\/ for testing\n\/\/ rsort($files);\n\/\/\n\/\/ $files = ['up-9.sql', 'up-2.sql', 'up-13.sql', 'up-0.sql', 'up.sql', 'up-1.sql'];\nusort($files,\n    function($v1, $v2){\n        $pos1 = strpos($v1,'-');\n        if ($pos1==false)$index1 = -1;\n        else $index1 = (int)substr($v1,$pos1+1,-4);\n        $pos2 = strpos($v2,'-');\n        if ($pos2==false)$index2 = -1;\n        else $index2 = (int)substr($v2,$pos2+1,-4);\n        return $index1-$index2;\n    }\n);\nforeach ($files as $f){\n    $rel = $v_dir.$f;\n    $exec_file = $this->dir.'\/'.$rel;\n    $exec_file = $this->dir.'\/'.$rel;\n    if (!file_exists($exec_file)){\n        echo \"\\nFile '$rel' was not found for migrations.\";\n        continue;\n    }\n    echo \"\\nExecute '$rel'\";\n    $sql = file_get_contents($exec_file);\n    $this->pdo->exec($sql);\n    if ($this->pdo->errorCode()!='00000'){\n        echo \"\\nSQL Error in '$rel':\\n\";\n        print_r($this->pdo->errorInfo());\n        return;\n    }\n}",
                        "declaration": "public function run_migration_version($version, $up_or_down)"
                    }
                ]
            }
        ]
    }
}