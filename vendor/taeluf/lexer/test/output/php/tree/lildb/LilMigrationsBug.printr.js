Array
(
    [type] => file
    [namespace] => Array
        (
            [type] => namespace
            [name] => Tlf
            [declaration] => namespace Tlf;
            [class] => Array
                (
                    [0] => Array
                        (
                            [type] => class
                            [docblock] => Array
                                (
                                    [type] => docblock
                                    [description] => This is a minimal version of LilMigrations for debugging the properties issue
                                )

                            [namespace] => Tlf
                            [fqn] => Tlf\LilMigrationsBug
                            [name] => LilMigrationsBug
                            [declaration] => class LilMigrationsBug
                            [properties] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => prop
                                            [value] => 'okay'
                                            [declaration] => public $prop = 'okay';
                                        )

                                )

                            [methods] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => ok
                                            [body] => function() use ($file){
    return;
};
$abc = 'def';
                                            [declaration] => public function ok()
                                        )

                                )

                        )

                )

        )

)
