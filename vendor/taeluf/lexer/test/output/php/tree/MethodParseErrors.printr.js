Array
(
    [type] => file
    [namespace] => 
    [class] => Array
        (
            [0] => Array
                (
                    [type] => class
                    [fqn] => Sample
                    [namespace] => 
                    [name] => Sample
                    [extends] => cats
                    [declaration] => class Sample extends cats
                    [methods] => Array
                        (
                            [0] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => cats
                                    [body] => 
                                    [declaration] => public function cats()
                                )

                            [1] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [type] => arg
                                                    [name] => arg
                                                    [declaration] => $arg
                                                )

                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => dogs
                                    [body] => 
                                    [declaration] => public function dogs($arg)
                                )

                            [2] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [type] => arg
                                                    [name] => arg
                                                    [value] => 1
                                                    [declaration] => $arg=1
                                                )

                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => dogs2
                                    [body] => 
                                    [declaration] => public function dogs2($arg=1)
                                )

                            [3] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [type] => arg
                                                    [name] => arg
                                                    [value] => 'yes'
                                                    [declaration] => $arg='yes'
                                                )

                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => dogs_3
                                    [body] => 
                                    [declaration] => public function dogs_3($arg='yes')
                                )

                            [4] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                        )

                                    [docblock] => Array
                                        (
                                            [type] => docblock
                                            [description] => bears are so cute
                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => bears
                                    [body] => 
                                    [declaration] => public function bears()
                                )

                            [5] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                        )

                                    [docblock] => Array
                                        (
                                            [type] => docblock
                                            [description] => bears are so cute
                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => bears_are_best
                                    [body] => 
                                    [declaration] => public function bears_are_best()
                                )

                            [6] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => bears_do_stuff
                                    [body] => echo "love bears";
                                    [declaration] => public function bears_do_stuff()
                                )

                            [7] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => bears_cuddle_stuff
                                    [body] => $str = "resist the temptation to cuddle a bear. not safe. big sad";
echo $str;
                                    [declaration] => public function bears_cuddle_stuff()
                                )

                            [8] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => bears_nest
                                    [body] => $cats = 'run away from bears';
if ($cats == 'idk'){
    echo "this is a block";
}
                                    [declaration] => public function bears_nest()
                                )

                            [9] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [type] => arg
                                                    [name] => srcCode
                                                    [declaration] => $srcCode
                                                )

                                        )

                                    [docblock] => Array
                                        (
                                            [type] => docblock
                                            [description] => Replaces inline PHP code with placeholder, indexes the placeholder, and returns the modified code

                                            [attribute] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => attribute
                                                            [name] => param
                                                            [description] => mixed $srcCode - The source code
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => attribute
                                                            [name] => return
                                                            [description] => string - source code with all PHP replaced by codeIds
                                                        )

                                                )

                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => cleanSource
                                    [return_types] => Array
                                        (
                                            [0] => string
                                        )

                                    [body] => $parser = new PHPParser($srcCode);
$parsed = $parser->pieces();
                                    [declaration] => public function cleanSource($srcCode): string
                                )

                            [10] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => makes_it_11
                                    [body] => 
                                    [declaration] => public function makes_it_11()
                                )

                            [11] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => output
                                    [return_types] => Array
                                        (
                                            [0] => string
                                        )

                                    [body] => // print_r($this->code);
// // echo $this->code[0]->;
// exit;
// print_r($this->placeholder);
$code = implode("\n",$this->code);
// return $code;
$ph = [];
foreach ($this->placeholder as $id=>$codeArray){
    $ph[$id] = implode('',$codeArray);
}
$last = $code;
while($last != $code = str_replace(array_keys($ph),$ph,$code))$last=$code;
return $code;
                                    [declaration] => public function output(): string
                                )

                            [12] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => ok_13
                                    [body] => 
                                    [declaration] => public function ok_13()
                                )

                            [13] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [type] => arg
                                                    [arg_types] => Array
                                                        (
                                                            [0] => string
                                                        )

                                                    [name] => file
                                                    [declaration] => string $file
                                                )

                                            [1] => Array
                                                (
                                                    [type] => arg
                                                    [name] => chmodTo
                                                    [value] => null
                                                    [declaration] => $chmodTo=null
                                                )

                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => writeTo
                                    [return_types] => Array
                                        (
                                            [0] => bool
                                        )

                                    [body] => $output = $this->output();
if (is_dir(dirname($file))){
    mkdir(dirname($file),0771,true);
    // chmod(dirname($file),0770);
}
$didPut = file_put_contents($file,$output);
if ($chmodTo!==null){
    // chmod($file,$chmodTo);
}
if ($didPut===false)return false;
else return true;
                                    [declaration] => public function writeTo(string $file, $chmodTo=null): bool
                                )

                            [14] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => yep_15
                                    [body] => 
                                    [declaration] => public function yep_15()
                                )

                            [15] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [type] => arg
                                                    [name] => html
                                                    [declaration] => $html
                                                )

                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => __construct
                                    [body] => parent::__construct();
$this->srcHTML = $html;
$parser = new PHTML\PHPParser($html);
$enc = $parser->pieces();
$this->php = $enc->php;
$this->cleanSrc = $enc->html;
$this->cleanSrc = $this->cleanHTML($this->cleanSrc);
$hideXmlErrors=true;
libxml_use_internal_errors($hideXmlErrors);
$this->registerNodeClass('DOMElement', '\\Taeluf\\PHTML\\Node');
$this->registerNodeClass('DOMText', '\\Taeluf\\PHTML\\TextNode');
// $this->registerNodeClass('DOMText', 'RBText');
$html = '<root>'.$this->cleanSrc.'</root>';
$this->loadHTML($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
$this->formatOutput = true;
libxml_use_internal_errors(false);
                                    [declaration] => public function __construct($html)
                                )

                            [16] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => okay_17
                                    [body] => 
                                    [declaration] => public function okay_17()
                                )

                            [17] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [type] => arg
                                                    [name] => withPHP
                                                    [value] => true
                                                    [declaration] => $withPHP=true
                                                )

                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => output2
                                    [body] => // echo "\n".'-start output call-'."\n";
$list = $this->childNodes[0]->childNodes;
$hiddenTagsNodes = $this->xpath('//*[@hideOwnTag]');
foreach ($hiddenTagsNodes as $htn){
    if ($htn->hideOwnTag==false||$htn->hideOwnTag=='false'){
        unset($htn->hideOwnTag);
        continue;
    }
    $parent = $htn->parentNode;
    $childNodeList = $htn->children;
    foreach ($childNodeList as $child){
        $htn->removeChild($child);
        $parent->insertBefore($child, $htn);
    }
    $parent->removeChild($htn);
}
$html = '';
foreach ($list as $item){
    $html .= $this->saveHTML($item);
}
/** Run the php-code-replacer as long as there is a placeholder (while preventing infinite looping) */
$html = $this->fill_php($html, $withPHP);
$html = $this->restoreHtml($html);
return $html;
                                    [declaration] => public function output2($withPHP=true)
                                )

                            [18] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => now_19
                                    [body] => 
                                    [declaration] => public function now_19()
                                )

                            [19] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [type] => arg
                                                    [name] => html
                                                    [declaration] => $html
                                                )

                                            [1] => Array
                                                (
                                                    [type] => arg
                                                    [name] => withPHP
                                                    [value] => true
                                                    [declaration] => $withPHP=true
                                                )

                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => fill_php
                                    [body] => $maxIters = 25;
$iters = 0;
while ($iters++<$maxIters&&preg_match('/php([a-zA-Z]{26})php/', $html, $match)){
    foreach ($this->php as $id=>$code){
        if ($withPHP)$html = str_replace($id,$code,$html);
        else $html = str_replace($id,'',$html);
    }
}
if (($phpAttrVal=$this->phpAttrValue)!=null){
    $html = str_replace("=\"$phpAttrVal\"", '', $html);
}
return $html;
                                    [declaration] => public function fill_php($html, $withPHP=true)
                                )

                            [20] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => ugh_21
                                    [body] => 
                                    [declaration] => public function ugh_21()
                                )

                            [21] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                            [0] => Array
                                                (
                                                    [type] => arg
                                                    [arg_types] => Array
                                                        (
                                                            [0] => string
                                                        )

                                                    [name] => tableName
                                                    [declaration] => string $tableName
                                                )

                                            [1] => Array
                                                (
                                                    [type] => arg
                                                    [arg_types] => Array
                                                        (
                                                            [0] => array
                                                        )

                                                    [name] => colDefinitions
                                                    [declaration] => array $colDefinitions
                                                )

                                            [2] => Array
                                                (
                                                    [type] => arg
                                                    [arg_types] => Array
                                                        (
                                                            [0] => bool
                                                        )

                                                    [name] => recreateIfExists
                                                    [value] => false
                                                    [declaration] => bool $recreateIfExists=false
                                                )

                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => create
                                    [body] => $colStatements = [];
foreach ($colDefinitions as $col => $definition){
    $statement = '`'.$col.'` '. $definition;
    $colStatements[] = $statement;
}
$colsSql = implode(", ", $colStatements);
$drop = $recreateIfExists ? "DROP TABLE IF EXISTS `{$tableName}`;\n" : '';
$sql =
<<<SQL
    {$drop}
    CREATE TABLE IF NOT EXISTS `{$tableName}`
    (
    {$colsSql}
    )
    ;
SQL;
$this->exec($sql);
                                    [declaration] => public function create(string $tableName, array $colDefinitions, bool $recreateIfExists=false)
                                )

                            [22] => Array
                                (
                                    [type] => method
                                    [args] => Array
                                        (
                                        )

                                    [modifiers] => Array
                                        (
                                            [0] => public
                                        )

                                    [name] => its_23
                                    [body] => 
                                    [declaration] => public function its_23()
                                )

                        )

                )

        )

)
