Array
(
    [type] => file
    [namespace] => 
    [functions] => Array
        (
            [0] => Array
                (
                    [type] => function
                    [args] => Array
                        (
                        )

                    [name] => abc
                    [body] => 
                    [declaration] => function abc()
                )

            [1] => Array
                (
                    [type] => function
                    [args] => Array
                        (
                        )

                    [name] => yep
                    [body] => 
                    [declaration] => function yep()
                )

        )

    [class] => Array
        (
            [0] => Array
                (
                    [type] => class
                    [fqn] => Abc
                    [namespace] => 
                    [name] => Abc
                    [declaration] => class Abc
                )

            [1] => Array
                (
                    [type] => class
                    [fqn] => Def
                    [namespace] => 
                    [name] => Def
                    [declaration] => class Def
                )

        )

    [value] => 
    [comments] => Array
        (
            [0] => $descript = $method->description;
        )

)
