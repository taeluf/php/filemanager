Array
(
    [type] => file
    [namespace] => Array
        (
            [type] => namespace
            [name] => Taeluf
            [declaration] => namespace Taeluf;
            [class] => Array
                (
                    [0] => Array
                        (
                            [type] => class
                            [docblock] => Array
                                (
                                    [type] => docblock
                                    [description] => Makes DUMDocument... less terrible, but still not truly good
                                )

                            [namespace] => Taeluf
                            [fqn] => Taeluf\Phtml
                            [name] => Phtml
                            [extends] => \DOMDocument
                            [declaration] => class Phtml extends \DOMDocument
                            [properties] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => protected
                                                    [1] => string
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => The source HTML + PHP code

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => var
                                                                    [description] => string
                                                                )

                                                        )

                                                )

                                            [name] => src
                                            [declaration] => protected string $src;
                                        )

                                    [1] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => protected
                                                    [1] => string
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => The source code with all the PHP replaced by placeholders
                                                )

                                            [name] => cleanSrc
                                            [declaration] => protected string $cleanSrc;
                                        )

                                    [2] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => protected
                                                    [1] => array
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => [ 'phpplaceholder' => $phpCode, 'placeholder2' => $morePHP ]
                                                )

                                            [name] => php
                                            [declaration] => protected array $php;
                                        )

                                    [3] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => protected
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => A random string used when adding php code to a node's tag declaration. This string is later removed during output()
                                                )

                                            [name] => phpAttrValue
                                            [declaration] => protected $phpAttrValue;
                                        )

                                )

                            [comments] => Array
                                (
                                    [0] => /**
                                    [1] => * True if the source html had a '<html>' tag
                                    [2] => * Except we're not implementing that???
                                    [3] => * @var bool
                                    [4] => */
                                    [5] => protected bool $isHTMLDoc = false;
                                )

                            [methods] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => html
                                                            [declaration] => $html
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Create a DOMDocument, passing your HTML + PHP to __construct. 


                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $html a block of HTML + PHP code. It does not have to have PHP. PHP will be handled gracefully.
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => void
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => __construct
                                            [body] => parent::__construct();
$this->srcHTML = $html;
$parser = new PHTML\PHPParser($html);
$enc = $parser->pieces();
$this->php = $enc->php;
$this->cleanSrc = $enc->html;
$this->cleanSrc = $this->cleanHTML($this->cleanSrc);
$hideXmlErrors=true;
libxml_use_internal_errors($hideXmlErrors);
$this->registerNodeClass('DOMElement', '\\Taeluf\\PHTML\\Node');
$this->registerNodeClass('DOMText', '\\Taeluf\\PHTML\\TextNode');
// $this->registerNodeClass('DOMText', 'RBText');
$html = '<root>'.$this->cleanSrc.'</root>';
$this->loadHTML($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
$this->formatOutput = true;
libxml_use_internal_errors(false);
                                            [declaration] => public function __construct($html)
                                        )

                                    [1] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => string
                                                            [declaration] => string $string
                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => placeholder
                                            [return_types] => Array
                                                (
                                                    [0] => string
                                                )

                                            [body] => $placeholder = PHTML\PHPParser::getRandomAlpha();
$placeholder = 'php'.$placeholder.'php';
$this->php[$placeholder] = $string;
return $placeholder;
                                            [declaration] => public function placeholder(string $string): string
                                        )

                                    [2] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => placeholder
                                                            [declaration] => string $placeholder
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Get the code that's represented by the placeholder
                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => the stored code or null
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => codeFromPlaceholder
                                            [return_types] => Array
                                                (
                                                    [0] => string
                                                )

                                            [body] => $code = $this->php[trim($placeholder)] ?? null;
return $code;
                                            [declaration] => public function codeFromPlaceholder(string $placeholder): string
                                        )

                                    [3] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => enclosedPHP
                                                            [declaration] => string $enclosedPHP
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Get a placeholder for the given block of code
Intention is to parse a single '<?php //piece of php code ?>' and not '<?php //stuff ?><?php //more stuff?>'
When used as intended, will return a single 'word' that is the placeholder for the given code

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $enclosedPHP an HTML + PHP string
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => string the parsed block of content where PHP code blocks are replaced by placeholders.
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => phpPlaceholder
                                            [return_types] => Array
                                                (
                                                    [0] => string
                                                )

                                            [body] => $parser = new PHTML\PHPParser($enclosedPHP);
$enc = $parser->pieces();
// This block doesn't work because it's over-eager. A workaround to just add code, regardless of open/close tags, would be good.
// if (count($enc->php)==0){
    // $code = \PHTML\PHPParser::getRandomAlpha();
    // $enc->php = ["php${code}php"=>$enclosedPhp];
    // $enc->html = $code;
// }
$this->php = array_merge($this->php,$enc->php);
return $enc->html;
                                            [declaration] => public function phpPlaceholder(string $enclosedPHP): string
                                        )

                                    [4] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => codeWithPlaceholders
                                                            [declaration] => string $codeWithPlaceholders
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Decode the given code by replacing PHP placeholders with the PHP code itself

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $str
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => void
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => fillWithPHP
                                            [return_types] => Array
                                                (
                                                    [0] => string
                                                )

                                            [body] => $decoded = str_replace(array_keys($this->php),$this->php,$codeWithPlaceholders);
return $decoded;
                                            [declaration] => public function fillWithPHP(string $codeWithPlaceholders): string
                                        )

                                    [5] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => See output()

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => string
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => __toString
                                            [body] => return $this->output();
                                            [declaration] => public function __toString()
                                        )

                                    [6] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => withPHP
                                                            [value] => true
                                                            [declaration] => $withPHP=true
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Return the decoded document as as tring. All PHP will be back in its place

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $withPHP passing FALSE means placeholders will still be present & PHP code will not be
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => string the final document with PHP where it belongs
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => output
                                            [body] => // echo "\n".'-start output call-'."\n";
$list = $this->childNodes[0]->childNodes;
$hiddenTagsNodes = $this->xpath('//*[@hideOwnTag]');
foreach ($hiddenTagsNodes as $htn){
    if ($htn->hideOwnTag==false||$htn->hideOwnTag=='false'){
        unset($htn->hideOwnTag);
        continue;
    }
    $parent = $htn->parentNode;
    $childNodeList = $htn->children;
    foreach ($childNodeList as $child){
        $htn->removeChild($child);
        $parent->insertBefore($child, $htn);
    }
    $parent->removeChild($htn);
}
$html = '';
foreach ($list as $item){
    $html .= $this->saveHTML($item);
}
/** Run the php-code-replacer as long as there is a placeholder (while preventing infinite looping) */
$html = $this->fill_php($html, $withPHP);
$html = $this->restoreHtml($html);
return $html;
                                            [declaration] => public function output($withPHP=true)
                                        )

                                    [7] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => html
                                                            [declaration] => $html
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => withPHP
                                                            [value] => true
                                                            [declaration] => $withPHP=true
                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => fill_php
                                            [body] => $maxIters = 25;
$iters = 0;
while ($iters++<$maxIters&&preg_match('/php([a-zA-Z]{26})php/', $html, $match)){
    foreach ($this->php as $id=>$code){
        if ($withPHP)$html = str_replace($id,$code,$html);
        else $html = str_replace($id,'',$html);
    }
}
if (($phpAttrVal=$this->phpAttrValue)!=null){
    $html = str_replace("=\"$phpAttrVal\"", '', $html);
}
return $html;
                                            [declaration] => public function fill_php($html, $withPHP=true)
                                        )

                                    [8] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => xpath
                                                            [declaration] => $xpath
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => refNode
                                                            [value] => null
                                                            [declaration] => $refNode=null
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => get the results of an xpath query

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $xpath the xpath query, such as: //tagname[@attributename="value"]
                 If you use a refnode, prepend '.' at the beginning of your xpath query string
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $refNode a parent-node to search under
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => array the resulting DomNodeList is converted to an array & returned
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => xpath
                                            [body] => $xp = new \DOMXpath($this);
if ($refNode==null)$list =  $xp->query($xpath);
else $list = $xp->query($xpath,$refNode);
$arr = [];
foreach ($list as $item){
    $arr[] = $item;
}
return $arr;
                                            [declaration] => public function xpath($xpath,$refNode=null)
                                        )

                                    [9] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => node
                                                            [declaration] => $node
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => phpCode
                                                            [declaration] => $phpCode
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Set an attribute that will place PHP code inside the tag declartion of a node. 
Basically: `<node phpCodePlaceholder>`, which pHtml will later convert to `<node <?='some_stuff'?>>`. 
This avoids problems caused by attributes requiring a `=""`, which `DOMDocument` automatically places.

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => $phpCode A block of php code with opening & closing tags like <?='some stuff'?>
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => \Taeluf\PHTML\ValuelessAttribute
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => addPhpToTag
                                            [body] => $this->phpAttrValue = $this->phpAttrValue  ??  PHTML\PHPParser::getRandomAlpha();
$placeholder = $this->phpPlaceholder($phpCode);
$node->setAttribute($placeholder, $this->phpAttrValue);
return $placeholder;
                                            [declaration] => public function addPhpToTag($node, $phpCode)
                                        )

                                    [10] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => \DOMNode
                                                                )

                                                            [name] => node
                                                            [declaration] => \DOMNode $node
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => phpCode
                                                            [declaration] => $phpCode
                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => insertCodeBefore
                                            [body] => // $placeholder = $this->phpPlaceholder($phpCode);
$placeholder = $this->placeholder($phpCode);
$text = new \DOMText($placeholder);
return $node->parentNode->insertBefore($text, $node);
                                            [declaration] => public function insertCodeBefore(\DOMNode $node, $phpCode)
                                        )

                                    [11] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => \DOMNode
                                                                )

                                                            [name] => node
                                                            [declaration] => \DOMNode $node
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => phpCode
                                                            [declaration] => $phpCode
                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => insertCodeAfter
                                            [body] => $placeholder = $this->placeholder($phpCode);
$text = new \DOMText($placeholder);
if ($node->nextSibling!==null)return $node->parentNode->insertBefore($text,$node->nextSibling);
return $node->parentNode->insertBefore($text);
                                            [declaration] => public function insertCodeAfter(\DOMNode $node, $phpCode)
                                        )

                                    [12] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => html
                                                            [declaration] => $html
                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => cleanHTML
                                            [body] => // fix doctype
$html = preg_replace('/\<\!DOCTYPE(.*)\>/i', '<tlfphtml-doctype$1></tlfphtml-doctype>',$html);
// fix <html> tag
$html = preg_replace('/<html([ >])/','<tlfphtml-html$1',$html);
$html = str_ireplace('</html>', '</tlfphtml-html>', $html);
// fix <head> tag
$html = preg_replace('/<head([ >])/', '<tlfphtml-head$1',$html);
$html = str_ireplace('</head>', '</tlfphtml-head>',$html);
return $html;
                                            [declaration] => public function cleanHTML($html)
                                        )

                                    [13] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => html
                                                            [declaration] => $html
                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => restoreHtml
                                            [body] => $html = preg_replace('/\<tlfphtml\-doctype(.*)\>\<\/tlfphtml\-doctype\>/i', '<!DOCTYPE$1>',$html);
// fix <html> tag
$html = str_ireplace('<tlfphtml-html','<html',$html);
$html = str_ireplace('</tlfphtml-html>', '</html>', $html);
// fix <head> tag
$html = str_ireplace('<tlfphtml-head', '<head', $html);
$html = str_ireplace('</tlfphtml-head>', '</head>', $html);
return $html;
                                            [declaration] => public function restoreHtml($html)
                                        )

                                    [14] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => param
                                                            [declaration] => $param
                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => __get
                                            [body] => if ($param == 'form'){
    return $this->xpath('//form')[0] ?? null;
}
                                            [declaration] => public function __get($param)
                                        )

                                )

                        )

                )

        )

)
