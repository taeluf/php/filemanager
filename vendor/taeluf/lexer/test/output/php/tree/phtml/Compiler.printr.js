Array
(
    [type] => file
    [namespace] => Array
        (
            [type] => namespace
            [name] => Taeluf\PHTML
            [declaration] => namespace Taeluf\PHTML;
            [class] => Array
                (
                    [0] => Array
                        (
                            [type] => class
                            [namespace] => Taeluf\PHTML
                            [fqn] => Taeluf\PHTML\Compiler
                            [name] => Compiler
                            [declaration] => class Compiler
                            [properties] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => protected
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => An array of code to output.
Likely contains placeholder which will be replaced. 
May contain objects which implement __toString
                                                )

                                            [name] => code
                                            [value] => []
                                            [declaration] => protected $code = [];
                                        )

                                    [1] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => protected
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => The content of a PHP file for compilation
                                                )

                                            [name] => src
                                            [declaration] => protected $src;
                                        )

                                    [2] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => protected
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => An array of placeholder code with codeId => [prependedCode, code, appendedCode]... there can be any number of entries for each codeId
Code may be string or an object which implements __toString
codeIds are either sha sums (alpha-numeric, i think) or randmoized alpha
                                                )

                                            [name] => placeholder
                                            [value] => []
                                            [declaration] => protected $placeholder = [];
                                        )

                                    [3] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => protected
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => The parsed source code, with the PHP code replaced by placeholders
                                                )

                                            [name] => htmlSource
                                            [declaration] => protected $htmlSource;
                                        )

                                )

                            [methods] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => __construct
                                            [body] => 
                                            [declaration] => public function __construct()
                                        )

                                    [1] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => srcCode
                                                            [declaration] => $srcCode
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Replaces inline PHP code with placeholder, indexes the placeholder, and returns the modified code

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $srcCode - The source code
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => string - source code with all PHP replaced by codeIds
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => cleanSource
                                            [return_types] => Array
                                                (
                                                    [0] => string
                                                )

                                            [body] => $parser = new PHPParser($srcCode);
$parsed = $parser->pieces();
foreach ($parsed->php as $id=>$code){
    $this->placeholder[$id] = [$code];
}
return $parsed->html;
                                            [declaration] => public function cleanSource($srcCode): string
                                        )

                                    [2] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => phpCodeWithOpenCloseTags
                                                            [declaration] => $phpCodeWithOpenCloseTags
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => 1. Generates an id
2. indexes the passed-in-code with that id
3. Returns the id.

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $phpCodeWithOpenCloseTags - Code, as it would be found in a PHP file. AKA PHP code MUST include open/close tags
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => string the codeId. Either a random alpha-string OR an sha_sum, which I think is alpha-numeric
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => placeholderFor
                                            [return_types] => Array
                                                (
                                                    [0] => string
                                                )

                                            [body] => $code = $phpCodeWithOpenCloseTags;
$id = $this->freshId();
$this->placeholder[$id] = [$code];
return $id;
                                            [declaration] => public function placeholderFor($phpCodeWithOpenCloseTags): string
                                        )

                                    [3] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => code
                                                            [declaration] => $code
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Appends code to the output-to-be

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $code - Code to append. PHP code must be wrapped in open/close tags
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => void
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => appendCode
                                            [body] => $this->code[] = $code;
                                            [declaration] => public function appendCode($code)
                                        )

                                    [4] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => code
                                                            [declaration] => $code
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Prepends code to the output-to-be

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $code - Code to prepend. PHP code must be wrapped in open/close tags
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => void
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => prependCode
                                            [body] => // $this->code[] = $code;
array_unshift($this->code,$code);
                                            [declaration] => public function prependCode($code)
                                        )

                                    [5] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => placeholder
                                                            [declaration] => $placeholder
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => code
                                                            [declaration] => $code
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Prepend code immediately prior to the given placeholder

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $placeholder - a placeholder from placeholderFor()
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $code - a block of code. PHP must be wrapped in open/close tags
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => void
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => placeholderPrepend
                                            [body] => array_unshift($this->placeholder[$placeholder],$code);
                                            [declaration] => public function placeholderPrepend($placeholder,$code)
                                        )

                                    [6] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => placeholder
                                                            [declaration] => $placeholder
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => code
                                                            [declaration] => $code
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Append code immediately after the given placeholder

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $placeholder - a placeholder from placeholderFor()
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $code - a block of code. PHP must be wrapped in open/close tags
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => void
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => placeholderAppend
                                            [body] => $this->placeholder[$placeholder][] = $code;
                                            [declaration] => public function placeholderAppend($placeholder,$code)
                                        )

                                    [7] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Compile the code into a string & return it. 
output() can be called several times as it does NOT affect the state of the compiler.

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => string
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => output
                                            [return_types] => Array
                                                (
                                                    [0] => string
                                                )

                                            [body] => // print_r($this->code);
// // echo $this->code[0]->;
// exit;
// print_r($this->placeholder);
$code = implode("\n",$this->code);
// return $code;
$ph = [];
foreach ($this->placeholder as $id=>$codeArray){
    $ph[$id] = implode('',$codeArray);
}
$last = $code;
while($last != $code = str_replace(array_keys($ph),$ph,$code))$last=$code;
return $code;
                                            [declaration] => public function output(): string
                                        )

                                    [8] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => file
                                                            [declaration] => string $file
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => chmodTo
                                                            [value] => null
                                                            [declaration] => $chmodTo=null
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Writes the compiled output to the given file

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => string $file - an absolute filepath
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => $chmodTo - REMOVED DOES NOTHING

         0644 or whatever. If null, chmod will not be run.
         See https://www.php.net/manual/en/function.chmod.php
         Permissions are as follows:
             Value    Permission Level
              200        Owner Write
              400        Owner Read
              100        Owner Execute
               40         Group Read
               20         Group Write
               10         Group Execute
                4         Global Read
                2         Global Write
                1         Global Execute
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => boolean true if file_put_contents succeeds. False otherwise.
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => writeTo
                                            [return_types] => Array
                                                (
                                                    [0] => bool
                                                )

                                            [body] => $output = $this->output();
if (!is_dir(dirname($file))){
    mkdir(dirname($file),0771,true);
    // chmod(dirname($file),0770);
}
$didPut = file_put_contents($file,$output);
if ($chmodTo!==null){
    // chmod($file,$chmodTo);
}
if ($didPut===false)return false;
else return true;
                                            [declaration] => public function writeTo(string $file, $chmodTo=null): bool
                                        )

                                    [9] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => compileDir
                                                            [declaration] => $compileDir
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => code
                                                            [declaration] => $code
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Get an absolute file path which can be included to execute the given code

1. $codeId = sha1($code)
2. file_put_contents("$compileDir/$codeId.php", $code)
3. return the path of the new file

- Will create the directory (non-recursive) if not exists

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $compileDir - the directory in which the file should be written
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $code - the block of code. PHP code must be wrapped in open/close tags to be executed
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => string an absolute file path to a php file
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => fileForCode
                                            [return_types] => Array
                                                (
                                                    [0] => string
                                                )

                                            [body] => // $codeId = $this->freshId(30);
$codeId = sha1($code);
$file = $compileDir.'/'.$codeId.'.php';
if (!file_exists($compileDir))mkdir($compileDir,0770,true);
file_put_contents($file,$code);
return $file;
                                            [declaration] => public function fileForCode($compileDir,$code): string
                                        )

                                    [10] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => length
                                                            [value] => 26
                                                            [declaration] => $length = 26
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Generate a random string of lowercase letters

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $length The desired length of the random string. Default is 26 to avoid any clashing
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => string the random string
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => protected
                                                )

                                            [name] => freshId
                                            [return_types] => Array
                                                (
                                                    [0] => string
                                                )

                                            [body] => $characters = 'abcdefghijklmnopqrstuvwxyz';
$charactersLength = strlen($characters);
$randomString = '';
for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
}
return $randomString;
                                            [declaration] => protected function freshId($length = 26): string
                                        )

                                    [11] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => codeId
                                                            [declaration] => string $codeId
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => bool
                                                                )

                                                            [name] => asArray
                                                            [value] => false
                                                            [declaration] => bool $asArray=false
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Get the code for the given code id.

Placeholder code is stored as an array to enable the placeholderPrepend|Append functions, so I make it available as an array if you want.


                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => string $codeId - the id generated by placeholderFor()
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => bool $asArray - TRUE to get the code as it's array parts. FALSE to implode the array (no newlines) & return that
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => mixed an array of code pieces that make up this codeid or a string of the code
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => codeForId
                                            [body] => $codeArr = $this->placeholder[$codeId];
if ($asArray)return $codeArr;
else return implode('',$codeArr);
                                            [declaration] => public function codeForId(string $codeId,bool $asArray=false)
                                        )

                                )

                        )

                )

        )

)
