{
    "type": "file",
    "namespace": {
        "type": "namespace",
        "name": "Taeluf\\PHTML",
        "declaration": "namespace Taeluf\\PHTML;",
        "class": [
            {
                "type": "class",
                "docblock": {
                    "type": "docblock",
                    "description": "Parses .php files into:\n 1. A string with placeholders for the PHP code\n 2. An array of PHP code, identified by their placeholders"
                },
                "namespace": "Taeluf\\PHTML",
                "fqn": "Taeluf\\PHTML\\PHPParser",
                "name": "PHPParser",
                "declaration": "class PHPParser",
                "properties": [
                    {
                        "type": "property",
                        "modifiers": [
                            "protected",
                            "string"
                        ],
                        "docblock": {
                            "type": "docblock",
                            "description": "The source HTML + PHP code\n",
                            "attribute": [
                                {
                                    "type": "attribute",
                                    "name": "var",
                                    "description": "string"
                                }
                            ]
                        },
                        "name": "src",
                        "declaration": "protected string $src;"
                    },
                    {
                        "type": "property",
                        "modifiers": [
                            "protected",
                            "object"
                        ],
                        "docblock": {
                            "type": "docblock",
                            "description": "The parsed pieces of code in the format:\n [\n     'html' => '<div>A string of code with php placeholders like <b>phpadsfwefhaksldfjaskdlfjaskldfjasldfkphp<\/b> <\/div> and trailing text...',\n     'php'  => [\n         'phpadsfwefhaksldfjaskdlfjaskldfjasldfkphp' => '<?=\"Something echod\";?>', \n         'phpid2'=>'<?php \/\/ another code block\/?>'\n     ]\n ]\nThe array is simply (object) cast"
                        },
                        "name": "pieces",
                        "declaration": "protected object $pieces;"
                    }
                ],
                "methods": [
                    {
                        "type": "method",
                        "args": [
                            {
                                "type": "arg",
                                "arg_types": [
                                    "string"
                                ],
                                "name": "htmlPHPString",
                                "declaration": "string $htmlPHPString"
                            }
                        ],
                        "docblock": {
                            "type": "docblock",
                            "description": "Create a new PHP parser instance from a string\n",
                            "attribute": [
                                {
                                    "type": "attribute",
                                    "name": "param",
                                    "description": "mixed $htmlPHPString - a block of HTML + PHP code. PHP code will be inside open (<?php or <?=) and close (?>) tags"
                                },
                                {
                                    "type": "attribute",
                                    "name": "return",
                                    "description": "void"
                                }
                            ]
                        },
                        "modifiers": [
                            "public"
                        ],
                        "name": "__construct",
                        "body": "$this->src = $htmlPHPString;\n$this->pieces = $this->separatePHP();",
                        "declaration": "public function __construct(string $htmlPHPString)"
                    },
                    {
                        "type": "method",
                        "args": [],
                        "docblock": {
                            "type": "docblock",
                            "description": "Return the parsed pieces. See doc for protected $pieces\n",
                            "attribute": [
                                {
                                    "type": "attribute",
                                    "name": "return",
                                    "description": "object"
                                }
                            ]
                        },
                        "modifiers": [
                            "public"
                        ],
                        "name": "pieces",
                        "return_types": [
                            "object"
                        ],
                        "body": "return $this->pieces;",
                        "declaration": "public function pieces(): object"
                    },
                    {
                        "type": "method",
                        "args": [],
                        "docblock": {
                            "type": "docblock",
                            "description": "Separate the source code into it's pieces. See the protected $pieces docs",
                            "attribute": [
                                {
                                    "type": "attribute",
                                    "name": "return",
                                    "description": "object just an array cast to object"
                                }
                            ]
                        },
                        "modifiers": [
                            "protected"
                        ],
                        "name": "separatePHP",
                        "return_types": [
                            "object"
                        ],
                        "body": "$tokens = $this->tokens();\n$str = '';\n$inPHP = false;\n$phpCode = '';\n$phpList = [];\nforeach ($tokens as $index => $token){\n    $token = (object)$token;\n    if ($token->type=='T_OPEN_TAG'\n        ||$token->type=='T_OPEN_TAG_WITH_ECHO'){\n        $inPHP = true;\n    }\n    if (!$inPHP){\n        $str .= $token->code;\n    }\n    if ($inPHP){\n        $phpCode .= $token->code;\n    }\n    if ($token->type=='T_CLOSE_TAG'){\n        $id = 'php'.$this->randomAlpha().'php';\n        $phpList[$id] = $phpCode;\n        $phpCode = '';\n        $str .= $id;\n        $inPHP = false;\n    }\n}\nreturn (object)[\n    'php'=>$phpList,\n    'html'=>$str\n];",
                        "declaration": "protected function separatePHP(): object"
                    },
                    {
                        "type": "method",
                        "args": [
                            {
                                "type": "arg",
                                "name": "length",
                                "value": "26",
                                "declaration": "$length = 26"
                            }
                        ],
                        "docblock": {
                            "type": "docblock",
                            "description": "Generates a random string of characters a-z, all lowercase & returns them\n this is used for the PHP placeholders\n",
                            "attribute": [
                                {
                                    "type": "attribute",
                                    "name": "param",
                                    "description": "int $length"
                                },
                                {
                                    "type": "attribute",
                                    "name": "return",
                                    "description": "string"
                                }
                            ]
                        },
                        "modifiers": [
                            "protected"
                        ],
                        "name": "randomAlpha",
                        "return_types": [
                            "string"
                        ],
                        "body": "return static::getRandomAlpha($length);",
                        "declaration": "protected function randomAlpha($length = 26): string"
                    },
                    {
                        "type": "method",
                        "args": [
                            {
                                "type": "arg",
                                "name": "length",
                                "value": "26",
                                "declaration": "$length = 26"
                            }
                        ],
                        "docblock": {
                            "type": "docblock",
                            "description": "Generates a random string of characters a-z, all lowercase & returns them\n this is used for the PHP placeholders\n",
                            "attribute": [
                                {
                                    "type": "attribute",
                                    "name": "param",
                                    "description": "int $length"
                                },
                                {
                                    "type": "attribute",
                                    "name": "return",
                                    "description": "string"
                                }
                            ]
                        },
                        "modifiers": [
                            "static",
                            "public"
                        ],
                        "name": "getRandomAlpha",
                        "return_types": [
                            "string"
                        ],
                        "body": "$characters = 'abcdefghijklmnopqrstuvwxyz';\n$charactersLength = strlen($characters);\n$randomString = '';\nfor ($i = 0; $i < $length; $i++) {\n    $randomString .= $characters[rand(0, $charactersLength - 1)];\n}\nreturn $randomString;",
                        "declaration": "static public function getRandomAlpha($length = 26): string"
                    },
                    {
                        "type": "method",
                        "args": [],
                        "docblock": {
                            "type": "docblock",
                            "description": "Tokenize the src code into a slightly better format than token_get_all\n",
                            "attribute": [
                                {
                                    "type": "attribute",
                                    "name": "return",
                                    "description": "array of tokens"
                                }
                            ]
                        },
                        "modifiers": [
                            "protected"
                        ],
                        "name": "tokens",
                        "return_types": [
                            "array"
                        ],
                        "body": "$content = $this->src;\n$tokens = token_get_all($content);\n$niceTokens = [];\n$delLine = null;\nforeach ($tokens as $index=>$token){\n    $tok = [];\n    if (!is_array($token)){\n        $lastTok = array_slice($niceTokens,-1)[0];\n        $tok['type'] = \"UNNAMED_TOKEN\";\n        $tok['code'] = $token;\n        $tok['line'] = $lastTok['line'];\n    } else {\n        $tok['type'] = token_name($token[0]);\n        $tok['code'] = $token[1];\n        $tok['line'] = $token[2];\n    }\n    \/\/ This is old code for an idea I had\n    \/\/ if ($tok['type']=='T_STRING'&&$tok['code']=='PHPPlus'){\n        \/\/ $next = $tokens[$index+1];\n        \/\/ if (is_array($next)&&$next[1]=='::'){\n            \/\/ $delLine = $tok['line'];\n            \/\/ echo 'del line is '.$delLine.\"\\n\\n\";\n        \/\/ }\n    \/\/ }\n    $niceTokens[] = $tok;\n}\nforeach ($niceTokens as $index=>$token){\n    if ($token['line']===$delLine){\n        unset($niceTokens[$index]);\n    }\n}\nreturn $niceTokens;",
                        "declaration": "protected function tokens(): array"
                    }
                ]
            }
        ]
    }
}