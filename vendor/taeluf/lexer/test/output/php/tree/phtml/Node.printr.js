Array
(
    [type] => file
    [namespace] => Array
        (
            [type] => namespace
            [name] => Taeluf\PHTML
            [declaration] => namespace Taeluf\PHTML;
            [class] => Array
                (
                    [0] => Array
                        (
                            [type] => class
                            [docblock] => Array
                                (
                                    [type] => docblock
                                    [description] => less sucky HTML DOM Element

This class extends PHP's DOMElement to make it suck less

The original version of this file was a pre-made script by the author below.
The only meaningful pieces of code I kept are the two `if 'innerHTML'` blocks of code.
No license information was available in the copied code and I don't remember what it said on the author's website.

The original package had the following notes from the author:

- Authored by: Keyvan Minoukadeh - http://www.keyvan.net - keyvan@keyvan.net
  - See: http://fivefilters.org (the project this was written for)
                                )

                            [namespace] => Taeluf\PHTML
                            [fqn] => Taeluf\PHTML\Node
                            [name] => Node
                            [extends] => \DOMElement
                            [declaration] => class Node extends \DOMElement
                            [comments] => Array
                                (
                                    [0] => public $hideOwnTag = false;
                                    [1] => protected $children = [];
                                    [2] => 
                                )

                            [methods] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => __construct
                                            [body] => parent::__construct();
// $children = [];
      // foreach ($this->childNodes as $childNode){
          // $children[] = $childNode;
      // }
      // $this->children = $children;
                                            [declaration] => public function __construct()
                                        )

                                    [1] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => tagName
                                                            [declaration] => string $tagName
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => is this node the given tag
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => is
                                            [return_types] => Array
                                                (
                                                    [0] => bool
                                                )

                                            [body] => if (strtolower($this->tagName)==strtolower($tagName))return true;
return false;
                                            [declaration] => public function is(string $tagName): bool
                                        )

                                    [2] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => attribute
                                                            [declaration] => string $attribute
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => 
                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => alias
                                                                    [description] => for DOMDocument::hasAttribute();
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => has
                                            [return_types] => Array
                                                (
                                                    [0] => bool
                                                )

                                            [body] => return $this->hasAttribute($attribute);
                                            [declaration] => public function has(string $attribute): bool
                                        )

                                    [3] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => get an array of DOMAttrs on this node
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => attributes
                                            [body] => $attrs = $this->attributes;
$list = [];
foreach ($attrs as $attr){
	$list[] = $attr;
}
return $list;
                                            [declaration] => public function attributes()
                                        )

                                    [4] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => return an array of attributes ['attributeName'=>'value', ...];
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => attributesAsArray
                                            [body] => $list = [];
foreach ($this->attributes as $attr){
    $list[$attr->name] = $attr->value;
}
return $list;
                                            [declaration] => public function attributesAsArray()
                                        )

                                    [5] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => name
                                                            [declaration] => $name
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => value
                                                            [declaration] => $value
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Used for setting innerHTML like it's done in JavaScript:
@code
$div->innerHTML = '<h2>Chapter 2</h2><p>The story begins...</p>';
@endcode
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => __set
                                            [body] => if (strtolower($name) == 'innerhtml') {
	// first, empty the element
	for ($x=$this->childNodes->length-1; $x>=0; $x--) {
		$this->removeChild($this->childNodes->item($x));
	}
	// $value holds our new inner HTML
	if ($value != '') {
		$f = $this->ownerDocument->createDocumentFragment();
		// appendXML() expects well-formed markup (XHTML)
		$result = @$f->appendXML($value); // @ to suppress PHP warnings
		if ($result) {
			if ($f->hasChildNodes()) $this->appendChild($f);
		} else {
			// $value is probably ill-formed
			$f = new DOMDocument();
			$value = mb_convert_encoding($value, 'HTML-ENTITIES', 'UTF-8');
			// Using <htmlfragment> will generate a warning, but so will bad HTML
			// (and by this point, bad HTML is what we've got).
			// We use it (and suppress the warning) because an HTML fragment will
			// be wrapped around <html><body> tags which we don't really want to keep.
			// Note: despite the warning, if loadHTML succeeds it will return true.
			$result = @$f->loadHTML('<htmlfragment>'.$value.'</htmlfragment>');
			if ($result) {
				$import = $f->getElementsByTagName('htmlfragment')->item(0);
				foreach ($import->childNodes as $child) {
					$importedNode = $this->ownerDocument->importNode($child, true);
					$this->appendChild($importedNode);
				}
			} else {
				// oh well, we tried, we really did. :(
				// this element is now empty
			}
		}
	}
} else {
	$this->setAttribute($name,$value);
	return;
	$trace = debug_backtrace();
	trigger_error('Undefined property via __set(): '.$name.' in '.$trace[0]['file'].' on line '.$trace[0]['line'], E_USER_NOTICE);
}
                                            [declaration] => public function __set($name, $value)
                                        )

                                    [6] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => name
                                                            [declaration] => $name
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => if the node has the named attribute, it will be removed. Otherwise, nothing happens
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => __unset
                                            [body] => if ($this->hasAttribute($name))$this->removeAttribute($name);
                                            [declaration] => public function __unset($name)
                                        )

                                    [7] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => name
                                                            [declaration] => $name
                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => __isset
                                            [body] => return $this->hasAttribute($name);
                                            [declaration] => public function __isset($name)
                                        )

                                    [8] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => name
                                                            [declaration] => $name
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Used for getting innerHTML like it's done in JavaScript:
@code
$string = $div->innerHTML;
@endcode
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => __get
                                            [body] =>       if (method_exists($this, $getter = 'get'.strtoupper($name))){
          return $this->$getter();
      } else if ($name=='doc'){
          return $this->ownerDocument;
      } else if (strtolower($name) == 'innerhtml') {
              $inner = '';
              foreach ($this->childNodes as $child) {
                  $inner .= $this->ownerDocument->saveXML($child);
              }
              return $inner;
          } else if ($name=='form'&&strtolower($this->tagName)=='input'){
          $parent = $this->parentNode ?? null;
          while ($parent!=null&&strtolower($parent->tagName)!='form')$parent = $parent->parentNode ?? null;
          return $parent;
      } else if ($name=='inputs' && strtolower($this->tagName)=='form'){
          $inputList = $this->doc->xpath('//input', $this);
          // var_dump($inputList);
          // exit;
          return $inputList;
      } else if ($name=='children'){
          $children = [];
          for ($i=0;$i<$this->childNodes->count();$i++){
              $children[] = $this->childNodes->item($i);
          }
          return $children;
      }
      else if ($this->hasAttribute($name)){
          return $this->getAttribute($name);
      } else {
          return null;
      }
// $trace = debug_backtrace();
// trigger_error('Undefined property via __get(): '.$name.' in '.$trace[0]['file'].' on line '.$trace[0]['line'], E_USER_NOTICE);
// return null;
                                            [declaration] => public function __get($name)
                                        )

                                    [9] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => __toString
                                            [body] => return $this->ownerDocument->saveHTML($this);
// return '['.$this->tagName.']';
                                            [declaration] => public function __toString()
                                        )

                                    [10] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => xpath
                                                            [declaration] => $xpath
                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => xpath
                                            [body] => return $this->doc->xpath($xpath, $this);
                                            [declaration] => public function xpath($xpath)
                                        )

                                    [11] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => inputName
                                                            [declaration] => $inputName
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => value
                                                            [declaration] => $value
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Adds a hidden input to a form node
If a hidden input already exists with that name, do nothing
If a hidden input does not exist with that name, create and append it

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $key
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $value
                                                                )

                                                            [2] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => throws
                                                                    [description] => \BadMethodCallException if this method is called on a non-form node
                                                                )

                                                            [3] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => void
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => addHiddenInput
                                            [body] => if (strtolower($this->tagName)!='form')throw new \BadMethodCallException("addHiddenInput can only be called on a Form node");
      $xPath = new \DOMXpath($this->ownerDocument);
      $inputs = $xPath->query('//input[@name="'.$inputName.'"][@type="hidden"]');
      if (count($inputs)>0)return;
$input = $this->ownerDocument->createElement('input');
$input->setAttribute('name',$inputName);
$input->setAttribute('value',$value);
$input->setAttribute('type','hidden');
$this->appendChild($input);
                                            [declaration] => public function addHiddenInput($inputName, $value)
                                        )

                                    [12] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => attributeName
                                                            [declaration] => $attributeName
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Find out if this node has a true value for the given attribute name.
Literally just returns $this->hasAttribute($attributeName)

I wanted to implement an attribute="false" option... but that goes against the standards of HTML5, so that idea is on hold.

See https://stackoverflow.com/questions/4139786/what-does-it-mean-in-html-5-when-an-attribute-is-a-boolean-attribute 

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $attributeName The name of the attribute we're checking for.
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => bool
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => boolAttribute
                                            [body] => return $this->hasAttribute($attributeName);
                                            [declaration] => public function boolAttribute($attributeName)
                                        )

                                    [13] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => getInnerText
                                            [body] => return $this->textContent;
                                            [declaration] => public function getInnerText()
                                        )

                                    [14] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => method
                                                            [declaration] => $method
                                                        )

                                                    [1] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => args
                                                            [declaration] => $args
                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => __call
                                            [body] => if (substr($method,0,2)=='is'){
    $prop = lcfirst(substr($method,2));
    if ($this->has($prop)&&$this->$prop != 'false')return true;
    return false;
}
throw new \BadMethodCallException("Method '$method' does not exist on ".get_class($this));
                                            [declaration] => public function __call($method, $args)
                                        )

                                )

                        )

                )

        )

)
