Array
(
    [type] => file
    [namespace] => Array
        (
            [type] => namespace
            [name] => Taeluf\PHTML
            [declaration] => namespace Taeluf\PHTML;
            [class] => Array
                (
                    [0] => Array
                        (
                            [type] => class
                            [docblock] => Array
                                (
                                    [type] => docblock
                                    [description] => Parses .php files into:
 1. A string with placeholders for the PHP code
 2. An array of PHP code, identified by their placeholders
                                )

                            [namespace] => Taeluf\PHTML
                            [fqn] => Taeluf\PHTML\PHPParser
                            [name] => PHPParser
                            [declaration] => class PHPParser
                            [properties] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => protected
                                                    [1] => string
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => The source HTML + PHP code

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => var
                                                                    [description] => string
                                                                )

                                                        )

                                                )

                                            [name] => src
                                            [declaration] => protected string $src;
                                        )

                                    [1] => Array
                                        (
                                            [type] => property
                                            [modifiers] => Array
                                                (
                                                    [0] => protected
                                                    [1] => object
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => The parsed pieces of code in the format:
 [
     'html' => '<div>A string of code with php placeholders like <b>phpadsfwefhaksldfjaskdlfjaskldfjasldfkphp</b> </div> and trailing text...',
     'php'  => [
         'phpadsfwefhaksldfjaskdlfjaskldfjasldfkphp' => '<?="Something echod";?>', 
         'phpid2'=>'<?php // another code block/?>'
     ]
 ]
The array is simply (object) cast
                                                )

                                            [name] => pieces
                                            [declaration] => protected object $pieces;
                                        )

                                )

                            [methods] => Array
                                (
                                    [0] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [arg_types] => Array
                                                                (
                                                                    [0] => string
                                                                )

                                                            [name] => htmlPHPString
                                                            [declaration] => string $htmlPHPString
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Create a new PHP parser instance from a string

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => mixed $htmlPHPString - a block of HTML + PHP code. PHP code will be inside open (<?php or <?=) and close (?>) tags
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => void
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => __construct
                                            [body] => $this->src = $htmlPHPString;
$this->pieces = $this->separatePHP();
                                            [declaration] => public function __construct(string $htmlPHPString)
                                        )

                                    [1] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Return the parsed pieces. See doc for protected $pieces

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => object
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => public
                                                )

                                            [name] => pieces
                                            [return_types] => Array
                                                (
                                                    [0] => object
                                                )

                                            [body] => return $this->pieces;
                                            [declaration] => public function pieces(): object
                                        )

                                    [2] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Separate the source code into it's pieces. See the protected $pieces docs
                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => object just an array cast to object
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => protected
                                                )

                                            [name] => separatePHP
                                            [return_types] => Array
                                                (
                                                    [0] => object
                                                )

                                            [body] => $tokens = $this->tokens();
$str = '';
$inPHP = false;
$phpCode = '';
$phpList = [];
foreach ($tokens as $index => $token){
    $token = (object)$token;
    if ($token->type=='T_OPEN_TAG'
        ||$token->type=='T_OPEN_TAG_WITH_ECHO'){
        $inPHP = true;
    }
    if (!$inPHP){
        $str .= $token->code;
    }
    if ($inPHP){
        $phpCode .= $token->code;
    }
    if ($token->type=='T_CLOSE_TAG'){
        $id = 'php'.$this->randomAlpha().'php';
        $phpList[$id] = $phpCode;
        $phpCode = '';
        $str .= $id;
        $inPHP = false;
    }
}
return (object)[
    'php'=>$phpList,
    'html'=>$str
];
                                            [declaration] => protected function separatePHP(): object
                                        )

                                    [3] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => length
                                                            [value] => 26
                                                            [declaration] => $length = 26
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Generates a random string of characters a-z, all lowercase & returns them
 this is used for the PHP placeholders

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => int $length
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => string
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => protected
                                                )

                                            [name] => randomAlpha
                                            [return_types] => Array
                                                (
                                                    [0] => string
                                                )

                                            [body] => return static::getRandomAlpha($length);
                                            [declaration] => protected function randomAlpha($length = 26): string
                                        )

                                    [4] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                    [0] => Array
                                                        (
                                                            [type] => arg
                                                            [name] => length
                                                            [value] => 26
                                                            [declaration] => $length = 26
                                                        )

                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Generates a random string of characters a-z, all lowercase & returns them
 this is used for the PHP placeholders

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => param
                                                                    [description] => int $length
                                                                )

                                                            [1] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => string
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => static
                                                    [1] => public
                                                )

                                            [name] => getRandomAlpha
                                            [return_types] => Array
                                                (
                                                    [0] => string
                                                )

                                            [body] => $characters = 'abcdefghijklmnopqrstuvwxyz';
$charactersLength = strlen($characters);
$randomString = '';
for ($i = 0; $i < $length; $i++) {
    $randomString .= $characters[rand(0, $charactersLength - 1)];
}
return $randomString;
                                            [declaration] => static public function getRandomAlpha($length = 26): string
                                        )

                                    [5] => Array
                                        (
                                            [type] => method
                                            [args] => Array
                                                (
                                                )

                                            [docblock] => Array
                                                (
                                                    [type] => docblock
                                                    [description] => Tokenize the src code into a slightly better format than token_get_all

                                                    [attribute] => Array
                                                        (
                                                            [0] => Array
                                                                (
                                                                    [type] => attribute
                                                                    [name] => return
                                                                    [description] => array of tokens
                                                                )

                                                        )

                                                )

                                            [modifiers] => Array
                                                (
                                                    [0] => protected
                                                )

                                            [name] => tokens
                                            [return_types] => Array
                                                (
                                                    [0] => array
                                                )

                                            [body] => $content = $this->src;
$tokens = token_get_all($content);
$niceTokens = [];
$delLine = null;
foreach ($tokens as $index=>$token){
    $tok = [];
    if (!is_array($token)){
        $lastTok = array_slice($niceTokens,-1)[0];
        $tok['type'] = "UNNAMED_TOKEN";
        $tok['code'] = $token;
        $tok['line'] = $lastTok['line'];
    } else {
        $tok['type'] = token_name($token[0]);
        $tok['code'] = $token[1];
        $tok['line'] = $token[2];
    }
    // This is old code for an idea I had
    // if ($tok['type']=='T_STRING'&&$tok['code']=='PHPPlus'){
        // $next = $tokens[$index+1];
        // if (is_array($next)&&$next[1]=='::'){
            // $delLine = $tok['line'];
            // echo 'del line is '.$delLine."\n\n";
        // }
    // }
    $niceTokens[] = $tok;
}
foreach ($niceTokens as $index=>$token){
    if ($token['line']===$delLine){
        unset($niceTokens[$index]);
    }
}
return $niceTokens;
                                            [declaration] => protected function tokens(): array
                                        )

                                )

                        )

                )

        )

)
