{
    "type": "file",
    "namespace": {
        "type": "namespace",
        "name": "Taeluf\\PHTML",
        "declaration": "namespace Taeluf\\PHTML;",
        "class": [
            {
                "type": "class",
                "namespace": "Taeluf\\PHTML",
                "fqn": "Taeluf\\PHTML\\TextNode",
                "name": "TextNode",
                "extends": "\\DOMText",
                "declaration": "class TextNode extends \\DOMText",
                "methods": [
                    {
                        "type": "method",
                        "args": [
                            {
                                "type": "arg",
                                "arg_types": [
                                    "string"
                                ],
                                "name": "tagName",
                                "declaration": "string $tagName"
                            }
                        ],
                        "docblock": {
                            "type": "docblock",
                            "description": "is this node the given tag"
                        },
                        "modifiers": [
                            "public"
                        ],
                        "name": "is",
                        "return_types": [
                            "bool"
                        ],
                        "body": "if (strtolower($this->nodeName)==strtolower($tagName))return true;\nreturn false;",
                        "declaration": "public function is(string $tagName): bool"
                    }
                ]
            }
        ]
    }
}