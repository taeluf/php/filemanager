<?php

namespace Tlf\Lexer\Test\Document;


class Idk extends \Tlf\Lexer\Test\Tester {

    public function testLexPhp(){
        //@export_start(Test.Doc.LexPhpString)

        // initialize lexer & grammar
        $lexer = new \Tlf\Lexer();
        $phpGrammar = new \Tlf\Lexer\PhpGrammar();
        $lexer->addGrammar($phpGrammar);

        // Add starting directive to the lexer
        $starting_directives = $phpGrammar->getDirectives(':php_open');
        // there's only one... i don't know why it's like this...
        $lexer->addDirective($starting_directives['php_open']);

        // typically, either "file" or "str"
        $starting_ast = new \Tlf\Lexer\Ast($ast_type="file", $initial_tree=[]);

        // read a file & generate an ast. May load from cache.
        $file = $this->file('test/input/php/DocumentationExample.php');
        $ast = $lexer->lexFile($file);

        // or you can lex a string, but then you don't get caching 
        //$string = file_get_contents($this->file('test/input/php/DocumentationExample.php'));
        //$ast = $lexer->lex($string, $ast)

        // convert ast to array
        $tree = $ast->getTree();
        //@export_end(Test.Doc.LexPhpString)
        
        $this->compare($tree, require($this->file('test/input/php/DocumentationExampleTree.php')));
    }

    public function testLexString(){
        //@export_start(Test.Doc.LexString)
        $lexer = new \Tlf\Lexer();
        $docGrammar = new \Tlf\Lexer\DocblockGrammar();
        $lexer->addGrammar($docGrammar);
        $lexer->addDirective($docGrammar->getDirectives(':/*')['/*']);
        $str = "/** I am docblock */";
        $ast = $lexer->lex($str);

        $tree = $ast->getTree();
        $actual = $tree['docblock'][0];
        $expect = [
            'type'=>'docblock',
            'description'=>'I am docblock',
        ];
        //@export_end(Test.Doc.LexString)

        $this->compare($expect, $actual);
        
        $this->compare($ast->src, $str);
    }

    public function testLexAst(){

        echo "Uses old php grammar";
        $this->disable();
        return;


        //@export_start(Test.Doc.LexAst)
        $lexer = new \Tlf\Lexer();
        $phpGrammar = new \Tlf\Lexer\PhpGrammar();
        $lexer->addGrammar($phpGrammar);

        // set up the ast
        $ast = new \Tlf\Lexer\Ast('code');
        $ast->set ('language', 'php');
        $code = '<?php class Abc extends Alphabet {}';
        $ast->set('src', $code);


        $ast = $lexer->lex($code, $ast);
        $actual = $ast->getTree();
        $expect = [
            'type'=>'code',
            'language'=>'php',
            'src'=>$code,
            'class'=>[
                0=>[
                    'type'=>'class',
                    'docblock'=>'',
                    'namespace'=>'',
                    'name'=>'Abc',
                    'declaration'=>'class Abc extends Alphabet ',
                ],
            ],
        ];
        //@export_end(Test.Doc.LexAst)

        $this->compare($actual, $expect);
        $this->compare($ast->src, $code);
    }

    public function testLexFile(){

        echo "Uses old php grammar";
        $this->disable();
        return;

        $dir = dirname(__DIR__).'/php/';
        $file = $dir.'SampleClass.php';
        $targetTree = include($dir.'SampleClass.tree.php');

        //@export_start(Test.Doc.LexFile)
        $lexer = new \Tlf\Lexer();
        $lexer->useCache = false; // cache is disabled only for testing
        $lexer->addGrammar($phpGrammar = new \Tlf\Lexer\PhpGrammar());

        $ast = $lexer->lexFile(dirname(__DIR__).'/php/SampleClass.php');

        // An array detailing the file 
        $tree = $ast->getTree(); 
        //@export_end(Test.Doc.LexFile)

        $this->compare(
            $targetTree, 
            $tree
        );
    }

}
