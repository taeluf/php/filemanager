<?php

namespace Tlf\Lexer\Test;

/**
 * Tests otherwise unsorted
 */
class Main extends \Tlf\Tester {

    public function testTrimTrailingWhitespace(){
        $str = "abc   \ndef  \nokay ";
        $clean = \Tlf\Lexer\Utility::trim_trailing_whitespace($str);

        $this->compare(
            "abc\ndef\nokay",
            $clean
        );
    }
}
