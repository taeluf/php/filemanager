<?php

namespace Tlf\Lexer\Test\Main;

/**
 * Just a simple test with a simple grammar to make sure the lexer works
 */
class StarterGrammar extends \Tlf\Lexer\Test\Tester {

    public function testStarterGrammar(){
        //@---export_start(Test.Doc.LexString)
        $lexer = new \Tlf\Lexer();
        $lexer->debug = true;

        $starterGrammar = new \Tlf\Lexer\Test\Src\StarterGrammar();
        $lexer->addGrammar($starterGrammar);
        $str = "(a, b, c) (d, e,f)";
        $ast = $lexer->lex($str);

        $tree = $ast->getTree();
        $actual = $tree['argsets'];
        $expect = [
            [
                'type'=>'arglist',
                'args'=>['a','b','c'],
            ],
            [
                'type'=>'arglist',
                'args'=>['d','e','f'],
            ],
        ];
        //@---export_end(Test.Doc.LexString)

        $this->compare($expect, $actual);
        
        $this->compare($ast->src, $str);
    }
}
