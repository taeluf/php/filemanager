<?php

namespace Tlf\Lexer\Test\Src\Starter;

trait OtherDirectives {

    protected $_other_directives = [

        'parenthesis'=>[
            'start'=>[
                'match'=>'(',
                'ast.new'=>[
                    '_type'=>'arglist',
                    '_addto'=>'argsets',
                ],
                'buffer.clear',
                'then :comma',
                'then.pop :parenthesis.stop 1',
            ],
            'stop'=>[
                'match'=>')',
                'inherit :comma.start',
                'ast.pop', 
            ]
        ],
        'comma'=>[
            'start'=>[
                'match'=>'/,$/', //any string starting with a `/` will be treated as regex
                'rewind 1',
                'this:trimBuffer',
                'ast.push args',
                'forward 1',
                'buffer.clear',
                'stop',
            ]
        ],

    ];

}
