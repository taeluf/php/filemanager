<?php

namespace Tlf\Lexer\Test\Directives;

trait UseTrait {
    protected $_use_trait_tests = [

        'UseTrait.Docblock'=>[
            'start'=>['php_code'],
            'input'=>"/** docs */ use AbcDef_09;",
            'expect'=>[
                'traits'=>[
                    0=>[
                        'type'=>'use_trait',
                        'docblock'=>[
                            'type'=>'docblock',
                            'description'=>'docs',
                        ],
                        'name'=>'AbcDef_09',
                        'declaration'=>'use AbcDef_09;',
                    ]
                ],
            ],
        ],

        'UseTrait.WithNamespace.IntermediateDocblocks'=>[
            'is_bad_test'=>'The declaration SHOULD have a space after `use`, but it doesn\'t. I don\'t plan to fix this. The test is passing because this bug is acceptable. ',
            'start'=>['php_code'],
            'input'=>"use/*k*/Abc\Def\_09;",
            'expect'=>[
                'traits'=>[
                    0=>[
                        'type'=>'use_trait',
                        'name'=>'Abc\Def\_09',
                        'declaration'=>'useAbc\Def\_09;',
                    ]
                ],
            ],
        ],

        'UseTrait.WithNamespace'=>[
            'start'=>['php_code'],
            'input'=>"use Abc\Def\_09;",
            'expect'=>[
                'traits'=>[
                    0=>[
                        'type'=>'use_trait',
                        'name'=>'Abc\Def\_09',
                        'declaration'=>'use Abc\Def\_09;',
                    ]
                ],
            ],
        ],

        'UseTrait.Simple'=>[
            'start'=>['php_code'],
            'input'=>"use AbcDef_09;",
            'expect'=>[
                'traits'=>[
                    0=>[
                        'type'=>'use_trait',
                        'name'=>'AbcDef_09',
                        'declaration'=>'use AbcDef_09;',
                    ]
                ],
            ],
        ],

    ];
}
