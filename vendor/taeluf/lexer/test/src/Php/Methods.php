<?php

namespace Tlf\Lexer\Test\Directives;

trait Methods {
    protected $_method_tests = [

        'Method.SimpleBody'=>[
            // 'expect_failure'=>true,
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>
                <<<PHP
                    public function is_the_us_imperialist():bool {
                        \$var = new \Value();
                        return true;
                    } 
                PHP,
            'expect'=>[
                'methods'=>[
                    0=>[
                        'type'=>'method',
                        'args'=>[],
                        'modifiers'=>['public'],
                        'name'=>'is_the_us_imperialist',
                        'return_types'=>['bool'],
                        'body'=>"\$var = new \\Value();\nreturn true;",
                        'declaration'=>'public function is_the_us_imperialist():bool',
                    ],
                ],
            ],
        ],
        'Method.ReturnReference'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>'public function &abc() {',
            'expect'=>[
                'methods'=>[
                    0=>[
                        'type'=>'method',
                        'args'=>[],
                        'modifiers'=>['public'],
                        'return_by_reference'=>true,
                        'name'=>'abc',
                        'body'=>[],
                        'declaration'=>'public function &abc()',
                    ],
                ],
            ],
        ],

        'Method.WithNestedBlock.2'=>[
            // 'expect_failure'=>true,
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>
                <<<PHP
                    public function is_the_us_imperialist():bool {
                        if (\$you_buy_into_imperialist_propaganda){
                            return false;
                        } 
                        return true;
                    } 
                    public function is_the_us_nice():bool {
                        if (\$you_are_rich){
                            return true;
                        } 
                        return false;
                    } 
                PHP,
            'expect'=>[
                'methods'=>[
                    0=>[
                        'type'=>'method',
                        'args'=>[],
                        'modifiers'=>['public'],
                        'name'=>'is_the_us_imperialist',
                        'return_types'=>['bool'],
                        'body'=>
                            "if (\$you_buy_into_imperialist_propaganda){"
                            ."\n    return false;"
                            ."\n}" 
                            ."\nreturn true;",
                        
                        'declaration'=>'public function is_the_us_imperialist():bool',
                    ],
                    1=>[
                        'type'=>'method',
                        'args'=>[],
                        'modifiers'=>['public'],
                        'name'=>'is_the_us_nice',
                        'return_types'=>['bool'],
                        'body'=>
                            "if (\$you_are_rich){"
                            ."\n    return true;"
                            ."\n}" 
                            ."\nreturn false;",
                        'declaration'=>'public function is_the_us_nice():bool',
                    ],
                ],
            ],
        ],

        'Method.WithNestedBlock'=>[
            // 'expect_failure'=>true,
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>
                <<<PHP
                    public function is_the_us_imperialist():bool {
                        if (\$you_buy_into_imperialist_propaganda){
                            return false;
                        } 
                        return true;
                    } 
                PHP,
            'expect'=>[
                'methods'=>[
                    0=>[
                        'type'=>'method',
                        'args'=>[],
                        'modifiers'=>['public'],
                        'name'=>'is_the_us_imperialist',
                        'return_types'=>['bool'],
                        "body"=>
                            "if (\$you_buy_into_imperialist_propaganda){"
                            ."\n    return false;"
                            ."\n}" 
                            ."\nreturn true;",
                        'declaration'=>'public function is_the_us_imperialist():bool',
                    ],
                ],
            ],
        ],

        'Method.WithReturnType'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>'static public function abc(bool $b): string {',
            'expect'=>[
                'methods'=>[
                    0=>[
                        'type'=>'method',
                        'args'=>[
                            0=>[
                                'type'=>'arg',
                                'arg_types'=>['bool'],
                                'name'=>'b',
                                'declaration'=>'bool $b',
                            ],
                        ],
                        'modifiers'=>['static', 'public'],
                        'name'=>'abc',
                        'return_types'=>['string'],
                        'body'=>'',
                        'declaration'=>'static public function abc(bool $b): string',
                    ],
                ],
            ],
        ],

       
        'Method.TwoArgs'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>'/* whoo */ private function abc($arg1, $arg2) {',
            'expect'=>[
                'methods'=>[
                    0=>[
                        'type'=>'method',
                        'args'=>[
                            0=>[
                                'type'=>'arg',
                                'name'=>'arg1',
                                'declaration'=>'$arg1',
                            ],
                            1=>[
                                'type'=>'arg',
                                'name'=>'arg2',
                                'declaration'=>'$arg2',
                            ],
                        ],
                        'docblock'=>['type'=>'docblock','description'=>'whoo'],
                        'modifiers'=>['private'],
                        'name'=>'abc',
                        'body'=>'',
                        'declaration'=>'private function abc($arg1, $arg2)',
                    ],
                ],
            ],
        ],

        'Method.OneArg'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>'/* whoo */ public function abc(string $def=96) {',
            'expect'=>[
                'methods'=>[
                    0=>[
                        'type'=>'method',
                        'args'=>[
                            0=>[
                                'type'=>'arg',
                                'arg_types'=>['string'],
                                'name'=>'def',
                                'declaration'=>'string $def=96',
                                'value'=>'96',
                            ],
                        ],
                        'docblock'=>['type'=>'docblock','description'=>'whoo'],
                        'modifiers'=>['public'],
                        'name'=>'abc',
                        'body'=>'',
                        'declaration'=>'public function abc(string $def=96)',
                    ],
                ],
            ],
        ],

        'Method.Simple.OneArg'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>'public function abc($def) {',
            'expect'=>[
                'methods'=>[
                    0=>[
                        'type'=>'method',
                        'args'=>[
                            0=>[
                                'type'=>'arg',
                                'name'=>'def',
                                'declaration'=>'$def',
                            ],
                        ],
                        'modifiers'=>['public'],
                        'name'=>'abc',
                        'body'=>'',
                        'declaration'=>'public function abc($def)',
                    ],
                ],
            ],
        ],

        'Method.Static'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>'static public function abc() {',
            'expect'=>[
                'methods'=>[
                    0=>[
                        'type'=>'method',
                        'args'=>[],
                        'modifiers'=>['static','public'],
                        'name'=>'abc',
                        'body'=>'',
                        'declaration'=>'static public function abc()',
                    ],
                ],
            ],
        ],

        'Method.Simple'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>'public function abc() {',
            'expect'=>[
                'methods'=>[
                    0=>[
                        'type'=>'method',
                        'args'=>[],
                        'modifiers'=>['public'],
                        'name'=>'abc',
                        'body'=>'',
                        'declaration'=>'public function abc()',
                    ],
                ],
            ],
        ],
    ];
}
