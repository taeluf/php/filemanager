<?php

namespace Tlf\Lexer\Test\Directives;

trait Traits {

    protected $_trait_tests = [

        'Trait.InNamespace'=>[
            'ast.type'=>'file',
            'start'=>['php_code'],
            'input'=>"namespace Abc; trait Def {}",
            'expect'=>[
                'namespace'=>[
                    'type'=>'namespace',
                    'name'=>'Abc',
                    'declaration'=>'namespace Abc;',
                    'trait'=>[
                        0=>[
                            'type'=>'trait',
                            'fqn'=>'Abc\\Def',
                            'namespace'=>'Abc',
                            'name'=>'Def',
                            'declaration'=>'trait Def',
                        ],
                    ],
                ],
            ]
        ],

        'Trait.EmptyBody'=>[
            'ast.type'=>'file',
            'start'=>['php_code'],
            'input'=>"trait Abc {\n\n}",
            'expect'=>[
                'namespace'=>'',
                'trait'=>[
                    0=>[
                        'type'=>'trait',
                        'fqn'=>'Abc',
                        'namespace'=>'',
                        'name'=>'Abc',
                        'declaration'=>'trait Abc',
                    ],
                ],
            ]
        ],

        'Trait.OpenInFile'=>[
            'ast.type'=>'file',
            'start'=>['php_code'],
            'input'=>'trait Abc {',
            'expect'=>[
                'namespace'=>'',
                'trait'=>[
                    0=>[
                    'type'=>'trait',
                    'fqn'=>'Abc',
                    'namespace'=>'',
                    'name'=>'Abc',
                    'declaration'=> 'trait Abc',
                    ],
                ]
            ],
        ],

        'Trait.OpenInNamespace'=>[
            'ast.type'=>'namespace',
            'start'=>['php_code'],
            'input'=>'trait Abc {',
            'expect'=>[
                'trait'=>[
                    0=>[
                    'type'=>'trait',
                    'fqn'=>'Abc',
                    'namespace'=>'',
                    'name'=>'Abc',
                    'declaration'=> 'trait Abc',
                    ],
                ],
            ],
        ],

    ];
}
