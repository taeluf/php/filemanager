<?php

namespace Tlf\Lexer\Test\Directives;

trait Consts  {

    protected $_const_tests= [

        'Const.Two'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>' const blm = "Yes!";private const cat="yep"; ',
            'expect'=>[
                'const'=>[
                    0=>[
                        'type'=>'const',
                        'name'=>'blm',
                        'value'=> '"Yes!"',
                        'declaration'=>'const blm = "Yes!";',
                    ],
                    1=>[
                        'type'=>'const',
                        'modifiers'=>['private'],
                        'name'=>'cat',
                        'value'=> '"yep"',
                        'declaration'=>'private const cat="yep";',
                    ],
                ],
            ],
        ],
        
        'Const.Private'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>'private const blm = 86;',
            'expect'=>[
                'const'=>[
                    0=>[
                        'type'=>'const',
                        'name'=>'blm',
                        'modifiers'=>['private'],
                        'value'=>'86',
                        'declaration'=>'private const blm = 86;',
                    ],
                ],
            ],
        ],

        'Const.Simple'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>'const blm = "yes";',
            'expect'=>[
                'const'=>[
                    0=>[
                        'type'=>'const',
                        'name'=>'blm',
                        'value'=>'"yes"',
                        'declaration'=>'const blm = "yes";',
                    ],
                ],
            ],
        ],

        'Const.NoValue'=>[
            'ast.type'=>'class_body',
            'start'=>['php_code'],
            'input'=>'const blm ',
            'expect'=>[
                'const'=>[
                    0=>[
                        'type'=>'const',
                        'name'=>'blm',
                    ],
                ],
            ],
        ],

    ];
}
