<?php

namespace Tlf\Lexer\Test\Directives;

trait Classes {
    protected $_class_tests = [

        'Class.InNamespace'=>[
            'ast.type'=>'file',
            'start'=>['php_code'],
            'input'=>"namespace Abc; class Def {}",
            'expect'=>[
                'namespace'=>[
                    'type'=>'namespace',
                    'name'=>'Abc',
                    'declaration'=>'namespace Abc;',
                    'class'=>[
                        0=>[
                            'type'=>'class',
                            'namespace'=>'Abc',
                            'fqn'=>'Abc\\Def',
                            'name'=>'Def',
                            'declaration'=>'class Def',
                        ],
                    ],
                ],
            ]
        ],

        'Class.Final.EmptyBody'=>[
            'ast.type'=>'file',
            'start'=>['php_code'],
            'input'=>"final class Abc {\n\n}",
            'expect'=>[
                'namespace'=>'',
                'class'=>[
                    0=>[
                        'type'=>'class',
                        'modifiers'=>['final'],
                        'fqn'=>'Abc',
                        'namespace'=>'',
                        'name'=>'Abc',
                        'declaration'=>'final class Abc',
                    ],
                ],
            ]
        ],

        'Class.Implements'=>[
            'is_bad_test'=>"We don't yet catch what interfaces a class implements. We just shove it in the declaration",
            'ast.type'=>'namespace',
            'start'=>['php_code'],
            'input'=>'class Abc extends \Def\Ghi implements iAbc, iDef {',
            'expect'=>[
                'class'=>[
                    0=>[
                    'type'=>'class',
                    'fqn'=>'Abc',
                    'namespace'=>'',
                    'name'=>'Abc',
                    'extends'=>'\Def\Ghi',
                    'declaration'=> 'class Abc extends \Def\Ghi implements iAbc, iDef',
                    ],
                ],
            ],
        ],

        'Class.Extends'=>[
            'ast.type'=>'namespace',
            'start'=>['php_code'],
            'input'=>'class Abc extends \Def\Ghi {',
            'expect'=>[
                'class'=>[
                    0=>[
                    'type'=>'class',
                    'fqn'=>'Abc',
                    'namespace'=>'',
                    'name'=>'Abc',
                    'extends'=>'\Def\Ghi',
                    'declaration'=> 'class Abc extends \Def\Ghi',
                    ],
                ],
            ],
        ],
        'Class.Abstract'=>[
            'ast.type'=>'namespace',
            'start'=>['php_code'],
            'input'=>'abstract class Abc {',
            'expect'=>[
                'class'=>[
                    0=>[
                    'type'=>'class',
                    'modifiers'=>['abstract'],
                    'fqn'=>'Abc',
                    'namespace'=>'',
                    'name'=>'Abc',
                    'declaration'=> 'abstract class Abc',
                    ],
                ],
            ],
        ],
        'Class.OpenInFile'=>[
            'ast.type'=>'file',
            'start'=>['php_code'],
            'input'=>'class Abc {',
            'expect'=>[
                'namespace'=>'',
                'class'=>[
                    0=>[
                    'type'=>'class',
                    'fqn'=>'Abc',
                    'namespace'=>'',
                    'name'=>'Abc',
                    'declaration'=> 'class Abc',
                    ],
                ]
            ],
        ],
        'Class.OpenInNamespace'=>[
            'ast.type'=>'namespace',
            'start'=>['php_code'],
            'input'=>'class Abc {',
            'expect'=>[
                'class'=>[
                    0=>[
                    'type'=>'class',
                    'fqn'=>'Abc',
                    'namespace'=>'',
                    'name'=>'Abc',
                    'declaration'=> 'class Abc',
                    ],
                ],
            ],
        ],
    ];
}
