<?php

namespace Tlf\Lexer\Test\Directives;

trait Values {

    protected $_values_tests= [


        'Values.Arithmetic.Nesting'=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            // PEMDAS
            'input'=>'(1*2+(4*6/((1+2)+(3**2)))+3 - 12) / 3 ** 2 + 987;',
            'expect'=>[
                'value'=>'(1*2+(4*6/((1+2)+(3**2)))+3-12)/3**2+987',
                'declaration'=>'(1*2+(4*6/((1+2)+(3**2)))+3 - 12) / 3 ** 2 + 987;',
            ],
        ],
        'Values.Arithmetic'=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            // PEMDAS
            'input'=>'(1*2)+3 - 12 / 3 ** 2 + 987;',
            'expect'=>[
                'value'=>'(1*2)+3-12/3**2+987',
                'declaration'=>'(1*2)+3 - 12 / 3 ** 2 + 987;',
            ],
        ],

        'Values.Arithmetic.Add'=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            // PEMDAS
            'input'=>'1 + 2;',
            'expect'=>[
                'value'=>'1+2',
                'declaration'=>'1 + 2;',
            ],
        ],

        'Values.Array.WithConcats'=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            'input'=>'["a"."b"."c",22,"c"];',
            'expect'=>[
                'value'=>'["a"."b"."c",22,"c"]',
                'declaration'=>'["a"."b"."c",22,"c"];',
            ],
        ],

        'Values.Array.Keyed'=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            'input'=>'["a"=>1,22,"c"=>\'third element\'];',
            'expect'=>[
                'value'=>'["a"=>1,22,"c"=>\'third element\']',
                'declaration'=>'["a"=>1,22,"c"=>\'third element\'];',
            ],
        ],

        'Values.Array.IntStringValues'=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            'input'=>'["a",22,"c"];',
            'expect'=>[
                'value'=>'["a",22,"c"]',
                'declaration'=>'["a",22,"c"];',
            ],
        ],

        'Values.Array.StringValues'=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            'input'=>'["a","b","c"];',
            'expect'=>[
                'value'=>'["a","b","c"]',
                'declaration'=>'["a","b","c"];',
            ],
        ],

        'Values.Array.DoubleValue'=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            'input'=>'[1.33,2,3];',
            'expect'=>[
                'value'=>'[1.33,2,3]',
                'declaration'=>'[1.33,2,3];',
            ],
        ],

        'Values.Array.IntValues'=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            'input'=>'[1,2,3];',
            'expect'=>[
                'value'=>'[1,2,3]',
                'declaration'=>'[1,2,3];',
            ],
        ],

        'Values.BothQuotesConcat'=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            'input'=>'"This"."is".\'a\'."const";',
            'expect'=>[
                'value'=>'"This"."is".\'a\'."const"',
                'declaration'=>'"This"."is".\'a\'."const";',
            ],
        ],

        'Values.SingleQuotesConcat'=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            'input'=>"'This'.'is'.'a'.'thing';",
            'expect'=>[
                'value'=>"'This'.'is'.'a'.'thing'",
                'declaration'=>"'This'.'is'.'a'.'thing';",
            ],
        ],

        'Values.DoubleQuotesConcat'=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            'input'=>'"This"."is"."a"."thing";',
            'expect'=>[
                'value'=>'"This"."is"."a"."thing"',
                'declaration'=>'"This"."is"."a"."thing";',
            ],
        ],

        'Values.Double'=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            'input'=>'1.87;',
            'expect'=>[
                'value'=>'1.87',
                'declaration'=>'1.87;',
            ],
        ],

        'Values.Int'=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            'input'=>'1;',
            'expect'=>[
                'value'=>'1',
                'declaration'=>'1;',
            ],
        ],

        "Values.SemicolonStop"=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            'input'=>'"This";',
            'expect'=>[
                'value'=>'"This"',
                'declaration'=>'"This";',
            ],
        ],

        'Values.CommaSeparatedArg'=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            'input'=>'"This",',
            'expect'=>[
                'value'=>'"This"',
                'declaration'=>'"This"',
            ],
        ],

        'Values.CloseArgList'=>[
            'ast.type'=>'var_assign',
            'start'=>['php_code'],
            'input'=>'"This")',
            'expect'=>[
                'value'=>'"This"',
                'declaration'=>'"This"',
            ],
        ],
    ];
}
