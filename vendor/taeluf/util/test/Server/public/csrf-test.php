<?php

$fs = new \Tlf\Util\FormSpam();
ob_start();
$key = $fs->enable_csrf('csrf-test', 10, '/csrf-test-post.php');
ob_end_clean();
$data = $_SESSION[$key];
$data['key'] = $key;

echo json_encode($data);
