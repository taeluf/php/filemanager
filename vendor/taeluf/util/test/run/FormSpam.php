<?php

namespace Tlf\Util\Test;

/**
 * Test honeypots and full spam control
 */
class FormSpam extends \Tlf\Tester {

    public function testFormSpam(){
        $address = $this->get_server();
        $output = ob_end_clean();
        echo "\n\nVisit $address/spam-form.php";
        echo "\nWhen you submit the form, does it respond with 'submission valid'?";
        $answer = readline("Answer y/n/s-skip");


        ob_start();
        echo $output;


        if ($answer =='s'){
            $this->disable();
            return;
        } else if ($answer=='y')$this->handleDidPass(true);
        else $this->handleDidPass(false);

    }

    public function testSubmitWithCsrfOnly(){
        $response = $this->curl_post('/spam-form.php');
        $content = $response['body'];
        $session_id = $response['cookies']['PHPSESSID']['value'];
        // $csrf = json_decode($content, true);
//
        // var_dump($response);
        // exit;
        // $phtml = new \Taeluf\PHTML('<html>'.$content.'</html>');
        $phtml = new \Taeluf\PHTML($content);
        $key_node = $phtml->xpath('//input[@name="csrf_key"]')[0];
        $key = $key_node->value;
        $value_node = $phtml->xpath('//input[@name="'.$key.'"]')[0];
        $code = $value_node->value;
        

        $bad_response = $this->curl_post('/spam-post.php', [$key=>null],
            $files=[],'backward_compat',
            [
               'REFERER: '.$this->cli->get_server_host(),
               'Cookie'=> 'PHPSESSID='.$session_id,
            ]
        );

        $this->str_contains(
            $bad_response['body'],
            'submission NOT valid'
        );
        // echo "\n\nInvalid Response:\n".$bad_response['body'];

        $good_response = $this->curl_post('/spam-post.php', [$key=>$code],
            $files=[],'backward_compat',
            [
               'REFERER: '.$this->cli->get_server_host(),
               'Cookie'=> 'PHPSESSID='.$session_id,
            ]
        );

        $this->str_contains($good_response['body'],
            'submission NOT valid'
        );

        // echo "\n\nGood Response:\n".$good_response['body'];
    }

    public function testSubmitWithoutHoneyOrCsrf(){
        // $form = $this->get('/honey-form.php');
        $response = $this->post("/spam-post.php");
        $this->compare('submission NOT valid', $response);
    }
}
