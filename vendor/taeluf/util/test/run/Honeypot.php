<?php

namespace Tlf\Util\Test;

/**
 * Test honeypots and full spam control
 */
class Honeypot extends \Tlf\Tester {

    public function testSubmitWithProperHoney(){
        $address = $this->get_server();
        $output = ob_end_clean();
        echo "\n\nVisit $address/honey-form.php";
        echo "\nWhen you submit the form, does it respond with 'honey post test success'?";
        $answer = readline("Answer y/n/s-skip");


        ob_start();
        echo $output;


        if ($answer =='s'){
            $this->disable();
            return;
        } else if ($answer=='y')$this->handleDidPass(true);
        else $this->handleDidPass(false);

    }

    public function testSubmitWithoutHoney(){
        // $form = $this->get('/honey-form.php');
        $response = $this->post("/honey-post.php");
        $this->compare('honey post test not valid', $response);
    }
    public function testGetHoney(){
        $content = $this->get('/honey-form.php');

        echo $content;

        $this->str_contains($content, 
            '<input type="text"',
            '<input type="hidden"',
            'Please type <b>',
            ' into here, or enable javascript:',
        );

    }

}
