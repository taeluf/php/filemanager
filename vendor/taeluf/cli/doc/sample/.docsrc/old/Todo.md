<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# Code Scrawl TODO    
  
## ToDONEs for v 0.0.03  
- Wrote a new README.     
- Wrote a Configs extension    
- Wrote an Events extension    
- Delete old code from this branch.    
- Handle user configuration    
- Smart defaults for configs    
- Re-approach how the CLI thing works    
- Wrote InputExtensions as I expect them to work    
- Moved all code to the files they belong in    
- Sequestered old code (twice)    
- implemented a cleaner approach to keeping track of multiple regexes (using an array with nice keys, instead of multiple properties)    
- Redesigned this library's architecture to be extensible (this was the HARD part)    
- Written some new code to support the extensible version of this that I'm doing now    
- wrote a simple docblock class    
- Was very nice to myself about all the old code I had written that was sloppy, confusing, and hard to follow. <3    
- Wrote this READMENOW file to make Future Reed very happy and less overwhelmed.    
- I love you future Reed. (lol. If you're NOT a Reed, maybe do something to make your future self happy. Or maybe don't. Maybe your future self is an evil duck & you don't want evil ducks to be happy. I don't know your life.) - Present Reed (but i guess its kinda past Reed, if you think about it)    
