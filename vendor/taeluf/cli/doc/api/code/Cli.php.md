<!-- DO NOT EDIT. This file generated from template by Code Scrawl https://tluf.me/php/code-scrawl/ -->  
# File code/Cli.php  
  
# class Tlf\Cli  
  
  
  
## Constants  
  
## Properties  
- `public $pwd;` The current working directory  
- `public $command_map = [];` map of command name to callable  
- `public $help_map = [];` command_name=>help_msg  
- `public $command = 'main';` The command being executed  
- `public $args = [];` The args to pass to your command's callable (key=>value array)  
- `public $name;` The name of the command used to execute this (not path)  
- `public string $log_dir;` Directory to write log files to.  
- `public $argv;`   
  
## Methods   
- `public function __construct($pwd = null, $argv  null)`   
- `public function log(string $message, string $log_file = 'tlf-main')` Log $message to disk with a date & timestamp. You must set `$cli->log_dir` to a valid directory.  
  
- `public function load_stdin($stdin=null)` load standard inputs from cli  
- `public function parse_args($stdin_args, $args=[])`   
- `public function load_command(string $command, $callable, string $help_msg='')`   
- `public function load_json_file(string $file)` Load a json settings file, if it exists. Fails silently if it does not exist.  
  
- `public function load_inputs($args)`   
- `public function call_command(string $command, array $args=[])` Execute a command by name  
  
- `public function execute()` Run the cli based on loaded inputs  
- `public function help_menu($cli, $args)`   
- `public function ask(string $msg, $success_func=null,...$args)` Prompt user for y/no answer. Lowercase 'y' is yes. All other answers are no.  
  
- `public function prompt(string $msg)` Prompt user for string input  
  
  
