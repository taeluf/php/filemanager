# Development Status

## December 3, 2021
I think it's basically good to go. More features can always be added, though. I didn't setup most the things listed in the TODO below


## Before December 3, 2021

- `test/proto` is a sample implementation of the library
- `test/Cli.php` is a very simple, early version, feature incomplete implementation of a cli-library
- `test.php` is a sample test file with a few simple tests that are passing

### TODO
- add handling for 'command not found'
- add handling for sub-commands
- add handling for routing parent commands
- scope configs/settings to the command they're relevant to
- add help menus (even if it ONLY lists the commands with no help info)
- add helper functions for:
    - load json file as inputs
    - load php file (which returns an array) as inputs
    - load a class's methods as a set of commands
- Documentation
- polish
